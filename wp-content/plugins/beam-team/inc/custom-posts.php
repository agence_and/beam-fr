<?php
function beam_team_custom_post_team()
{
    $labels = array(
        'name'               	=> __( 'Team members', BEAM_TEAM_SETTINGS_OPTION_NAME ),
        'singular_name'      	=> __( 'Team member', BEAM_TEAM_SETTINGS_OPTION_NAME ),
        'add_new'            	=> __( 'Add a team member', BEAM_TEAM_SETTINGS_OPTION_NAME ),
        'add_new_item'       	=> __( 'Add a team member' ),
        'new_item'           	=> __( 'New team member' ),
        'edit_item'          	=> __( 'Modify the team member' ),
        'view_item'          	=> __( 'See the team member' ),
        'all_items'          	=> __( 'All team' ),
        'search_items'       	=> __( 'Search team members' ),
        'parent_item_colon'  	=> __( 'Parent team member :' ),
        'not_found'          	=> __( 'No team member found' ),
        'not_found_in_trash' 	=> __( 'No team member found in trash' )
    );

    $args = array(
        'labels'             	=> $labels,
        'public'             	=> true,
        'show_ui'            	=> true,
        'menu_icon'       		=> 'dashicons-groups',
        'menu_position'			=> 20,
        'exclude_from_search' 	=> true,
        'hierarchical'       	=> false,
        'supports'          	=> array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'			=> array( 'team_category' ),
        'has_archive'        	=> false,
        'rewrite'            	=> array(
            'slug' 		=> 'team',
            'withfront'	=> true,
        ),
        'query_var'          	=> true,
    );

    register_post_type( 'team', $args );
}


function _beam_team_init_custom_posts()
{
    beam_team_custom_post_team();
}
add_action( 'init', '_beam_team_init_custom_posts' );