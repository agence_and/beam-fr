<?php

function beam_team_custom_taxonomy_category()
{
    $labels = array(
        'name'              			=> __( 'Types of team member', BEAM_TEAM_SETTINGS_OPTION_NAME ),
        'singular_name'     			=> __( 'Type of team member', BEAM_TEAM_SETTINGS_OPTION_NAME ),
        'search_items'      			=> __( 'Search an type of team member' ),
        'all_items'         			=> __( 'All types of team member' ),
        'parent_item'       			=> __( 'Parent type of team member' ),
        'parent_item_colon'				=> __( 'Parent type of team member :' ),
        'edit_item'         			=> __( 'Modify the type of team member' ),
        'view_item'         			=> __( 'See' ),
        'update_item'       			=> __( 'Update type of team member' ),
        'add_new_item'      			=> __( 'Add a new type of team member' ),
        'new_item_name'     			=> __( 'Name of type of team member' ),
        'popular_items'     			=> __( 'Popular types of team member' ),
        'separate_items_with_commas'    => __( 'Separate types of team member with comas' ),
        'add_or_remove_items'     		=> __( 'Add or remove type of team member' ),
        'choose_from_most_used'     	=> __( 'Choose from most used' ),
        'not_found'     				=> __( 'No type of team member found' ),
        'menu_name'         			=> __( 'Type of team member' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array(
            'slug' 			=> 'team_category',
            'withfront'		=> false,
            'hierarchical'	=> false,
        ),
    );

    register_taxonomy( 'team_category', array( 'team' ), $args );
}


function _beam_team_custom_taxonomies()
{
    beam_team_custom_taxonomy_category();
}
add_action( 'init', '_beam_team_custom_taxonomies', 0 );