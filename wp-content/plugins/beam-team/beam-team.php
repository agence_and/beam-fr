<?php
/*
Plugin Name: BeAM - Team Members
Plugin URI: http://www.and-digital.fr
Description: Manage team members
Version: 1.0.0
Author: Agence AND
Author URI: http://www.and-digital.fr
*/
define( 'BEAM_TEAM_SETTINGS_OPTION_GROUP', 'beamteamSettings' );
define( 'BEAM_TEAM_SETTINGS_OPTION_NAME', 'beam_team_settings' );

include( plugin_dir_path( __FILE__ ) . 'inc/custom-posts.php' );
include( plugin_dir_path( __FILE__ ) . 'inc/custom-taxonomies.php' );