<?php
/*
Plugin Name: BeAM - Press releases
Plugin URI: http://www.and-digital.fr
Description: Manage press releases
Version: 1.0.0
Author: Agence AND
Author URI: http://www.and-digital.fr
*/
define( 'BEAM_PRESS_SETTINGS_OPTION_GROUP', 'beampressSettings' );
define( 'BEAM_PRESS_SETTINGS_OPTION_NAME', 'beam_press_settings' );

include( plugin_dir_path( __FILE__ ) . 'inc/custom-posts.php' );
//include( plugin_dir_path( __FILE__ ) . 'inc/custom-taxonomies.php' );