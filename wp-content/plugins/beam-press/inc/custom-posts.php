<?php
function beam_press_custom_post_press()
{
    $labels = array(
        'name'               	=> __( 'Press releases', BEAM_PRESS_SETTINGS_OPTION_NAME ),
        'singular_name'      	=> __( 'Press release', BEAM_PRESS_SETTINGS_OPTION_NAME ),
        'add_new'            	=> __( 'Add a press release', BEAM_PRESS_SETTINGS_OPTION_NAME ),
        'add_new_item'       	=> __( 'Add a press release' ),
        'new_item'           	=> __( 'New press release' ),
        'edit_item'          	=> __( 'Modify the press release' ),
        'view_item'          	=> __( 'See the press release' ),
        'all_items'          	=> __( 'All press' ),
        'search_items'       	=> __( 'Search press' ),
        'parent_item_colon'  	=> __( 'Parent press release :' ),
        'not_found'          	=> __( 'No press release found' ),
        'not_found_in_trash' 	=> __( 'No press release found in trash' )
    );

    $args = array(
        'labels'             	=> $labels,
        'public'             	=> true,
        'show_ui'            	=> true,
        'menu_icon'       		=> 'dashicons-media-document',
        'menu_position'			=> 20,
        'exclude_from_search' 	=> true,
        'hierarchical'       	=> false,
        'supports'          	=> array( 'title', 'editor' ),
        'taxonomies'			=> array( 'press_category' ),
        'has_archive'        	=> false,
        'rewrite'            	=> array(
            'slug' 		=> 'press',
            'withfront'	=> true,
        ),
        'query_var'          	=> true,
    );

    register_post_type( 'press', $args );
}


function _beam_press_init_custom_posts()
{
    beam_press_custom_post_press();
}
add_action( 'init', '_beam_press_init_custom_posts' );