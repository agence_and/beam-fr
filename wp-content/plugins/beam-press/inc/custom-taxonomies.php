<?php

function beam_press_custom_taxonomy_category()
{
    $labels = array(
        'name'              			=> __( 'Press releases types', BEAM_PRESS_SETTINGS_OPTION_NAME ),
        'singular_name'     			=> __( 'Press releases type', BEAM_PRESS_SETTINGS_OPTION_NAME ),
        'search_items'      			=> __( 'Search a press releases type' ),
        'all_items'         			=> __( 'All press releases types' ),
        'parent_item'       			=> __( 'Parent press releases type' ),
        'parent_item_colon'				=> __( 'Parent press releases type :' ),
        'edit_item'         			=> __( 'Modify the press releases type' ),
        'view_item'         			=> __( 'See' ),
        'update_item'       			=> __( 'Update press releases type' ),
        'add_new_item'      			=> __( 'Add a new press releases type' ),
        'new_item_name'     			=> __( 'Name of press releases type' ),
        'popular_items'     			=> __( 'Popular press releases types' ),
        'separate_items_with_commas'    => __( 'Separate press releases types with comas' ),
        'add_or_remove_items'     		=> __( 'Add or remove press releases type' ),
        'choose_from_most_used'     	=> __( 'Choose from most used' ),
        'not_found'     				=> __( 'No press releases type found' ),
        'menu_name'         			=> __( 'Press releases type' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array(
            'slug' 			=> 'press_category',
            'withfront'		=> false,
            'hierarchical'	=> false,
        ),
    );

    register_taxonomy( 'press_category', array( 'press' ), $args );
}


function _beam_press_custom_taxonomies()
{
    beam_press_custom_taxonomy_category();
}
add_action( 'init', '_beam_press_custom_taxonomies', 0 );