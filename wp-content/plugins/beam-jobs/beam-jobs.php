<?php
/*
Plugin Name: BeAM - Jobs
Plugin URI: http://www.and-digital.fr
Description: Manage jobs
Version: 1.0.0
Author: Agence AND
Author URI: http://www.and-digital.fr
*/
define( 'BEAM_JOBS_SETTINGS_OPTION_GROUP', 'beamjobsSettings' );
define( 'BEAM_JOBS_SETTINGS_OPTION_NAME', 'beam_jobs_settings' );

include( plugin_dir_path( __FILE__ ) . 'inc/custom-posts.php' );
include( plugin_dir_path( __FILE__ ) . 'inc/custom-taxonomies.php' );