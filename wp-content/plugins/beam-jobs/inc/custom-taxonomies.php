<?php

function beam_jobs_custom_taxonomy_category()
{
    $labels = array(
        'name'              			=> __( 'Positions', BEAM_JOBS_SETTINGS_OPTION_NAME ),
        'singular_name'     			=> __( 'Position', BEAM_JOBS_SETTINGS_OPTION_NAME ),
        'search_items'      			=> __( 'Search a position' ),
        'all_items'         			=> __( 'All positions' ),
        'parent_item'       			=> __( 'Parent position' ),
        'parent_item_colon'				=> __( 'Parent position :' ),
        'edit_item'         			=> __( 'Modify the position' ),
        'view_item'         			=> __( 'See' ),
        'update_item'       			=> __( 'Update position' ),
        'add_new_item'      			=> __( 'Add a new position' ),
        'new_item_name'     			=> __( 'Name of position' ),
        'popular_items'     			=> __( 'Popular positions' ),
        'separate_items_with_commas'    => __( 'Separate positions with comas' ),
        'add_or_remove_items'     		=> __( 'Add or remove position' ),
        'choose_from_most_used'     	=> __( 'Choose from most used' ),
        'not_found'     				=> __( 'No position found' ),
        'menu_name'         			=> __( 'Position' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array(
            'slug' 			=> 'position',
            'withfront'		=> false,
            'hierarchical'	=> false,
        ),
    );

    register_taxonomy( 'jobs_category', array( 'jobs' ), $args );
}


function _beam_jobs_custom_taxonomies()
{
    beam_jobs_custom_taxonomy_category();
}
add_action( 'init', '_beam_jobs_custom_taxonomies', 0 );