<?php
function beam_jobs_custom_post_jobs()
{
    $labels = array(
        'name'               	=> __( 'Jobs', BEAM_JOBS_SETTINGS_OPTION_NAME ),
        'singular_name'      	=> __( 'Job', BEAM_JOBS_SETTINGS_OPTION_NAME ),
        'add_new'            	=> __( 'Add a job', BEAM_JOBS_SETTINGS_OPTION_NAME ),
        'add_new_item'       	=> __( 'Add a job' ),
        'new_item'           	=> __( 'New job' ),
        'edit_item'          	=> __( 'Modify the job' ),
        'view_item'          	=> __( 'See the job' ),
        'all_items'          	=> __( 'All jobs' ),
        'search_items'       	=> __( 'Search jobs' ),
        'parent_item_colon'  	=> __( 'Parent job :' ),
        'not_found'          	=> __( 'No job found' ),
        'not_found_in_trash' 	=> __( 'No job found in trash' )
    );

    $args = array(
        'labels'             	=> $labels,
        'public'             	=> true,
        'show_ui'            	=> true,
        'menu_icon'       		=> 'dashicons-list-view',
        'menu_position'			=> 20,
        'exclude_from_search' 	=> true,
        'hierarchical'       	=> false,
        'supports'          	=> array( 'title' ),
        'taxonomies'			=> array( 'jobs_category' ),
        'has_archive'        	=> false,
        'rewrite'            	=> array(
            'slug' 		=> 'jobs',
            'withfront'	=> true,
        ),
        'query_var'          	=> true,
    );

    register_post_type( 'jobs', $args );
}


function _beam_jobs_init_custom_posts()
{
    beam_jobs_custom_post_jobs();
}
add_action( 'init', '_beam_jobs_init_custom_posts' );