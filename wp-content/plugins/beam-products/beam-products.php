<?php
/*
Plugin Name: BeAM - Products
Plugin URI: http://www.and-digital.fr
Description: Manage products
Version: 1.0.0
Author: Agence AND
Author URI: http://www.and-digital.fr
*/
define( 'BEAM_PRODUCTS_SETTINGS_OPTION_GROUP', 'beamproductsSettings' );
define( 'BEAM_PRODUCTS_SETTINGS_OPTION_NAME', 'beam_products_settings' );

include( plugin_dir_path( __FILE__ ) . 'inc/custom-posts.php' );