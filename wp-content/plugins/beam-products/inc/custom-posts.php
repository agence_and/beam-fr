<?php
function beam_products_custom_post_products()
{
    $labels = array(
        'name'               	=> __( 'Products', BEAM_PRODUCTS_SETTINGS_OPTION_NAME ),
        'singular_name'      	=> __( 'Product', BEAM_PRODUCTS_SETTINGS_OPTION_NAME ),
        'add_new'            	=> __( 'Add a product', BEAM_PRODUCTS_SETTINGS_OPTION_NAME ),
        'add_new_item'       	=> __( 'Add a product' ),
        'new_item'           	=> __( 'New product' ),
        'edit_item'          	=> __( 'Modify the product' ),
        'view_item'          	=> __( 'See the product' ),
        'all_items'          	=> __( 'All products' ),
        'search_items'       	=> __( 'Search products' ),
        'parent_item_colon'  	=> __( 'Parent product :' ),
        'not_found'          	=> __( 'No product found' ),
        'not_found_in_trash' 	=> __( 'No product found in trash' )
    );

    $args = array(
        'labels'             	=> $labels,
        'public'             	=> true,
        'show_ui'            	=> true,
        'menu_icon'       		=> 'dashicons-cart',
        'menu_position'			=> 20,
        'exclude_from_search' 	=> true,
        'hierarchical'       	=> false,
        'supports'          	=> array( 'title', 'revisions' ),
        'taxonomies'			=> array( 'products_category' ),
        'has_archive'        	=> false,
        'rewrite'            	=> array(
            'slug' 		=> 'produits',
            'withfront'	=> true,
        ),
        'query_var'          	=> true,
    );

    register_post_type( 'products', $args );
}


function _beam_products_init_custom_posts()
{
    beam_products_custom_post_products();
}
add_action( 'init', '_beam_products_init_custom_posts' );