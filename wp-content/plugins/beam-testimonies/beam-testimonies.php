<?php
/*
Plugin Name: BeAM - Testimonies
Plugin URI: http://www.and-digital.fr
Description: Manage testimonies
Version: 1.0.0
Author: Agence AND
Author URI: http://www.and-digital.fr
*/
define( 'BEAM_TESTIMONIES_SETTINGS_OPTION_GROUP', 'beamtestimoniesSettings' );
define( 'BEAM_TESTIMONIES_SETTINGS_OPTION_NAME', 'beam_testimonies_settings' );

include( plugin_dir_path( __FILE__ ) . 'inc/custom-posts.php' );
include( plugin_dir_path( __FILE__ ) . 'inc/custom-taxonomies.php' );