<?php

function beam_testimonies_custom_taxonomy_category()
{
    $labels = array(
        'name'              			=> __( 'Testimonies types', BEAM_TESTIMONIES_SETTINGS_OPTION_NAME ),
        'singular_name'     			=> __( 'Testimonies type', BEAM_TESTIMONIES_SETTINGS_OPTION_NAME ),
        'search_items'      			=> __( 'Search a testimonies type' ),
        'all_items'         			=> __( 'All testimonies types' ),
        'parent_item'       			=> __( 'Parent testimonies type' ),
        'parent_item_colon'				=> __( 'Parent testimonies type :' ),
        'edit_item'         			=> __( 'Modify the testimonies type' ),
        'view_item'         			=> __( 'See' ),
        'update_item'       			=> __( 'Update testimonies type' ),
        'add_new_item'      			=> __( 'Add a new testimonies type' ),
        'new_item_name'     			=> __( 'Name of testimonies type' ),
        'popular_items'     			=> __( 'Popular testimonies types' ),
        'separate_items_with_commas'    => __( 'Separate testimonies types with comas' ),
        'add_or_remove_items'     		=> __( 'Add or remove testimonies type' ),
        'choose_from_most_used'     	=> __( 'Choose from most used' ),
        'not_found'     				=> __( 'No testimonies type found' ),
        'menu_name'         			=> __( 'Testimonies type' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array(
            'slug' 			=> 'testimonies_category',
            'withfront'		=> false,
            'hierarchical'	=> false,
        ),
    );

    register_taxonomy( 'testimonies_category', array( 'testimonies' ), $args );
}


function _beam_testimonies_custom_taxonomies()
{
    beam_testimonies_custom_taxonomy_category();
}
add_action( 'init', '_beam_testimonies_custom_taxonomies', 0 );