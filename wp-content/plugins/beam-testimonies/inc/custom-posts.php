<?php
function beam_testimonies_custom_post_testimonies()
{
    $labels = array(
        'name'               	=> __( 'Testimonies', BEAM_TESTIMONIES_SETTINGS_OPTION_NAME ),
        'singular_name'      	=> __( 'Testimony', BEAM_TESTIMONIES_SETTINGS_OPTION_NAME ),
        'add_new'            	=> __( 'Add a testimony', BEAM_TESTIMONIES_SETTINGS_OPTION_NAME ),
        'add_new_item'       	=> __( 'Add a testimony' ),
        'new_item'           	=> __( 'New testimony' ),
        'edit_item'          	=> __( 'Modify the testimony' ),
        'view_item'          	=> __( 'See the testimony' ),
        'all_items'          	=> __( 'All testimonies' ),
        'search_items'       	=> __( 'Search testimonies' ),
        'parent_item_colon'  	=> __( 'Parent testimony :' ),
        'not_found'          	=> __( 'No testimony found' ),
        'not_found_in_trash' 	=> __( 'No testimony found in trash' )
    );

    $args = array(
        'labels'             	=> $labels,
        'public'             	=> true,
        'show_ui'            	=> true,
        'menu_icon'       		=> 'dashicons-star-filled',
        'menu_position'			=> 20,
        'exclude_from_search' 	=> true,
        'hierarchical'       	=> false,
        'supports'          	=> array( 'title' ),
        'taxonomies'			=> array( 'testimonies_category' ),
        'has_archive'        	=> false,
        'rewrite'            	=> array(
            'slug' 		=> 'testimonies',
            'withfront'	=> true,
        ),
        'query_var'          	=> true,
    );

    register_post_type( 'testimonies', $args );
}


function _beam_testimonies_init_custom_posts()
{
    beam_testimonies_custom_post_testimonies();
}
add_action( 'init', '_beam_testimonies_init_custom_posts' );