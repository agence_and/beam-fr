<?php
/*
Plugin Name: BeAM - Partners
Plugin URI: http://www.and-digital.fr
Description: Manage partners
Version: 1.0.0
Author: Agence AND
Author URI: http://www.and-digital.fr
*/
define( 'BEAM_PARTNERS_SETTINGS_OPTION_GROUP', 'beampartnersSettings' );
define( 'BEAM_PARTNERS_SETTINGS_OPTION_NAME', 'beam_partners_settings' );

include( plugin_dir_path( __FILE__ ) . 'inc/custom-posts.php' );
include( plugin_dir_path( __FILE__ ) . 'inc/custom-taxonomies.php' );