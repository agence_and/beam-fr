<?php
function beam_partners_custom_post_partners()
{
    $labels = array(
        'name'               	=> __( 'Partners', BEAM_PARTNERS_SETTINGS_OPTION_NAME ),
        'singular_name'      	=> __( 'Partner', BEAM_PARTNERS_SETTINGS_OPTION_NAME ),
        'add_new'            	=> __( 'Add a partner', BEAM_PARTNERS_SETTINGS_OPTION_NAME ),
        'add_new_item'       	=> __( 'Add a partner' ),
        'new_item'           	=> __( 'New partner' ),
        'edit_item'          	=> __( 'Modify the partner' ),
        'view_item'          	=> __( 'See the partner' ),
        'all_items'          	=> __( 'All partners' ),
        'search_items'       	=> __( 'Search partners' ),
        'parent_item_colon'  	=> __( 'Parent partner :' ),
        'not_found'          	=> __( 'No partner found' ),
        'not_found_in_trash' 	=> __( 'No partner found in trash' )
    );

    $args = array(
        'labels'             	=> $labels,
        'public'             	=> true,
        'show_ui'            	=> true,
        'menu_icon'       		=> 'dashicons-networking',
        'menu_position'			=> 20,
        'exclude_from_search' 	=> true,
        'hierarchical'       	=> false,
        'supports'          	=> array( 'title', 'thumbnail' ),
        'taxonomies'			=> array( 'partners_category' ),
        'has_archive'        	=> false,
        'rewrite'            	=> array(
            'slug' 		=> 'partenaires',
            'withfront'	=> true,
        ),
        'query_var'          	=> true,
    );

    register_post_type( 'partners', $args );
}


function _beam_partners_init_custom_posts()
{
    beam_partners_custom_post_partners();
}
add_action( 'init', '_beam_partners_init_custom_posts' );