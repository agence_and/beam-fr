<?php

function beam_partners_custom_taxonomy_category()
{
    $labels = array(
        'name'              			=> __( 'Partners types', BEAM_PARTNERS_SETTINGS_OPTION_NAME ),
        'singular_name'     			=> __( 'Partners type', BEAM_PARTNERS_SETTINGS_OPTION_NAME ),
        'search_items'      			=> __( 'Search a partners type' ),
        'all_items'         			=> __( 'All partners types' ),
        'parent_item'       			=> __( 'Parent partners type' ),
        'parent_item_colon'				=> __( 'Parent partners type :' ),
        'edit_item'         			=> __( 'Modify the partners type' ),
        'view_item'         			=> __( 'See' ),
        'update_item'       			=> __( 'Update partners type' ),
        'add_new_item'      			=> __( 'Add a new partners type' ),
        'new_item_name'     			=> __( 'Name of partners type' ),
        'popular_items'     			=> __( 'Popular partners types' ),
        'separate_items_with_commas'    => __( 'Separate partners types with comas' ),
        'add_or_remove_items'     		=> __( 'Add or remove partners type' ),
        'choose_from_most_used'     	=> __( 'Choose from most used' ),
        'not_found'     				=> __( 'No partners type found' ),
        'menu_name'         			=> __( 'Partners type' ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array(
            'slug' 			=> 'partners_category',
            'withfront'		=> false,
            'hierarchical'	=> false,
        ),
    );

    register_taxonomy( 'partners_category', array( 'partners' ), $args );
}


function _beam_partners_custom_taxonomies()
{
    beam_partners_custom_taxonomy_category();
}
add_action( 'init', '_beam_partners_custom_taxonomies', 0 );