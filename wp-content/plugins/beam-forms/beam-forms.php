<?php
/*
Plugin Name: BeAM - Forms
Plugin URI: https://www.and-digital.fr
Description: Manage forms
Version: 1.0.0
Author: Agence AND
Author URI: https://www.and-digital.fr
*/
define('BEAM_FORMS_TABLE_CONTACT', 'form_contact');
define('BEAM_FORMS_TABLE_PRESS', 'form_press');
define('BEAM_FORMS_TABLE_APPLICATION', 'form_application');

define('BEAM_FORMS_SETTINGS_OPTION_GROUP', 'beamformsSettings');
define('BEAM_FORMS_SETTINGS_OPTION_NAME', 'beam_forms_settings');

define( 'BEAM_FORMS_UPLOAD_FOLDER_NAME', 'beam-apply-attachments' );
define( 'BEAM_FORMS_ATTACHMENT_MAX_SIZE', '1' ); // Mo.

include(plugin_dir_path(__FILE__) . 'inc/ajax.php');
include(plugin_dir_path(__FILE__) . 'inc/helpers.php');
include(plugin_dir_path(__FILE__) . 'inc/define.php');

include(plugin_dir_path(__FILE__) . 'inc/front/contactmini.php');
include(plugin_dir_path(__FILE__) . 'inc/front/contact.php');
include(plugin_dir_path(__FILE__) . 'inc/front/press.php');
include(plugin_dir_path(__FILE__) . 'inc/front/application.php');
include(plugin_dir_path(__FILE__) . 'inc/front/spontaneousapplication.php');

if (is_admin()) {
    include(plugin_dir_path(__FILE__) . 'inc/admin/export.php');
    include(plugin_dir_path(__FILE__) . 'inc/admin/settings.php');
    include(plugin_dir_path(__FILE__) . 'inc/admin/stats.php');
}

// ACTIVATE ************************************************************************************************************************

function _beam_forms_activate()
{
    // Create folder to store attachments

    $upload_dir = beam_forms_get_upload_dir();

    if ( !is_dir($upload_dir) )
    {
        mkdir( $upload_dir, 0700 );
    }

    // Create files to secure folders

    // Copy "Silence is golden" file

    copy( WP_CONTENT_DIR . '/index.php', $upload_dir . '/index.php' );

    // Create .htaccess file

    $htaccess = fopen( $upload_dir . '/.htaccess', 'w' );
    fwrite( $htaccess, 'deny from all' );
    fclose( $htaccess );



    // Create table to store requests
    // See https://codex.wordpress.org/Creating_Tables_with_Plugins

    global $wpdb;

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $sql = "CREATE TABLE IF NOT EXISTS " . BEAM_FORMS_TABLE_CONTACT . " (
			id INT(10) unsigned NOT NULL AUTO_INCREMENT,
			fullname VARCHAR(255) NOT NULL,
			phone VARCHAR(255) NOT NULL,
			email VARCHAR(255) NOT NULL,
			country VARCHAR(255) NOT NULL,
			object VARCHAR(255) NOT NULL,
            message TEXT NOT NULL,
            record_ip VARCHAR(50) NOT NULL,
            record_date DATETIME NOT NULL,
            PRIMARY KEY  (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store contact datas from website' AUTO_INCREMENT=1;";
	dbDelta($sql);

    $sql = "CREATE TABLE IF NOT EXISTS " . BEAM_FORMS_TABLE_APPLICATION . " (
            id INT(10) unsigned NOT NULL AUTO_INCREMENT,
            offer VARCHAR(255) NOT NULL,
            fullname VARCHAR(255) NOT NULL,
            phone VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL,
            country VARCHAR(255) NOT NULL,
            job VARCHAR(255) NOT NULL,
            message TEXT NOT NULL,
            record_ip VARCHAR(50) NOT NULL,
            record_date DATETIME NOT NULL,
            PRIMARY KEY  (id)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store application form datas from website' AUTO_INCREMENT=1;";
	dbDelta($sql);

    $sql = "CREATE TABLE IF NOT EXISTS " . BEAM_FORMS_TABLE_PRESS . " (
            id INT(10) unsigned NOT NULL AUTO_INCREMENT,
            fullname VARCHAR(255) NOT NULL,
            media VARCHAR(255) NOT NULL,
            phone VARCHAR(255) NOT NULL,
            email VARCHAR(255) NOT NULL,
            message TEXT NOT NULL,
            record_ip VARCHAR(50) NOT NULL,
            record_date DATETIME NOT NULL,
            PRIMARY KEY  (id)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store contact press datas from website' AUTO_INCREMENT=1;";
	dbDelta($sql);

    add_option('beam_forms_db_version', '1.1');
}

register_activation_hook(__FILE__, '_beam_forms_activate');


// JS & CSS ************************************************************************************************************************

function _beam_forms_js_css()
{
    wp_enqueue_script(
        'beam-forms-js',
        plugins_url('js/front.js', __FILE__),
        array('jquery'),
        null,
        true
    );

    // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
    // see http://codex.wordpress.org/AJAX_in_Plugins
    wp_localize_script('beam-forms-js', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('wp_enqueue_scripts', '_beam_forms_js_css');