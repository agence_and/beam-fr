jQuery(document).ready(function ($) {

    $('form#form-contactmini').validator({
        disable: false,
    }).on('submit', function (e) {
            var form = $(this);

            var btn = form.find('input[type=submit]');

            btn.button('loading');

            form.find('.custom-alert.custom-error').hide();


            if (e.isDefaultPrevented()) {
                form.find('.custom-alert.custom-error').show();
                if (form.find('textarea[name=g-recaptcha-response]').val() == '') {
                    form.find('.g-recaptcha').addClass('error');
                }

                btn.button('reset');
            }
            else if (form.find('textarea[name=g-recaptcha-response]').val() == '') {
                form.find('.custom-alert.custom-error').show();
                form.find('.g-recaptcha').addClass('error');

                btn.button('reset');

                return false;
            }
            else {
                var name = form.find('input[name=contactmini_name]').val();
                var phone = form.find('input[name=contactmini_phone]').val();
                var email = form.find('input[name=contactmini_email]').val();
                var country = form.find('select[name=contactmini_country]').find(":selected").val();
                var object = form.find('input[name=contactmini_object]').val();
                var to = form.find('input[name=contactmini_to]').val();
                var message = form.find('textarea[name=contactmini_message]').val();

                var recaptcha = form.find('textarea[name=g-recaptcha-response]').val();
                var nonce = form.find('input#beam_contactmini_form_submit_nonce').val();

                var datas = {};
                datas['action'] = 'beam_forms_contactmini_form_ajax'; // see http://codex.wordpress.org/AJAX_in_Plugins
                datas['name'] = name;
                datas['phone'] = phone;
                datas['email'] = email;
                datas['country'] = country;
                datas['object'] = object;
                datas['to'] = to;
                datas['message'] = message;
                datas['g-recaptcha-response'] = recaptcha;
                datas['beam_contactmini_form_submit_nonce'] = nonce;

                $.post(
                    ajax_object.ajax_url,
                    datas,
                    function (data) {
                        btn.hide().button('reset');

                        if (data.error == false) {
                            form.find('input,textarea,select').val('');

                            form.find('.custom-alert.custom-success').show().delay(1000).fadeOut(2000, function () {
                                form.find('.custom-alert.custom-success').hide();
                                grecaptcha.reset(recaptchaWidgets[form.find('.g-recaptcha').attr('id')]);
                                btn.show();
                            });
                        }
                        else {
                            form.find('.custom-alert.custom-error').show();
                            btn.show();
                        }
                    },
                    "json"
                );

                return false;
            }
        }
    );


    $('form#form-contact').validator({
        disable: false,
    }).on('submit', function (e) {
            var form = $(this);

            var btn = form.find('input[type=submit]');

            btn.button('loading');

            form.find('.custom-alert.custom-error').hide();


            if (e.isDefaultPrevented()) {
                form.find('.custom-alert.custom-error').show();
                if (form.find('textarea[name=g-recaptcha-response]').val() == '') {
                    form.find('.g-recaptcha').addClass('error');
                }

                btn.button('reset');
            }
            else if (form.find('textarea[name=g-recaptcha-response]').val() == '') {
                form.find('.custom-alert.custom-error').show();
                form.find('.g-recaptcha').addClass('error');

                btn.button('reset');

                return false;
            }
            else {
                var name = form.find('input[name=contact_name]').val();
                var phone = form.find('input[name=contact_phone]').val();
                var email = form.find('input[name=contact_email]').val();
                var country = form.find('select[name=contact_country]').find(":selected").val();
                var object = form.find('input[name=contact_object]').val();
                var to = form.find('input[name=contact_to]').val();
                var message = form.find('textarea[name=contact_message]').val();

                var recaptcha = form.find('textarea[name=g-recaptcha-response]').val();
                var nonce = form.find('input#beam_contact_form_submit_nonce').val();

                var datas = {};
                datas['action'] = 'beam_forms_contact_form_ajax'; // see http://codex.wordpress.org/AJAX_in_Plugins
                datas['name'] = name;
                datas['phone'] = phone;
                datas['email'] = email;
                datas['country'] = country;
                datas['object'] = object;
                datas['to'] = to;
                datas['message'] = message;
                datas['g-recaptcha-response'] = recaptcha;
                datas['beam_contact_form_submit_nonce'] = nonce;

                $.post(
                    ajax_object.ajax_url,
                    datas,
                    function (data) {
                        btn.hide().button('reset');

                        if (data.error == false) {
                            form.find('input,textarea,select').val('');

                            form.find('.custom-alert.custom-success').show().delay(1000).fadeOut(2000, function () {
                                form.find('.custom-alert.custom-success').hide();
                                grecaptcha.reset(recaptchaWidgets[form.find('.g-recaptcha').attr('id')]);
                                btn.show();
                            });
                        }
                        else {
                            form.find('.custom-alert.custom-error').show();
                            btn.show();
                        }
                    },
                    "json"
                );

                return false;
            }
        }
    );

    $('form#form-press').validator({
        disable: false,
    }).on('submit', function (e) {
            var form = $(this);

            var btn = form.find('input[type=submit]');

            btn.button('loading');

            form.find('.custom-alert.custom-error').hide();


            if (e.isDefaultPrevented()) {
                form.find('.custom-alert.custom-error').show();
                if (form.find('textarea[name=g-recaptcha-response]').val() == '') {
                    form.find('.g-recaptcha').addClass('error');
                }

                btn.button('reset');
            }
            else if (form.find('textarea[name=g-recaptcha-response]').val() == '') {
                form.find('.custom-alert.custom-error').show();
                form.find('.g-recaptcha').addClass('error');

                btn.button('reset');

                return false;
            }
            else {
                var name = form.find('input[name=press_name]').val();
                var phone = form.find('input[name=press_phone]').val();
                var email = form.find('input[name=press_email]').val();
                var media = form.find('input[name=press_media]').val();
                var message = form.find('textarea[name=press_message]').val();
                var recaptcha = form.find('textarea[name=g-recaptcha-response]').val();
                var nonce = form.find('input#beam_press_form_submit_nonce').val();

                var datas = {};
                datas['action'] = 'beam_forms_press_form_ajax'; // see http://codex.wordpress.org/AJAX_in_Plugins
                datas['name'] = name;
                datas['phone'] = phone;
                datas['email'] = email;
                datas['media'] = media;
                datas['message'] = message;
                datas['g-recaptcha-response'] = recaptcha;
                datas['beam_press_form_submit_nonce'] = nonce;

                $.post(
                    ajax_object.ajax_url,
                    datas,
                    function (data) {
                        btn.hide().button('reset');

                        if (data.error == false) {
                            form.find('input,textarea,select').val('');

                            form.find('.custom-alert.custom-success').show().delay(1000).fadeOut(2000, function () {
                                form.find('.custom-alert.custom-success').hide();
                                grecaptcha.reset(recaptchaWidgets[form.find('.g-recaptcha').attr('id')]);
                                btn.show();
                            });
                        }
                        else {
                            form.find('.custom-alert.custom-error').show();
                            btn.show();
                        }
                    },
                    "json"
                );

                return false;
            }
        }
    );




    // Apply form

    $('.form-application').validator({
        disable: false,
    })
        .on('submit',

            function(e)
            {

                var form = $(this);
                var btn = form.find('input[type=submit]');

                btn.button('loading');
                btn.prop('disabled', true);

                if (e.isDefaultPrevented())
                {
                    form.find('.custom-alert.custom-error').removeClass('hidden');
                    btn.button('reset');
                    btn.prop('disabled', false);
                    return false;
                }
                else if (form.find('textarea[name=g-recaptcha-response]').val() == '') {
                    form.find('.custom-alert.custom-error').removeClass('hidden');
                    form.find('.g-recaptcha').addClass('error');
                    btn.button('reset');
                    btn.prop('disabled', false);
                    return false;
                }
                else
                {
                    // everything looks good!
                }
            }

        );



});