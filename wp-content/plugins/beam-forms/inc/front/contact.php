<?php
function beam_contact_form($object = '', $recipient = '')
{
    global $beam_contact_form_errors;

    global $BEAM_CONTACT_COUNTRY;

    $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );

    $domain = BEAM_FORMS_SETTINGS_OPTION_NAME;
    ?>
    <form name="form-contact" method="post" id="form-contact" novalidate class="forms form-contact">

        <input type="hidden" name="contact_to" value="<?php echo esc_attr( $recipient); ?>">
        <input type="hidden" name="contact_object" value="<?php echo esc_attr( $object ); ?>">
        <?php wp_nonce_field( 'beam_contact_form_submit', 'beam_contact_form_submit_nonce' ); ?>



        <?php
        $FORM = array();

        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contact_name',
                'label'	        => __( 'Nom / Prénom', $domain ),
                'type'			=> 'text',
                'required'		=> true,
//                'placeholder'   => __( 'Your name', $domain ),
            ),

        );

        $FORM[] = array(

            array(
                'row'			=> 'first',
                'class'			=> '',
                'id'			=> 'contact_phone',
                'label'	        => __( 'Téléphone', $domain ),
                'type'			=> 'text',
                'required'		=> true,
//                'placeholder'   => __( '+1...', $domain ),
            ),

        );


        $FORM[] = array(

            array(
                'row'			=> 'last',
                'class'			=> '',
                'id'			=> 'contact_email',
                'label'	        => __( 'Email', $domain ),
                'type'			=> 'email',
                'required'		=> true,
//                'placeholder'   => __( 'Your email', $domain ),
            ),

        );


        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contact_country',
                'label'	        => __( 'Pays', $domain ),
                'type'			=> 'select',
                'required'		=> true,
                'placeholder'   => __( 'Sélectionner', $domain ),
                'choices'		=> $BEAM_CONTACT_COUNTRY,
            ),

        );

        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contact_message',
                'label'	        => __( 'Votre demande', $domain ),
                'type'			=> 'textarea',
                'required'		=> true,
//                'placeholder'   => __( 'Start writing...', $domain ),
            ),

        );

        $contactform = new And_Flex_Form_Api( $FORM );
        $contactform->display_form( $beam_contact_form_errors );
        ?>


        <div class="custom-alert custom-error"<?php echo ( count( $beam_contact_form_errors ) == 0 ? ' style="display: none;"' : '' ); ?>>
            <p><?php echo nl2br( $options['beam_forms_settings_contact_alert_error'] ); ?></p>
        </div>

        <div class="custom-alert custom-success" style="display: none;">
            <p><?php echo nl2br( $options['beam_forms_settings_contact_alert_ok'] ); ?></p>
        </div>


        <div class="form-row form-row-submit">
            <div class="form-group">
                <div class="g-recaptcha" id="recaptcha-contact" data-sitekey="<?php echo esc_attr( $options['beam_forms_settings_recaptcha_site_key'] ); ?>"></div>
            </div>

            <div class="form-group btn-ctn">
                <div class="form-notice">
                    <span class="required">*</span>
                    <?php esc_html_e('champs obligatoires', 'beam'); ?>
                </div>
                <input type="submit" class="btn btn-grey" data-loading-text="<?php esc_attr_e( 'Envoi ...', $domain ); ?>" value="<?php esc_html_e('Envoyer', $domain); ?>"/>
            </div>
        </div>

    </form>

    <!--  <div class="form-notice">
        <p><?php /*_e('Les informations que vous nous avez transmises sont nécessaires à la société Beam pour les traitements informatiques liés à votre information et à la gestion de votre demande.', 'beam'); */?><?php /*_e('Vous disposez d’un droit d’opposition, d’accès, de rectification et de suppression que vous pouvez exercer par courriel auprès de ​​​​contact@beam.org', 'beam'); */?></p>
    </div>-->
    <?php
}