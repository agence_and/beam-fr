<?php
function beam_contactmini_form($object = '', $recipient = '')
{
    global $beam_contactmini_form_errors;
	
	global $BEAM_CONTACT_COUNTRY;

    $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );

    $domain = BEAM_FORMS_SETTINGS_OPTION_NAME;
    ?>
    <form name="form-contactmini" method="post" id="form-contactmini" novalidate class="forms form-contact form-contact--mini">

        <input type="hidden" name="contactmini_to" value="<?php echo esc_attr( $recipient); ?>">
        <input type="hidden" name="contactmini_object" value="<?php echo esc_attr( $object ); ?>">
		<?php wp_nonce_field( 'beam_contactmini_form_submit', 'beam_contactmini_form_submit_nonce' ); ?>


        <?php
        $FORM = array();

        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contactmini_name',
                'label'	        => __( 'Nom', $domain ),
                'type'			=> 'text',
                'required'		=> true,
//                'placeholder'   => __( 'First name Last name', $domain ),
            ),

        );

        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contactmini_phone',
                'label'	        => __( 'Téléphone', $domain ),
                'type'			=> 'text',
                'required'		=> true,
//                'placeholder'   => __( '+33(0)6 00 00 00 00', $domain ),
            ),

        );


        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contactmini_email',
                'label'	        => __( 'Email', $domain ),
                'type'			=> 'email',
                'required'		=> true,
//                'placeholder'   => __( 'example@mail.com', $domain ),
            ),

        );


        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contactmini_country',
                'label'	        => __( 'Country', $domain ),
                'type'			=> 'select',
                'required'		=> true,
                'placeholder'   => __( 'Pays', $domain ),
                'choices'		=> $BEAM_CONTACT_COUNTRY,
            ),

        );

        $FORM[] = array(

            array(
                'row'			=> 'firstlast',
                'class'			=> '',
                'id'			=> 'contactmini_message',
                'label'	        => __( 'Message', $domain ),
                'type'			=> 'textarea',
                'required'		=> true,
//                'placeholder'   => __( 'Start writing...', $domain ),
            ),

        );

        $contactformmini = new And_Flex_Form_Api( $FORM );
        $contactformmini->display_form( $beam_contactmini_form_errors );
        ?>


        <div class="custom-alert custom-error"<?php echo ( count( $beam_contactmini_form_errors ) == 0 ? ' style="display: none;"' : '' ); ?>>
            <p><?php echo nl2br( $options['beam_forms_settings_contact_alert_error'] ); ?></p>
        </div>

        <div class="custom-alert custom-success" style="display: none;">
            <p><?php echo nl2br( $options['beam_forms_settings_contact_alert_ok'] ); ?></p>
        </div>


        <div class="form-row form-row-submit">
            <div class="form-group">
                <div class="g-recaptcha" id="recaptcha-contactmini" data-sitekey="<?php echo esc_attr( $options['beam_forms_settings_recaptcha_site_key'] ); ?>" data-size="compact"></div>
            </div>

            <div class="form-group btn-ctn">
                <div class="form-notice">
                    <span class="required">*</span>
                    <?php esc_html_e('champs obligatoires', 'beam'); ?>
                </div>
                <input type="submit" class="btn btn-grey" data-loading-text="<?php esc_attr_e( 'Envoi...', $domain ); ?>" value="<?php esc_html_e('Envoyer votre message', $domain); ?>"/>
            </div>
        </div>

    </form>

  <!--  <div class="form-notice">
        <p><?php /*_e('Les informations que vous nous avez transmises sont nécessaires à la société Beam pour les traitements informatiques liés à votre information et à la gestion de votre demande.', 'beam'); */?><?php /*_e('Vous disposez d’un droit d’opposition, d’accès, de rectification et de suppression que vous pouvez exercer par courriel auprès de ​​​​contact@beam.org', 'beam'); */?></p>
    </div>-->
    <?php
}