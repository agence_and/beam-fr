<?php
function beam_application_form($offer = '', $offerID = '')
{
    global $beam_application_form_errors;


    $options = get_option(BEAM_FORMS_SETTINGS_OPTION_NAME);

    $domain = BEAM_FORMS_SETTINGS_OPTION_NAME;

    ?>

    <form enctype="multipart/form-data" name="form-application" method="post" id="form-application" novalidate
          class="forms form-application">

        <input type="hidden" name="id" id="id" value="<?php echo esc_attr($offerID); ?>">
        <input type="hidden" name="back" id="back" value="<?php the_ID(); ?>">
        <input type="hidden" name="application_offer" value="<?php echo esc_attr($offer); ?>"
               id="application-offer">
        <?php echo wp_nonce_field('beam_forms_form_application_submit', 'beam_forms_form_application_submit_nonce'); ?>

        <?php
        if (isset($_GET['confirm'])) {
            ?>
            <div class="custom-alert custom-success">
                <p><?php echo nl2br($options['beam_forms_settings_application_alert_ok']); ?></p>
            </div>
            <?php
        } else {
            ?>
            <div class="custom-alert custom-error <?php echo(count($beam_application_form_errors) == 0 ? ' hidden' : ''); ?>">
                <p><?php echo nl2br($options['beam_forms_settings_application_alert_error']);
                    ?></p>
            </div>

            <?php
        }
        ?>


        <?php
        $FORM = array();

        $FORM[] = array(

            array(
                'row' => 'firstlast',
                'class' => '',
                'id' => 'application_name',
                'label' => __('Nom', $domain),
                'type' => 'text',
                'required' => true,
//                'placeholder' => __('Your name', $domain),
            ),

        );

        $FORM[] = array(

            array(
                'row' => 'first',
                'class' => '',
                'id' => 'application_phone',
                'label' => __('Téléphone', $domain),
                'type' => 'text',
                'required' => true,
//                'placeholder' => __('+1...', $domain),
            ),

        );

        $FORM[] = array(

            array(
                'row' => 'last',
                'class' => '',
                'id' => 'application_email',
                'label' => __('Email', $domain),
                'type' => 'email',
                'required' => true,
//                'placeholder' => __('Your email', $domain),
            ),

        );

        $FORM[] = array(

            array(
                'row' => 'firstlast',
                'class' => '',
                'id' => 'application_resume',
                'label' => __('CV', $domain),
                'type' => 'file_advanced',
                'required' => true,
                'placeholder' => __('Téléchargez votre CV (PDF,' . BEAM_FORMS_ATTACHMENT_MAX_SIZE . ' Mo maximum) ', $domain),
            ),

        );

        $FORM[] = array(

            array(
                'row' => 'firstlast',
                'class' => '',
                'id' => 'application_message',
                'label' => __('Lettre de motivation', $domain),
                'type' => 'textarea',
                'required' => true,
//                'placeholder' => __('Insert your cover letter here', $domain),
            ),

        );

        $applicationform = new And_Flex_Form_Api($FORM);
        $applicationform->display_form($beam_application_form_errors);
        ?>


        <div class="form-row form-row-submit">
            <div class="form-group">
                <div class="g-recaptcha" id="recaptcha-application"
                     data-sitekey="<?php echo esc_attr($options['beam_forms_settings_recaptcha_site_key']); ?>"></div>
            </div>

            <div class="form-group btn-ctn">
                <div class="form-notice">
                    <span class="required">*</span>
                    <?php esc_html_e('champs obligatoires', 'beam'); ?>
                </div>
                <input type="submit" class="btn btn-grey"
                       data-loading-text="<?php esc_attr_e('Envoi ...', $domain); ?>"
                       value="<?php esc_html_e('Envoyer', $domain); ?>"
                       autocomplete="off"/>
            </div>
        </div>

    </form>

    <?php

}


function _beam_forms_form_application_submit()
{
    global $beam_application_form_errors;
    global $wpdb;

    if (isset($_POST['beam_forms_form_application_submit_nonce'])
        && wp_verify_nonce($_POST['beam_forms_form_application_submit_nonce'], 'beam_forms_form_application_submit')
    ) {
        // Check reCAPTCHA

        require_once ABSPATH . 'vendor/autoload.php';

        $options = get_option(BEAM_FORMS_SETTINGS_OPTION_NAME);

        $recaptcha = new \ReCaptcha\ReCaptcha($options['beam_forms_settings_alert_secret_key']);

        $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

        if (!$resp->isSuccess()) {
            $beam_application_form_errors[] = 'recaptcha';
        }

        // Check datas

        if (!isset($_POST['application_offer']) || $_POST['application_offer'] == '') $beam_application_form_errors[] = 'application_offer';
        else $offer = stripslashes(sanitize_text_field($_POST['application_offer']));

        if (!isset($_POST['application_name']) || $_POST['application_name'] == '') $beam_application_form_errors[] = 'application_name';
        else $name = stripslashes(sanitize_text_field($_POST['application_name']));

        if (!isset($_POST['application_phone']) || $_POST['application_phone'] == '') $beam_application_form_errors[] = 'application_phone';
        else $phone = stripslashes(sanitize_text_field($_POST['application_phone']));

        if (!isset($_POST['application_email']) || !is_email($_POST['application_email'])) $beam_application_form_errors[] = 'application_email';
        else $email = stripslashes(sanitize_email($_POST['application_email']));

        if (!isset($_POST['application_message']) || $_POST['application_message'] == '') $beam_application_form_errors[] = 'application_message';
        else $message = stripslashes(sanitize_text_field($_POST['application_message']));

//        $message = stripslashes(sanitize_text_field($_POST['application_message'])); // not required


        // Upload files
        $record_id = $wpdb->insert_id;
        $resume = beam_forms_helpers_upload_file('application_resume', $record_id, $beam_application_form_errors);
//            $letter = beam_forms_helpers_upload_file( 'application_letter', $record_id, $beam_application_form_errors );

        $file_parts = pathinfo($resume);

        switch($file_parts['extension'])
        {
            case "pdf":
                break;

            default:
                $beam_application_form_errors[] = 'application_resume';
        }



        if (sizeof($beam_application_form_errors) == 0) {
            // Save datas in database

            $datas_array = array(
                'offer' => $offer,
                'fullname' => $name,
                'phone' => $phone,
                'email' => $email,
                'message' => $message,
                'record_ip' => ($_SERVER['HTTP_X_FORWARDED_FOR'] != '' ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']),
                'record_date' => current_time('mysql', 1),
            );
            $format_array = array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            );
            $wpdb->insert(BEAM_FORMS_TABLE_APPLICATION, $datas_array, $format_array);

//            $record_id = $wpdb->insert_id;

            // Send mail

            $message = '<p>Hello,</p>
				<p>A visitor has just sent you the following message from the website.</p>
					
				<p>
				Offre : ' . esc_html($offer) . '<br>
				Nom : ' . esc_html($name) . '<br>
				Téléphone : ' . esc_html($phone) . '<br>
				Email : ' . esc_html($email) . '
				</p>
				
	
				' . ($message != '' ? '<p>Lettre de motivation :</p>
				
				<p>' . nl2br(esc_html(stripslashes($_POST['application_message']))) . '</p>' : '') . '';

            $headers[] = 'From: BeAM Machines - Site web <' . $options['beam_forms_settings_contact_email_to'] . '>';
            $headers[] = 'Content-Type: text/html; charset=UTF-8';

            $email_object = 'BeAM - Candidature envoyée depuis le site';

            $attachments = array();
            
            if ($resume != '') $attachments[] = $resume;
//            if( $letter != '' ) $attachments[] = $letter;

            wp_mail($options['beam_forms_settings_apply_email_to'], $email_object, $message, $headers, $attachments);


            // redirect to confirmation message

            wp_redirect(get_permalink((int)$_POST['back']) . '?offerID='.$_POST['id'].'&confirm');

            die();
        }
    }
}

add_action('init', '_beam_forms_form_application_submit');