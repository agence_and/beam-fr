<?php
	// Add submenu page in admin

	function _beam_forms_add_submenu_page_in_tools()
	{
		add_submenu_page( 'tools.php', '
Form statistics', '
Form statistics', 'manage_options', 'forms-stats', '_beam_forms_stats');
	}
	add_action( 'admin_menu', '_beam_forms_add_submenu_page_in_tools' );
	
	function _beam_forms_stats()
	{
		global $wpdb;
	?>
	<div class="wrap">
		<h2>
            FORM STATISTICS</h2>
        
        <?php
			$wpdb->query( 'SELECT ID FROM ' . BEAM_FORMS_TABLE_CONTACT );
		?>			
        <p><strong>Contact</strong> : <?php echo $wpdb->num_rows; ?> | <a href="<?php echo wp_nonce_url( admin_url( 'tools.php?page=forms-stats&export=contact' ), 'export_contact', 'export_contact_nonce' ); ?>">Export</a></p>

        <?php
        $wpdb->query( 'SELECT ID FROM ' . BEAM_FORMS_TABLE_PRESS );
        ?>
        <p><strong>Press contact</strong> : <?php echo $wpdb->num_rows; ?> | <a href="<?php echo wp_nonce_url( admin_url( 'tools.php?page=forms-stats&export=press' ), 'export_press', 'export_press_nonce' ); ?>">Export</a></p>
        
        <?php
			$wpdb->query( 'SELECT ID FROM ' . BEAM_FORMS_TABLE_APPLICATION );
		?>
        <p><strong>Job application</strong> : <?php echo $wpdb->num_rows; ?> | <a href="<?php echo wp_nonce_url( admin_url( 'tools.php?page=forms-stats&export=application' ), 'export_application', 'export_application_nonce' ); ?>">Export</a></p>

	</div>
	<?php
	}