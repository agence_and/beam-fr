<?php
function beam_forms_export_format_data($data)
{
    $data = utf8_decode($data);
    $data = stripslashes($data);
    $data = str_replace('&#8211;', '-', $data);
    $data = str_replace(';', ',', $data);
    $data = preg_replace("/\r|\n/", " ", $data);

    return $data;
}

function beam_forms_helpers_get_defined_text( $choices, $value )
{
    $text = '';
    foreach( $choices as $choice )
    {
        if( $choice['value'] == $value )
        {
            $text = $choice['text'];
        }
    }

    return $text;
}

function _beam_forms_export()
{
    if (is_admin()) {
        if ($_GET['page'] == 'forms-stats') {
            if (!empty($_GET['export'])) {
                if (!empty($_GET['export_' . $_GET['export'] . '_nonce']) && wp_verify_nonce($_GET['export_' . $_GET['export'] . '_nonce'], 'export_' . $_GET['export'])) {
                    global $wpdb;

                    global $BEAM_CONTACT_COUNTRY;
                    global $BEAM_JOB_INDUSTRY;

                    header('Content-Encoding: UTF-8');
                    header('Content-Type: text/csv; charset=UTF-8');
                    header("Content-Type: application/force-download");
                    header("Content-Type: application/octet-stream");
                    header("Content-Type: application/download");
                    header('Content-Disposition: attachment;filename=export-' . $_GET['export'] . '-' . date("YmdHis") . '.csv');
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    header('Content-Transfer-Encoding: binary');

                    switch ($_GET['export']) {
                        case 'contact' :
                            $TABLE = BEAM_FORMS_TABLE_CONTACT;
                            break;

                        case 'application' :
                            $TABLE = BEAM_FORMS_TABLE_APPLICATION;
                            break;

                        case 'press' :
                            $TABLE = BEAM_FORMS_TABLE_PRESS;
                            break;

                    }

                    $query = 'SELECT * FROM ' . $TABLE . ' ORDER BY ID DESC';
                    $results = $wpdb->get_results($query);

                    // Init header row

                    $header = array();
                    $header[] = 'RECORD ID';
                    $header[] = 'IP';
                    $header[] = 'DATE';
                    if ($_GET['export'] == 'contact') {
                        $header[] = 'OBJECT';
                    }
                    if ($_GET['export'] == 'application') {
                        $header[] = 'OFFER';
                    }


                    $header[] = 'FULLNAME';
                    $header[] = 'PHONE';
                    $header[] = 'EMAIL';


                    if ($_GET['export'] == 'press') {
                        $header[] = 'MEDIA';
                    }
                    if ($_GET['export'] == 'contact' || $_GET['export'] == 'application') {
                        $header[] = 'COUNTRY';
                    }

                    if ($_GET['export'] == 'application') {
                        $header[] = 'RESEARCHED JOB';
                    }

                    $header[] = 'MESSAGE';

                    // Init datas rows

                    $rows = array();

                    foreach ($results as $result) {
                        $row = array();

                        $row[] = $result->id;
                        $row[] = $result->record_ip;
                        $row[] = $result->record_date;

                        if ($_GET['export'] == 'contact') {
                            $row[] = beam_forms_export_format_data($result->object);
                        }

                        if ($_GET['export'] == 'application') {
                            $row[] = beam_forms_export_format_data($result->offer);
                        }

                        $row[] = beam_forms_export_format_data($result->fullname);
                        $row[] = beam_forms_export_format_data($result->phone);
                        $row[] = beam_forms_export_format_data($result->email);

                        if ($_GET['export'] == 'contact' || $_GET['export'] == 'application') {
                            $row[] = beam_forms_export_format_data($result->country);
                        }

                        if ($_GET['export'] == 'application') {
                            $row[] = beam_forms_export_format_data($result->job);
                        }


                        if ($_GET['export'] == 'press') {
                            $row[] = beam_forms_export_format_data($result->media);
                        }

                        $row[] = beam_forms_export_format_data($result->message);


                        $rows[] = join(';', $row);
                    }

                    // Output datas

                    echo join(';', $header);
                    echo "\n";

                    echo join("\n", $rows);

                    die();
                }
            }
        }
    }
}

add_action('init', '_beam_forms_export');