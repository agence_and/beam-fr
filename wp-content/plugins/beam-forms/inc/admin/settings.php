<?php
$datas = array(

    // CAPTCHA

    array(
        'id'		=> 'beam_forms_settings_recaptcha',
        'title'		=> __( 'RECAPTCHA', BEAM_FORMS_SETTINGS_OPTION_NAME ),
        'fields'	=> array(

            array(
                'id'		=> 'beam_forms_settings_recaptcha_site_key',
                'title'		=> __( 'Site key', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_alert_secret_key',
                'title'		=> __( 'Secret key', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'password',
            ),

        ),
    ),

    

    // GENERAL

    array(
        'id'		=> 'beam_forms_settings_contact',
        'title'		=> __( 'FORMS // GENERAL SETTINGS', BEAM_FORMS_SETTINGS_OPTION_NAME ),
        'fields'	=> array(

            array(
                'id'		=> 'beam_forms_settings_contact_email_to',
                'title'		=> __( 'Recipient', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_apply_email_to',
                'title'		=> __( 'Recipient - Apply form', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),


            array(
                'id'		=> 'beam_forms_settings_apply_spontaneous_corporate_email_to',
                'title'		=> __( 'Recipient - Spontaneous job application form - Job type Corporate', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_apply_spontaneous_business_development_email_to',
                'title'		=> __( 'Recipient - Spontaneous job application form - Job type Business Development', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_apply_spontaneous_process_technology_rd_email_to',
                'title'		=> __( 'Recipient - Spontaneous job application form - Job type Process Technology / R&D', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_apply_spontaneous_product_engineering_quality_email_to',
                'title'		=> __( 'Recipient - Spontaneous job application form - Job type Product Engineering & Quality', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_apply_spontaneous_operations_email_to',
                'title'		=> __( 'Recipient - Spontaneous job application form - Job type Operations', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_apply_spontaneous_email_to - Other',
                'title'		=> __( 'Recipient - Spontaneous job application form', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),



            array(
                'id'		=> 'beam_forms_settings_contactusa_email_to',
                'title'		=> __( 'Recipient - USA contact', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_contactfr_email_to',
                'title'		=> __( 'Recipient - France contact', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),


            array(
                'id'		=> 'beam_forms_settings_press_email_to',
                'title'		=> __( 'Recipient - Press contact', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),
			
			array(
                'id'		=> 'beam_forms_settings_contact_alert_ok',
                'title'		=> __( 'Confirmation message', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

            array(
                'id'		=> 'beam_forms_settings_contact_alert_error',
                'title'		=> __( 'Error message', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

            array(
                'id'		=> 'beam_forms_settings_application_alert_ok',
                'title'		=> __( 'Confirmation message - Apply form', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

            array(
                'id'		=> 'beam_forms_settings_application_alert_error',
                'title'		=> __( 'Error message - Apply form', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

        ),
    ),

    // CONTACT MINI

    array(
        'id'		=> 'beam_forms_settings_contactmini',
        'title'		=> __( 'CONTACT // SHORTCODE', BEAM_FORMS_SETTINGS_OPTION_NAME ),
        'fields'	=> array(

            array(
                'id'		=> 'beam_forms_settings_contactmini_form_title',
                'title'		=> __( 'Form title', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_contactmini_title',
                'title'		=> __( 'Form description title', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

            array(
                'id'		=> 'beam_forms_settings_contactmini_desc',
                'title'		=> __( 'Form description', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

            array(
                'id'		=> 'beam_forms_settings_contactmini_notice',
                'title'		=> __( 'Form notice', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

        ),
    ),


    // CONTACT MINI PRESS

    array(
        'id'		=> 'beam_forms_settings_press',
        'title'		=> __( 'CONTACT PRESS // SHORTCODE', BEAM_FORMS_SETTINGS_OPTION_NAME ),
        'fields'	=> array(

            array(
                'id'		=> 'beam_forms_settings_press_form_title',
                'title'		=> __( 'Form title', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'text',
            ),

            array(
                'id'		=> 'beam_forms_settings_press_title',
                'title'		=> __( 'Form description title', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

            array(
                'id'		=> 'beam_forms_settings_press_desc',
                'title'		=> __( 'Form description', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

            array(
                'id'		=> 'beam_forms_settings_press_notice',
                'title'		=> __( 'Form notice', BEAM_FORMS_SETTINGS_OPTION_NAME ),
                'input'		=> 'textarea',
            ),

        ),
    ),



);

$settings = new And_Wp_Settings_Api( $datas, 'Forms', BEAM_FORMS_SETTINGS_OPTION_NAME, BEAM_FORMS_SETTINGS_OPTION_GROUP );