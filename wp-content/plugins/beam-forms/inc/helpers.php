<?php
function beam_forms_get_upload_dir()
{
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/' . BEAM_FORMS_UPLOAD_FOLDER_NAME;

    return $upload_dir;
}

function beam_forms_helpers_upload_file( $name, $record_id, $errors )
{
    $attachment = '';

    if( isset( $_FILES[$name] ) && (int)$_FILES[$name] > 0 && $_FILES[$name]['error'] == UPLOAD_ERR_OK )
    {
        $file = $_FILES[$name];

        // 1. Check format and size

        $extension = end( explode( '.', $file['name'] ) );

        switch( $extension )
        {
            case 'pdf' :

                break;

            default :

                $errors[] = $name;

//                return '';

                break;
        }

        /*$finfo = new finfo( FILEINFO_MIME_TYPE );
        
        $check = $finfo->file( $file['tmp_name'] );
        
        switch( $check )
        {
            case 'application/pdf' :
            case 'application/msword' :
                
                break;
                
            default :
            
                $errors[] = $name;
                
                return '';
                
                break;
        }*/

        if( $file['size'] > BEAM_FORMS_ATTACHMENT_MAX_SIZE * 1024 * 1024 )
        {
            $errors[] = $name;

//            return '';
        }

        // 2. Upload file

        $filename = $record_id . '.' . $name . '.' . md5( basename( $file['name'] ) . time() ) . '.' . $extension;

        if( move_uploaded_file( $file['tmp_name'], beam_forms_get_upload_dir() . '/' . $filename ) )
        {
            $attachment = beam_forms_get_upload_dir() . '/' . $filename;
        }
        else
        {
            $errors[] = $name;

//            return '';
        }
    }
    else
    {
        $errors[] = $name;
    }

    return $attachment;
}