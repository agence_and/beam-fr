<?php
// CONTACT MINI : CALLBACK ************************************************************************************************************************

function beam_forms_contactmini_form_ajax_callback()
{
    global $wpdb;

    global $BEAM_CONTACT_COUNTRY;

    $datas = array(
        'error'	=> true,
    );



    if ( isset( $_POST['beam_contactmini_form_submit_nonce'] )
        && wp_verify_nonce( $_POST['beam_contactmini_form_submit_nonce'], 'beam_contactmini_form_submit' ) )
    {
        $errors = array();


        // Check reCAPTCHA

        require_once ABSPATH . 'vendor/autoload.php';

        $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );

        $recaptcha = new \ReCaptcha\ReCaptcha( $options['beam_forms_settings_alert_secret_key'] );

        $resp = $recaptcha->verify( $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );

        if( !$resp->isSuccess() )
        {
            $errors[] = 'recaptcha';
        }

        // Check datas

        if( !isset( $_POST['name'] ) || $_POST['name'] == '' ) $errors[] = 'name';
        else $name = stripslashes( sanitize_text_field( $_POST['name'] ) );
        if( !isset( $_POST['phone'] ) || $_POST['phone'] == '' ) $errors[] = 'phone';
        else $phone = stripslashes( sanitize_text_field( $_POST['phone'] ) );
        if( !isset( $_POST['email'] ) || !is_email( $_POST['email'] ) ) $errors[] = 'email';
        else $email = stripslashes( sanitize_email( $_POST['email'] ) );
        if( !isset( $_POST['country'] ) || $_POST['country'] == '' ) $errors[] = 'country';
        else $country = stripslashes( sanitize_text_field( $_POST['country'] ) );
        if( !isset( $_POST['message'] ) || $_POST['message'] == '' ) $errors[] = 'message';
        else $message = stripslashes( sanitize_text_field( $_POST['message'] ) );


        if( !empty( $_POST['object'] ) ){
            $object = stripslashes( utf8_decode( $_POST['object'] ) );
        } else {
            $object = 'Message';
        }

        if( count( $errors ) > 0 )
        {
            $datas['error'] = true;
        }
        else
        {
            // Save datas in database

            $datas_array = array(
                'fullname'		=> $name,
                'phone'			=> $phone,
                'email'			=> $email,
                'country'		=> $country,
                'object'        => $object,
                'message'		=> $message,
                'record_ip'		=> ( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ),
                'record_date'	=> current_time('mysql', 1),
            );
            $format_array = array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            );
            $wpdb->insert( BEAM_FORMS_TABLE_CONTACT, $datas_array, $format_array );

            $record_id = $wpdb->insert_id;



            // Send mail

            $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );


            $country_text = '';
            foreach( $BEAM_CONTACT_COUNTRY as $o )
            {
                if( $o['value'] == $country )
                {
                    $country_text = $o['text'];
                }
            }

            $message = '<p>Hello,</p>
				<p>A visitor has just sent you the following message from the website.</p>
					
				<p>
				Full name : ' . esc_html( $name ) . '<br>
				Phone : ' . esc_html( $phone ) . '<br>
				E-mail : ' . esc_html( $email ) . '<br>
				Country : ' . esc_html( $country_text ) . '</p>
	
				' . ( $message != '' ? '<p>Message :</p>
				
				<p>' . nl2br( esc_html( stripslashes( $_POST['message'] ) ) ) . '</p>' : '' ) . '';


            if( !empty( $_POST['object'] ) )
            {
                $email_object = stripslashes( utf8_decode( $_POST['object'] ) );
            }
            else
            {
                $email_object = 'BeAM - Message sent from the site';
            }

            if( !empty( $_POST['to'] ) && is_email( $_POST['to'] ) )
            {
                $email_to = sanitize_email( $_POST['to'] );
            }
            else
            {
                $email_to = $options['beam_forms_settings_contact_email_to'];
            }




            $headers[] = 'From: BeAM Machines - Website <' . $email_to . '>';
            $headers[] = 'Content-Type: text/html; charset=UTF-8';



            wp_mail( $email_to, $email_object, $message, $headers );


            $datas['error'] = false;
        }
    }


    echo json_encode($datas);

    die();
}
if ( is_admin() ) {
    add_action( 'wp_ajax_beam_forms_contactmini_form_ajax', 'beam_forms_contactmini_form_ajax_callback' );
    add_action( 'wp_ajax_nopriv_beam_forms_contactmini_form_ajax', 'beam_forms_contactmini_form_ajax_callback' ); // see http://codex.wordpress.org/AJAX_in_Plugins
}




// CONTACT : CALLBACK ************************************************************************************************************************

function beam_forms_contact_form_ajax_callback()
{
    global $wpdb;

    global $BEAM_CONTACT_COUNTRY;

    $datas = array(
        'error'	=> true,
    );



    if ( isset( $_POST['beam_contact_form_submit_nonce'] )
        && wp_verify_nonce( $_POST['beam_contact_form_submit_nonce'], 'beam_contact_form_submit' ) )
    {
        $errors = array();


        // Check reCAPTCHA

        require_once ABSPATH . 'vendor/autoload.php';

        $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );

        $recaptcha = new \ReCaptcha\ReCaptcha( $options['beam_forms_settings_alert_secret_key'] );

        $resp = $recaptcha->verify( $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );

        if( !$resp->isSuccess() )
        {
            $errors[] = 'recaptcha';
        }

        // Check datas

        if( !isset( $_POST['name'] ) || $_POST['name'] == '' ) $errors[] = 'name';
        else $name = stripslashes( sanitize_text_field( $_POST['name'] ) );
        if( !isset( $_POST['phone'] ) || $_POST['phone'] == '' ) $errors[] = 'phone';
        else $phone = stripslashes( sanitize_text_field( $_POST['phone'] ) );
        if( !isset( $_POST['email'] ) || !is_email( $_POST['email'] ) ) $errors[] = 'email';
        else $email = stripslashes( sanitize_email( $_POST['email'] ) );
        if( !isset( $_POST['country'] ) || $_POST['country'] == '' ) $errors[] = 'country';
        else $country = stripslashes( sanitize_text_field( $_POST['country'] ) );
        if( !isset( $_POST['message'] ) || $_POST['message'] == '' ) $errors[] = 'message';
        else $message = stripslashes( sanitize_text_field( $_POST['message'] ) );


        if( !empty( $_POST['object'] ) ){
            $object = stripslashes( utf8_decode( $_POST['object'] ) );
        } else {
            $object = '';
        }

        if( count( $errors ) > 0 )
        {
            $datas['error'] = true;
        }
        else
        {
            // Save datas in database

            $datas_array = array(
                'fullname'		    => $name,
                'phone'			=> $phone,
                'email'			=> $email,
                'country'		=> $country,
                'object'        => $object,
                'message'		=> $message,
                'record_ip'		=> ( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ),
                'record_date'	=> current_time('mysql', 1),
            );
            $format_array = array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            );
            $wpdb->insert( BEAM_FORMS_TABLE_CONTACT, $datas_array, $format_array );

            $record_id = $wpdb->insert_id;



            // Send mail

            $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );


            $country_text = '';
            foreach( $BEAM_CONTACT_COUNTRY as $o )
            {
                if( $o['value'] == $country )
                {
                    $country_text = $o['text'];
                }
            }

            $message = '<p>Hello,</p>
				<p>A visitor has just sent you the following message from the website.</p>
					
				<p>
				Full name : ' . esc_html( $name ) . '<br>
				Phone : ' . esc_html( $phone ) . '<br>
				E-mail : ' . esc_html( $email ) . '<br>
				Country : ' . esc_html( $country_text ) . '</p>
	
				' . ( $message != '' ? '<p>Message :</p>
				
				<p>' . nl2br( esc_html( stripslashes( $_POST['message'] ) ) ) . '</p>' : '' ) . '';




            if( !empty( $_POST['object'] ) )
            {
                $email_object = stripslashes( utf8_decode( $_POST['object'] ) );
            }
            else
            {
                $email_object = 'BeAM - Message sent from the site';
            }

            if( !empty( $_POST['to'] ) && is_email( $_POST['to'] ) )
            {
                $email_to = sanitize_email( $_POST['to'] );
            }
            else
            {
                $email_to = $options['beam_forms_settings_contact_email_to'];
            }




            $headers[] = 'From: BeAM Machines - Website <' . $email_to . '>';
            $headers[] = 'Content-Type: text/html; charset=UTF-8';



            wp_mail( $email_to, $email_object, $message, $headers );


            $datas['error'] = false;
        }
    }


    echo json_encode($datas);

    die();
}
if ( is_admin() ) {
    add_action( 'wp_ajax_beam_forms_contact_form_ajax', 'beam_forms_contact_form_ajax_callback' );
    add_action( 'wp_ajax_nopriv_beam_forms_contact_form_ajax', 'beam_forms_contact_form_ajax_callback' ); // see http://codex.wordpress.org/AJAX_in_Plugins
}


// PRESS : CALLBACK ************************************************************************************************************************

function beam_forms_press_form_ajax_callback()
{
    global $wpdb;

    $datas = array(
        'error'	=> true,
    );



    if ( isset( $_POST['beam_press_form_submit_nonce'] )
        && wp_verify_nonce( $_POST['beam_press_form_submit_nonce'], 'beam_press_form_submit' ) )
    {
        $errors = array();


        // Check reCAPTCHA

        require_once ABSPATH . 'vendor/autoload.php';

        $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );

        $recaptcha = new \ReCaptcha\ReCaptcha( $options['beam_forms_settings_alert_secret_key'] );

        $resp = $recaptcha->verify( $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );

        if( !$resp->isSuccess() )
        {
            $errors[] = 'recaptcha';
        }

        // Check datas

        if( !isset( $_POST['name'] ) || $_POST['name'] == '' ) $errors[] = 'name';
        else $name = stripslashes( sanitize_text_field( $_POST['name'] ) );
        if( !isset( $_POST['media'] ) || $_POST['media'] == '' ) $errors[] = 'media';
        else $media = stripslashes( sanitize_text_field( $_POST['media'] ) );
        if( !isset( $_POST['phone'] ) || $_POST['phone'] == '' ) $errors[] = 'phone';
        else $phone = stripslashes( sanitize_text_field( $_POST['phone'] ) );
        if( !isset( $_POST['email'] ) || !is_email( $_POST['email'] ) ) $errors[] = 'email';
        else $email = stripslashes( sanitize_email( $_POST['email'] ) );
        if( !isset( $_POST['message'] ) || $_POST['message'] == '' ) $errors[] = 'message';
        else $message = stripslashes( sanitize_text_field( $_POST['message'] ) );


        if( count( $errors ) > 0 )
        {
            $datas['error'] = true;
        }
        else
        {
            // Save datas in database

            $datas_array = array(
                'fullname'		=> $name,
                'media'		    => $media,
                'phone'			=> $phone,
                'email'			=> $email,
                'message'		=> $message,
                'record_ip'		=> ( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ),
                'record_date'	=> current_time('mysql', 1),
            );
            $format_array = array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            );
            $wpdb->insert( BEAM_FORMS_TABLE_PRESS, $datas_array, $format_array );

            $record_id = $wpdb->insert_id;



            // Send mail

            $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );



            $message = '<p>Hello,</p>
				<p>A visitor has just sent you the following message from the website.</p>
					
				<p>
				Full name : ' . esc_html( $name ) . '<br>
				Phone : ' . esc_html( $phone ) . '<br>
				E-mail : ' . esc_html( $email ) . '<br>
				Media : ' . esc_html( $media ) . '</p>
	
				' . ( $message != '' ? '<p>Message :</p>
				
				<p>' . nl2br( esc_html( stripslashes( $_POST['message'] ) ) ) . '</p>' : '' ) . '';


            $email_object = 'BeAM - Press contact - Message sent from the site';
            $email_to = $options['beam_forms_settings_press_email_to'];


            $headers[] = 'From: BeAM Machines - Website <' . $email_to . '>';
            $headers[] = 'Content-Type: text/html; charset=UTF-8';



            wp_mail( $email_to, $email_object, $message, $headers );


            $datas['error'] = false;
        }
    }


    echo json_encode($datas);

    die();
}
if ( is_admin() ) {
    add_action( 'wp_ajax_beam_forms_press_form_ajax', 'beam_forms_press_form_ajax_callback' );
    add_action( 'wp_ajax_nopriv_beam_forms_press_form_ajax', 'beam_forms_press_form_ajax_callback' ); // see http://codex.wordpress.org/AJAX_in_Plugins
}


// APPLICATION : CALLBACK ************************************************************************************************************************
/*
function beam_forms_application_form_ajax_callback()
{

    check_ajax_referer('beam_forms_application_form_ajax');

    global $beam_application_form_errors;
    global $wpdb;


    $datas = array(
        'error'	=> true,
    );



//    if ( isset( $_POST['beam_application_form_submit_nonce'] )
//        && wp_verify_nonce( $_POST['beam_application_form_submit_nonce'], 'beam_application_form_submit' ) )
//    {
        $errors = array();


        // Check reCAPTCHA

        require_once ABSPATH . 'vendor/autoload.php';

        $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );

        $recaptcha = new \ReCaptcha\ReCaptcha( $options['beam_forms_settings_alert_secret_key'] );

        $resp = $recaptcha->verify( $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );

        if( !$resp->isSuccess() )
        {
            $errors[] = 'recaptcha';
        }

        // Check datas
//    $offer  = filter_var( $_POST['application_offer'],FILTER_VALIDATE_INT );
    /*$offer  = filter_var( $_POST['application_offer'],FILTER_SANITIZE_STRING );
    $name  = filter_var( $_POST['application_name'],FILTER_SANITIZE_STRING );
    $phone = filter_var( $_POST['application_phone'],FILTER_SANITIZE_STRING );
    $email = filter_var( $_POST['application_email'], FILTER_VALIDATE_EMAIL );
    $message   = filter_var( $_POST['application_message'],FILTER_SANITIZE_FULL_SPECIAL_CHARS );


        if( !isset( $_POST['offer'] ) || $_POST['offer'] == '' ) $errors[] = 'offer';
        else $offer = stripslashes( sanitize_text_field( $_POST['offer'] ) );
        if( !isset( $_POST['name'] ) || $_POST['name'] == '' ) $errors[] = 'name';
        else $name = stripslashes( sanitize_text_field( $_POST['name'] ) );
        if( !isset( $_POST['phone'] ) || $_POST['phone'] == '' ) $errors[] = 'phone';
        else $phone = stripslashes( sanitize_text_field( $_POST['phone'] ) );
        if( !isset( $_POST['email'] ) || !is_email( $_POST['email'] ) ) $errors[] = 'email';
        else $email = stripslashes( sanitize_email( $_POST['email'] ) );
        if( !isset( $_POST['message'] ) || $_POST['message'] == '' ) $errors[] = 'message';
        else $message = stripslashes( sanitize_text_field( $_POST['message'] ) );

        if( count( $errors ) > 0 ) {

//    if ( ! ( $offer && $name && $phone && $email && $message) ) {
//        wp_send_json_error( array('msg' => 'Validation failed. Please try again later.') );

            $datas['error'] = true;
        }
        else
        {
            // Save datas in database

            $datas_array = array(
                'offer'		    => $offer,
                'name'		    => $name,
                'phone'			=> $phone,
                'email'			=> $email,
                'message'		=> $message,
                'record_ip'		=> ( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ),
                'record_date'	=> current_time('mysql', 1),
            );
            $format_array = array(
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
                '%s',
            );
            $wpdb->insert( BEAM_FORMS_TABLE_APPLICATION, $datas_array, $format_array );

            $record_id = $wpdb->insert_id;



            // Upload files

            $resume = beam_forms_helpers_upload_file( 'async-upload', $record_id, $errors );


            // Send mail


            $message = '<p>Hello,</p>
				<p>A visitor has just sent you the following message from the website.</p>
					
				<p>
				Offer : ' . esc_html( $offer ) . '<br>
				Full name : ' . esc_html( $name ) . '<br>
				Phone : ' . esc_html( $phone ) . '<br>
				E-mail : ' . esc_html( $email ) . '
				</p>
				
	
				' . ( $message != '' ? '<p>Cover letter :</p>
				
				<p>' . nl2br( esc_html( stripslashes( $_POST['message'] ) ) ) . '</p>' : '' ) . '';

            $headers[] = 'From: BeAM Machines - Website <' . $options['beam_forms_settings_contact_email_to'] . '>';
            $headers[] = 'Content-Type: text/html; charset=UTF-8';

            $attachments = array();
            if( $resume != '' ) $attachments[] = $resume;


            $email_object = 'BeAM - Job application sent from the site';



            wp_mail( $options['beam_forms_settings_apply_email_to'], $email_object, $message, $headers, $attachments );


            $datas['error'] = false;
        }
//    }


    echo json_encode($datas);

    die();
}
if ( is_admin() ) {
    add_action( 'wp_ajax_beam_forms_application_form_ajax', 'beam_forms_application_form_ajax_callback' );
    add_action( 'wp_ajax_nopriv_beam_forms_application_form_ajax', 'beam_forms_application_form_ajax_callback' ); // see http://codex.wordpress.org/AJAX_in_Plugins
}
*/

// SPONTANEOUS APPLICATION : CALLBACK ************************************************************************************************************************
/*
function beam_forms_spontaneousapplication_form_ajax_callback()
{

    check_ajax_referer('beam_forms_spontaneousapplication_form_ajax');

    global $beam_spontaneousapplication_form_errors;
    global $wpdb;


    $datas = array(
        'error'	=> true,
    );



//    if ( isset( $_POST['beam_application_form_submit_nonce'] )
//        && wp_verify_nonce( $_POST['beam_application_form_submit_nonce'], 'beam_application_form_submit' ) )
//    {
    $errors = array();


    // Check reCAPTCHA

    require_once ABSPATH . 'vendor/autoload.php';

    $options = get_option( BEAM_FORMS_SETTINGS_OPTION_NAME );

    $recaptcha = new \ReCaptcha\ReCaptcha( $options['beam_forms_settings_alert_secret_key'] );

    $resp = $recaptcha->verify( $_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );

    if( !$resp->isSuccess() )
    {
        $errors[] = 'recaptcha';
    }

    // Check datas
//    $offer  = filter_var( $_POST['application_offer'],FILTER_VALIDATE_INT );
    /*$offer  = filter_var( $_POST['application_offer'],FILTER_SANITIZE_STRING );
    $name  = filter_var( $_POST['application_name'],FILTER_SANITIZE_STRING );
    $phone = filter_var( $_POST['application_phone'],FILTER_SANITIZE_STRING );
    $email = filter_var( $_POST['application_email'], FILTER_VALIDATE_EMAIL );
    $message   = filter_var( $_POST['application_message'],FILTER_SANITIZE_FULL_SPECIAL_CHARS );


    if( !isset( $_POST['offer'] ) || $_POST['offer'] == '' ) $errors[] = 'offer';
    else $offer = stripslashes( sanitize_text_field( $_POST['offer'] ) );
    if( !isset( $_POST['name'] ) || $_POST['name'] == '' ) $errors[] = 'name';
    else $name = stripslashes( sanitize_text_field( $_POST['name'] ) );
    if( !isset( $_POST['phone'] ) || $_POST['phone'] == '' ) $errors[] = 'phone';
    else $phone = stripslashes( sanitize_text_field( $_POST['phone'] ) );
    if( !isset( $_POST['email'] ) || !is_email( $_POST['email'] ) ) $errors[] = 'email';
    else $email = stripslashes( sanitize_email( $_POST['email'] ) );
    if( !isset( $_POST['message'] ) || $_POST['message'] == '' ) $errors[] = 'message';
    else $message = stripslashes( sanitize_text_field( $_POST['message'] ) );

    if( count( $errors ) > 0 ) {

//    if ( ! ( $offer && $name && $phone && $email && $message) ) {
//        wp_send_json_error( array('msg' => 'Validation failed. Please try again later.') );

        $datas['error'] = true;
    }
    else
    {
        // Save datas in database

        $datas_array = array(
            'offer'		    => $offer,
            'name'		    => $name,
            'phone'			=> $phone,
            'email'			=> $email,
            'message'		=> $message,
            'record_ip'		=> ( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ),
            'record_date'	=> current_time('mysql', 1),
        );
        $format_array = array(
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
        );
        $wpdb->insert( BEAM_FORMS_TABLE_APPLICATION, $datas_array, $format_array );

        $record_id = $wpdb->insert_id;



        // Upload files

        $resume = beam_forms_helpers_upload_file( 'async-upload', $record_id, $errors );


        // Send mail


        $message = '<p>Hello,</p>
				<p>A visitor has just sent you the following message from the website.</p>
					
				<p>
				Offer : ' . esc_html( $offer ) . '<br>
				Full name : ' . esc_html( $name ) . '<br>
				Phone : ' . esc_html( $phone ) . '<br>
				E-mail : ' . esc_html( $email ) . '
				</p>
				
	
				' . ( $message != '' ? '<p>Cover letter :</p>
				
				<p>' . nl2br( esc_html( stripslashes( $_POST['message'] ) ) ) . '</p>' : '' ) . '';

        $headers[] = 'From: BeAM Machines - Website <' . $options['beam_forms_settings_contact_email_to'] . '>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $attachments = array();
        if( $resume != '' ) $attachments[] = $resume;


        $email_object = 'BeAM - Spontanob application sent from the site';



        wp_mail( $options['beam_forms_settings_apply_email_to'], $email_object, $message, $headers, $attachments );


        $datas['error'] = false;
    }
//    }


    echo json_encode($datas);

    die();
}
if ( is_admin() ) {
    add_action( 'wp_ajax_beam_forms_spontaneousapplication_form_ajax', 'beam_forms_spontaneousapplication_form_ajax_callback' );
    add_action( 'wp_ajax_nopriv_beam_forms_spontaneousapplication_form_ajax', 'beam_forms_spontaneousapplication_form_ajax_callback' ); // see http://codex.wordpress.org/AJAX_in_Plugins
}*/