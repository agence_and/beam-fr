<?php

$BEAM_JOB_INDUSTRY = array(
    array(
        'value'		=> 'corporate',
        'text'		=> __( 'Entreprise', BEAM_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'business_development',
        'text'		=> __( 'Développement des affaires', BEAM_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'process_technology_rd',
        'text'		=> __( 'Technologie de processus / R&D', BEAM_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'product_engineering_quality',
        'text'		=> __( 'Ingénierie des produits & Qualité', BEAM_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'operations',
        'text'		=> __( 'Opérations', BEAM_FORMS_SETTINGS_OPTION_NAME ),
    ),
);

/*$BEAM_JOB_INDUSTRY = array();

$terms = get_terms( array(
    'taxonomy' => 'jobs_category',
    'hide_empty' => false,
) );
echo '<pre>';
var_dump( $terms );
echo '</pre>';

foreach($terms as $term){

    
    $BEAM_JOB_INDUSTRY[] = array(
        'value'		=> $term->slug,
        'text'		=> $term->name,
    );
}*/


$BEAM_CONTACT_COUNTRY = array(
    array(
        'value'		=> 'AFG',
        'text'		=> __( 'Afghanistan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ALA',
        'text'		=> __( 'Îles Aland', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ALB',
        'text'		=> __( 'Albanie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'DZA',
        'text'		=> __( '	Algérie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ASM',
        'text'		=> __( 'Samoa américaines', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'AND',
        'text'		=> __( 'Andorre', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'AGO',
        'text'		=> __( 'Angola', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'AIA',
        'text'		=> __( 'Anguilla', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ATA',
        'text'		=> __( 'Antarctique', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ATG',
        'text'		=> __( 'Antigua-et-Barbuda', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ARG',
        'text'		=> __( 'Argentine', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ARM',
        'text'		=> __( 'Arménie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ABW',
        'text'		=> __( 'Aruba', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'AUS',
        'text'		=> __( 'Australie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'AUT',
        'text'		=> __( 'Autriche', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'AZE',
        'text'		=> __( 'Azerbaïdjan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BHS',
        'text'		=> __( 'Bahamas', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BHR',
        'text'		=> __( 'Bahreïn', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BGD',
        'text'		=> __( 'Bangladesh', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BRB',
        'text'		=> __( 'Barbade', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BLR',
        'text'		=> __( 'Biélorussie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BEL',
        'text'		=> __( 'Belgique', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BLZ',
        'text'		=> __( 'Belize', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BEN',
        'text'		=> __( 'Bénin', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BMU',
        'text'		=> __( 'Bermudes', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BTN',
        'text'		=> __( 'Bhoutan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BOL',
        'text'		=> __( 'Bolivie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BIH',
        'text'		=> __( 'Bosnie-Herzégovine', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BWA',
        'text'		=> __( 'Botswana', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BVT',
        'text'		=> __( 'L’île Bouvet', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BRA',
        'text'		=> __( 'Brésil', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'IOT',
        'text'		=> __( 'Territoire britannique de l’océan Indien', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BRN',
        'text'		=> __( 'Brunei Darussalam', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BGR',
        'text'		=> __( 'Bulgarie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BFA',
        'text'		=> __( 'Burkina Faso', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BDI',
        'text'		=> __( 'Burundi', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KHM',
        'text'		=> __( 'Cambodge', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CMR',
        'text'		=> __( 'Cameroun', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CAN',
        'text'		=> __( 'Canada', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CPV',
        'text'		=> __( 'Cap Vert', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CYM',
        'text'		=> __( 'Iles Cayman', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CAF',
        'text'		=> __( 'République centrafricaine', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TCD',
        'text'		=> __( 'Tchad', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CHL',
        'text'		=> __( 'Chili', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CHN',
        'text'		=> __( 'Chine', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CXR',
        'text'		=> __( 'Christmas Island', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CCK',
        'text'		=> __( 'Îles Cocos (Keeling)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'COL',
        'text'		=> __( 'Colombie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'COM',
        'text'		=> __( 'Comores', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'COG',
        'text'		=> __( 'Congo', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'COD',
        'text'		=> __( 'Congo République démocratique du', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'COK',
        'text'		=> __( 'Îles Cook', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CRI',
        'text'		=> __( 'Costa Rica', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CIV',
        'text'		=> __( 'Côte d\'Ivoire', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'HRV',
        'text'		=> __( 'Croatie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CUB',
        'text'		=> __( 'Cuba', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CYP',
        'text'		=> __( 'Chypre', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CZE',
        'text'		=> __( 'République tchèque', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'DNK',
        'text'		=> __( 'Danemark', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'DJI',
        'text'		=> __( 'Djibouti', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'DMA',
        'text'		=> __( 'Dominique', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'DOM',
        'text'		=> __( 'République dominicaine', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ECU',
        'text'		=> __( 'Equateur', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'EGY',
        'text'		=> __( 'Egypte', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SLV',
        'text'		=> __( 'El Salvador', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GNQ',
        'text'		=> __( 'Guinée équatoriale', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ERI',
        'text'		=> __( 'Érythrée', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'EST',
        'text'		=> __( 'Estonie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ETH',
        'text'		=> __( 'Éthiopie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'FLK',
        'text'		=> __( 'Îles Falkland (Malvinas)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'FRO',
        'text'		=> __( 'Îles Féroé', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'FJI',
        'text'		=> __( 'Fidji', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'FIN',
        'text'		=> __( 'Finlande', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'FRA',
        'text'		=> __( 'France', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GUF',
        'text'		=> __( 'Guyane française', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PYF',
        'text'		=> __( 'Polynésie française', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ATF',
        'text'		=> __( 'Terres australes françaises', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GAB',
        'text'		=> __( 'Gabon', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GMB',
        'text'		=> __( 'Gambie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GEO',
        'text'		=> __( 'Géorgie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'DEU',
        'text'		=> __( 'Allemagne', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GHA',
        'text'		=> __( 'Ghana', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GIB',
        'text'		=> __( 'Gibraltar', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GRC',
        'text'		=> __( 'Grèce', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GRL',
        'text'		=> __( 'Groenland', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GRD',
        'text'		=> __( 'Grenade', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GLP',
        'text'		=> __( 'Guadeloupe', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GUM',
        'text'		=> __( 'Guam', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GTM',
        'text'		=> __( 'Guatemala', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GGY',
        'text'		=> __( 'Guernesey', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GIN',
        'text'		=> __( 'Guinée', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GNB',
        'text'		=> __( 'Guinée-Bissau', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GUY',
        'text'		=> __( 'Guyane', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'HTI',
        'text'		=> __( 'Haïti', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'HMD',
        'text'		=> __( 'Entendu l’île et des îles McDonald', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'VAT',
        'text'		=> __( 'Saint-Siège (Cité du Vatican)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'HND',
        'text'		=> __( 'Honduras', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'HKG',
        'text'		=> __( 'Hong Kong', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'HUN',
        'text'		=> __( 'Hongrie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ISL',
        'text'		=> __( 'Islande', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'IND',
        'text'		=> __( 'Inde', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'IDN',
        'text'		=> __( 'Indonésie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'IRN',
        'text'		=> __( 'Iran République islamique d’', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'IRQ',
        'text'		=> __( 'Irak', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'IRL',
        'text'		=> __( 'Irlande', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'IMN',
        'text'		=> __( 'Ile de Man', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ISR',
        'text'		=> __( 'Israël', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ITA',
        'text'		=> __( 'Italie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'JAM',
        'text'		=> __( 'Jamaïque', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'JPN',
        'text'		=> __( 'Japon', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'JEY',
        'text'		=> __( 'Jersey', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'JOR',
        'text'		=> __( 'Jordanie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KAZ',
        'text'		=> __( '	Kazakhstan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KEN',
        'text'		=> __( 'Kenya', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KIR',
        'text'		=> __( 'Kiribati', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PRK',
        'text'		=> __( 'Corée République populaire démocratique de', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KOR',
        'text'		=> __( 'Corée République de', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KWT',
        'text'		=> __( 'Koweït', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KGZ',
        'text'		=> __( 'Kirghizistan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LAO',
        'text'		=> __( 'République démocratique populaire lao', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LVA',
        'text'		=> __( 'Lettonie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LBN',
        'text'		=> __( 'Liban', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LSO',
        'text'		=> __( 'Lesotho', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LBR',
        'text'		=> __( 'Libéria', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LBY',
        'text'		=> __( 'Libye', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LIE',
        'text'		=> __( 'Liechtenstein', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LTU',
        'text'		=> __( 'Lituanie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LUX',
        'text'		=> __( 'Luxembourg', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MAC',
        'text'		=> __( 'Macao', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MKD',
        'text'		=> __( 'Macédoine République de', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MDG',
        'text'		=> __( 'Madagascar', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MWI',
        'text'		=> __( 'Malawi', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MYS',
        'text'		=> __( 'Malaisie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MDV',
        'text'		=> __( 'Maldives', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MLI',
        'text'		=> __( 'Mali', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MLT',
        'text'		=> __( 'Malte', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MHL',
        'text'		=> __( 'Iles Marshall', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MTQ',
        'text'		=> __( 'Martinique', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MRT',
        'text'		=> __( 'Mauritanie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MUS',
        'text'		=> __( 'Maurice', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MYT',
        'text'		=> __( 'Mayotte', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MEX',
        'text'		=> __( 'Mexique', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'FSM',
        'text'		=> __( 'Micronésie États fédérés de', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MDA',
        'text'		=> __( 'Moldavie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MCO',
        'text'		=> __( 'Monaco', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MNG',
        'text'		=> __( 'Mongolie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MNE',
        'text'		=> __( 'Monténégro', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MSR',
        'text'		=> __( 'Montserrat', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MAR',
        'text'		=> __( 'Maroc', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MOZ',
        'text'		=> __( 'Mozambique', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MMR',
        'text'		=> __( 'Myanmar', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NAM',
        'text'		=> __( 'Namibie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NRU',
        'text'		=> __( 'Nauru', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NPL',
        'text'		=> __( 'Népal', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NLD',
        'text'		=> __( 'Pays-Bas', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NCL',
        'text'		=> __( 'Nouvelle-Calédonie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NZL',
        'text'		=> __( 'Nouvelle-Zélande', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NIC',
        'text'		=> __( 'Nicaragua', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NER',
        'text'		=> __( 'Niger', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NGA',
        'text'		=> __( 'Nigeria', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NIU',
        'text'		=> __( 'Niue', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NFK',
        'text'		=> __( 'L’île de Norfolk', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MNP',
        'text'		=> __( 'Îles Mariannes du Nord', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'NOR',
        'text'		=> __( 'Norvège', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'OMN',
        'text'		=> __( 'Oman', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PAK',
        'text'		=> __( 'Pakistan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PLW',
        'text'		=> __( 'Palau', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PSE',
        'text'		=> __( 'Territoires palestiniens (occupés)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PAN',
        'text'		=> __( 'Panama', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PNG',
        'text'		=> __( 'Papouasie-Nouvelle-Guinée', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PRY',
        'text'		=> __( 'Paraguay', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PER',
        'text'		=> __( 'Pérou', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PHL',
        'text'		=> __( 'Philippines', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PCN',
        'text'		=> __( 'Pitcairn', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'POL',
        'text'		=> __( 'Pologne', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PRT',
        'text'		=> __( 'Portugal', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'PRI',
        'text'		=> __( 'Puerto Rico', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'QAT',
        'text'		=> __( 'Qatar', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'REU',
        'text'		=> __( 'Réunion', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ROU',
        'text'		=> __( 'Roumanie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'RUS',
        'text'		=> __( 'Fédération de Russie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'RWA',
        'text'		=> __( 'Rwanda', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'BLM',
        'text'		=> __( 'Saint Barthélemy', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SHN',
        'text'		=> __( 'Sainte-Hélène', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'KNA',
        'text'		=> __( 'Saint-Kitts-et-Nevis', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LCA',
        'text'		=> __( 'Sainte-Lucie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'MAF',
        'text'		=> __( 'Saint-Martin (partie française)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SPM',
        'text'		=> __( '	Saint-Pierre-et-Miquelon', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'VCT',
        'text'		=> __( 'Saint-Vincent-et-les Grenadines', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'WSM',
        'text'		=> __( 'Samoa', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SMR',
        'text'		=> __( 'Saint-Marin', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'STP',
        'text'		=> __( 'Sao Tomé-et-Principe', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SAU',
        'text'		=> __( 'Arabie Saoudite', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SEN',
        'text'		=> __( 'Sénégal', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SRB',
        'text'		=> __( 'Serbie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SYC',
        'text'		=> __( 'Seychelles', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SLE',
        'text'		=> __( 'Sierra Leone', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SGP',
        'text'		=> __( 'Singapour', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SXM',
        'text'		=> __( 'Saint-Martin (partie allemande)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SVK',
        'text'		=> __( 'Slovaquie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SVN',
        'text'		=> __( 'Slovénie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SLB',
        'text'		=> __( 'Îles Salomon', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SOM',
        'text'		=> __( 'Somalie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ZAF',
        'text'		=> __( 'Afrique du Sud', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SGS',
        'text'		=> __( 'Géorgie du Sud et les îles Sandwich du Sud', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SSD',
        'text'		=> __( 'Sud-Soudann', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ESP',
        'text'		=> __( '	Espagne', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'LKA',
        'text'		=> __( 'Sri Lanka', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SDN',
        'text'		=> __( 'Soudan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SUR',
        'text'		=> __( 'Suriname', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SJM',
        'text'		=> __( 'Svalbard et Jan Mayen', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SWZ',
        'text'		=> __( 'Swaziland', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SWE',
        'text'		=> __( 'Suède', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'CHE',
        'text'		=> __( 'Suisse', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'SYR',
        'text'		=> __( 'République arabe syrienne (Syrie)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TWN',
        'text'		=> __( 'Taiwan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TJK',
        'text'		=> __( 'Tajikistan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TZA',
        'text'		=> __( 'Tanzanie *', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'THA',
        'text'		=> __( 'Thaïlande', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TLS',
        'text'		=> __( 'Timor-Leste', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TGO',
        'text'		=> __( 'Togo', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TKL',
        'text'		=> __( 'Tokelau', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TON',
        'text'		=> __( 'Tonga', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TTO',
        'text'		=> __( 'Trinité-et-Tobago', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TUN',
        'text'		=> __( 'Tunisie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TUR',
        'text'		=> __( 'Turquie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TKM',
        'text'		=> __( 'Turkménistan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TCA',
        'text'		=> __( 'Îles Turques et Caïques', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'TUV',
        'text'		=> __( 'Tuvalu', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'UGA',
        'text'		=> __( 'Ouganda', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'UKR',
        'text'		=> __( 'Ukraine', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ARE',
        'text'		=> __( 'Émirats arabes unis', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'GBR',
        'text'		=> __( 'Royaume-Uni', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'USA',
        'text'		=> __( 'États-Unis d’Amérique', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'UMI',
        'text'		=> __( 'États-Unis Îles mineures éloignées', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'URY',
        'text'		=> __( 'Uruguay', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'UZB',
        'text'		=> __( 'Ouzbékistan', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'VUT',
        'text'		=> __( 'Vanuatu', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'VEN',
        'text'		=> __( 'Venezuela (République bolivarienne du)', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'VNM',
        'text'		=> __( 'Viet Nam', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'VGB',
        'text'		=> __( 'Îles Vierges anglaises', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'VIR',
        'text'		=> __( 'Îles Vierges américaines', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'WLF',
        'text'		=> __( 'Wallis-et-Futuna', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ESH',
        'text'		=> __( 'Sahara occidental', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'YEM',
        'text'		=> __( 'Yémen', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ZMB',
        'text'		=> __( 'Zambie', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
    array(
        'value'		=> 'ZWE',
        'text'		=> __( 'Zimbabwe', ADF_FORMS_SETTINGS_OPTION_NAME ),
    ),
);