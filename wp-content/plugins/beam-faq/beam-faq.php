<?php
/*
Plugin Name: BeAM - FAQ
Plugin URI: http://www.and-digital.fr
Description: Manage FAQ
Version: 1.0.0
Author: Agence AND
Author URI: http://www.and-digital.fr
*/
	define( 'BEAM_FAQ_SETTINGS_OPTION_GROUP', 'beamfaqSettings' );
	define( 'BEAM_FAQ_SETTINGS_OPTION_NAME', 'beam_faq_settings' );
	
	include( plugin_dir_path( __FILE__ ) . 'inc/custom-posts.php' );
	include( plugin_dir_path( __FILE__ ) . 'inc/custom-taxonomies.php' );