<?php
	function beam_faq_custom_post_faq()
	{
		$labels = array(
            'name'               	=> __( 'FAQ', BEAM_FAQ_SETTINGS_OPTION_NAME ),
            'singular_name'      	=> __( 'FAQ', BEAM_FAQ_SETTINGS_OPTION_NAME ),
            'add_new'            	=> __( 'Add a question', BEAM_FAQ_SETTINGS_OPTION_NAME ),
            'add_new_item'       	=> __( 'Add a question' ),
            'new_item'           	=> __( 'New question' ),
            'edit_item'          	=> __( 'Modify the question' ),
            'view_item'          	=> __( 'See the question' ),
            'all_items'          	=> __( 'All questions' ),
            'search_items'       	=> __( 'Search questions' ),
            'parent_item_colon'  	=> __( 'Parent question :' ),
            'not_found'          	=> __( 'No question found' ),
            'not_found_in_trash' 	=> __( 'No question found in trash' )
		);

		$args = array(
			'labels'             	=> $labels,
			'public'             	=> true,
			'show_ui'            	=> true,
			'menu_icon'       		=> 'dashicons-editor-help',
			'menu_position'			=> 20,
			'exclude_from_search' 	=> true,
			'hierarchical'       	=> false,
			'supports'          	=> array( 'title', 'editor' ),
			'taxonomies'			=> array( 'faq_category' ),
			'has_archive'        	=> false,
			'rewrite'            	=> array( 
											'slug' 		=> 'faq',
											'withfront'	=> true,
									   ),
			'query_var'          	=> true,
		);

		register_post_type( 'faq', $args );
	}
	
	
	function _beam_faq_init_custom_posts()
	{
		beam_faq_custom_post_faq();
	}
	add_action( 'init', '_beam_faq_init_custom_posts' );