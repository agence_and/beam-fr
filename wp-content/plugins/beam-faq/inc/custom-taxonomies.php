<?php

	function beam_faq_custom_taxonomy_category() 
	{
		$labels = array(
			'name'              			=> __( 'Themes', BEAM_FAQ_SETTINGS_OPTION_NAME ),
			'singular_name'     			=> __( 'Theme', BEAM_FAQ_SETTINGS_OPTION_NAME ),
			'search_items'      			=> __( 'Search a theme' ),
			'all_items'         			=> __( 'All themes' ),
			'parent_item'       			=> __( 'Parent theme' ),
			'parent_item_colon'				=> __( 'Parent theme :' ),
			'edit_item'         			=> __( 'Modify the theme' ),
			'view_item'         			=> __( 'View' ),
			'update_item'       			=> __( 'Update the theme' ),
			'add_new_item'      			=> __( 'Add a new theme' ),
			'new_item_name'     			=> __( 'Theme\'s name' ),
			'popular_items'     			=> __( 'Popular themes' ),
			'separate_items_with_commas'    => __( 'Separate themes with commas' ),
			'add_or_remove_items'     		=> __( 'Add or remove themes' ),
			'choose_from_most_used'     	=> __( 'Choose from themes most used' ),
			'not_found'     				=> __( 'No theme found' ),
			'menu_name'         			=> __( 'Themes' ),
		);
	
		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 
										'slug' 			=> 'faq_category',
										'withfront'		=> false,
										'hierarchical'	=> false,
								   ),
		);

		register_taxonomy( 'faq_category', array( 'faq' ), $args );
	}
	
	
	function _beam_faq_custom_taxonomies()
	{
		beam_faq_custom_taxonomy_category();
	}
	add_action( 'init', '_beam_faq_custom_taxonomies', 0 );