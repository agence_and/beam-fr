<?php
get_header();

global $wp_query;
$permalink = home_url('/');

$posts_per_page = (int)get_option('posts_per_page');

?>

    <div class="page-content page-search">
        <div class="container">

            <?php

            $paged = (get_query_var('paged') ? (int)get_query_var('paged') : (get_query_var('page') ? (int)get_query_var('page') : 1));

            // Custom search query in configure/search.php

            if (have_posts()) {

                $nbPostsTotal = $wp_query->found_posts;


                ?>

                <h1 class='h1'><?php _e('Résultats de recherche', 'beam'); ?></h1>
                <p><?php echo $nbPostsTotal; ?> <?php _e('résultat(s) pour le mot clé', 'beam'); ?>
                    « <?php echo get_query_var('s') ?> »</p>

                <div class="infinite">
                    <div class="jscroll">
                        <?php
                        while (have_posts()) {
                            the_post();

                            $itemID = get_the_ID();
                            
                            $itemTitle = get_the_title($itemID);
                            $itemDate = get_the_date('', $itemID);
                            $itemPostType = get_post_type($itemID);
                            $itemContent = get_the_excerpt();
                            ?>
                            <a href="<?php the_permalink(); ?>" class="search-post">
                                <div class="post-title">
                            <?php switch ($itemPostType) {
                                case 'page':
                                    esc_html_e('PAGE // ', 'beam');
                                    break;
                                case 'products':
                                    esc_html_e('PRODUIT // ', 'beam');
                                    break;
                                case 'jobs':
                                    esc_html_e('OFFRE // ', 'beam');
                                    break;
                                case 'post':
                                    esc_html_e('ACTUALITÉ // ', 'beam');
                                    break;
                            }?>

                                    <?php the_title(); ?>
                                </div>
                                <?php if ($itemPostType == 'post') { ?>
                                    <div class="post-date"><?php echo $itemDate; ?></div>
                                <?php } ?>
                                <?php if ($itemPostType == 'products') {
                                    $productField = get_field('hp', $itemID);
                                    ?>
                                    <p><?php echo $productField['desc']; ?></p>
                                <?php } elseif ($itemPostType == 'jobs'){
                                    $jobLocation = get_field('location', $itemID);
                                    $jobDesc = get_field('short_desc', $itemID);
                                    ?>
                                    <div class="post-date"><?php echo $jobLocation['address']; ?></div>
                                    <p><?php echo $jobDesc; ?></p>
                                <?php } else {?>
                                <p><?php echo $itemContent ?></p>
                                <?php } ?>
                            </a>
                            <?php

                        }
                        //                        wp_reset_postdata();

                        ?>


                    </div>


                    <?php
                    if ($nbPostsTotal >= $posts_per_page) {

                        $next_page_permalink = untrailingslashit($permalink) . '/page/' . ($paged + 1) . '?s=' . $s;

                        echo '<div class="btn-ctn btn-ctn--loadmore"><a href="' . esc_url($next_page_permalink) . '" class="btn-loadmore jscrollnext">';
                        echo '<span>' . __('Voir + de résultats', 'beam') . '</span>';
                        echo '</a></div>';
                    }


                    ?>

                </div>
                <?php
            } else {
                ?>
                <h1 class='h1'><?php _e('Aucun résultat', 'beam'); ?></h1>
                <p><?php esc_html_e('Désolé, aucun résultat ne correspond à votre recherche. Merci de réessayer avec un mot clé différent.', 'beam'); ?></p>
            <?php } ?>


        </div>
    </div>
<?php get_footer('without-contact'); ?>