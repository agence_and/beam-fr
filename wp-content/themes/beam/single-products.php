<?php
/**
 * Single products template
 *
 * @package beam
 *
 */

get_header();

$id = get_the_ID();
$fields = get_fields($id);


$datas = $fields['single_product'];


$productTitle = get_the_title();
$contact_to = $datas['email'];;
$contact_object = 'BeAM - Contact pour ' . get_the_title();

// .product intro
$date = $datas['date'];
$desc = $datas['desc'];
$img = $datas['img'];

// .product-video
$video = $datas['video'];

// .product-exploded
/*$imgExploded = $datas['img_exploded']['img'];
$imgExplodedLegends = $datas['img_exploded']['legends'];*/

// .product-technical
$technicalDoc = $datas['technical_doc'];
$technicalInfo = $datas['technical_info'];

// .product-nozzles
$nozzlesTitle = $datas['nozzles']['title'];
$nozzlesDesc = $datas['nozzles']['desc'];
$nozzlesImages = $datas['nozzles']['images'];
$nozzlesComparativeTable = $datas['nozzles']['comparative_table'];
$nozzlesSlider = $datas['nozzles']['slider'];
$nozzlesQuote = $datas['nozzles']['quote'];

?>

    <div class="single-product page-content">

        <div class="product-intro">
            <div class="container">
                <div class="ctn-flex ctn-flex-nowrap">
                    <?php
                    if (!empty($img)) {
                        ?>
                        <div class="intro-img">
                            <img src="<?php echo $img['sizes']['large']; ?>" alt="<?php echo $img['alt']; ?>"/>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="intro-txt">
                        <h1 class="h1"><?php echo $productTitle; ?></h1>
                        <?php
                        if (!empty($date)) {
                            ?>

                            <div class="txt-date">(<?php echo $date; ?>)</div>
                            <?php
                        }
                        if (!empty($desc)) {
                            echo $desc;
                        }
                        ?>
                        <?php /* ?>
                    <div class="btn-ctn">
                        <a href="#"
                           class="link goto-next"><?php esc_html_e('Faites défiler pour découvrir ', 'beam') . ' ' . the_title(); ?></a>
                    </div>
   <?php*/ ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="product-video">
            <div class="container">
                <div class="container-mini">
                    <h2 class="h2 video-title"><?php _e('Présentation du Produit', 'beam'); ?></h2>
                </div>
                <div class="video-container">
                    <iframe src="<?php echo $video; ?>?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0"
                            frameborder="0"></iframe>
                </div>
            </div>
        </div>

        <?php /* ?>
    <div class="product-exploded">
        <div class="container">
            <?php
            if (!empty($imgExplodedLegends)) {
                $count = 0;
                ?>
                <div class="exploded-legends">
                    <?php
                    foreach ($imgExplodedLegends as $item) {
                        $count++;
                        ?>
                        <div class="legends-item ctn-flex ctn-flex-top ctn-flex-mini ctn-flex-start">
                            <span class="item-count"><?php echo $count; ?></span>
                            <span> <?php echo $item['legend']; ?> </span>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
            <div class="exploded-img">
                <img src="<?php echo $imgExploded['url']; ?>" alt="<?php echo $imgExploded['alt']; ?>"/>
            </div>
        </div>
    </div>
   <?php*/ ?>


        <div class="product-technical">
            <div class="container">
                <div class="ctn-flex ctn-flex-top">
                    <div class="technical-intro">
                        <div class="intro-txt">
                            <h1 class="h1"><?php echo $productTitle; ?></h1>
                            <?php
                            if (!empty($date)) {
                                ?>

                                <div class="txt-date">(<?php echo $date; ?>)</div>
                                <?php
                            }
                            if (!empty($desc)) {
                                echo $desc;
                            }
                            ?>
                            <a href="#" data-modal="modal-contact"
                               class="btn btn-white modal-trigger"><?php esc_html_e('Contactez-nous', 'beam'); ?></a>
                        </div>

                        <?php
                        if (!empty($technicalDoc)) {
                            ?>
                            <div class="intro-download ctn-flex ctn-flex-start ctn-flex-bottom">
                                <div class="download-img">
                                    <div>
                                        <div class="download-img-title">
                                            <?php the_title(); ?>
                                        </div>
                                        <div class="download-img-desc">
                                            <?php esc_html_e('Documentation technique', 'beam'); ?>
                                        </div>
                                    </div>
                                </div>

                                <a href="<?php echo $technicalDoc['url']; ?>"
                                   download="<?php echo $technicalDoc['filename']; ?>" target="_blank">
                                    <div class="ctn-flex ctn-flex-mini">
                                        <span class="ico ico-download"></span>
                                        <span><?php _e('Téléchargez<br/>notre documentation technique', 'beam'); ?></span>
                                    </div>

                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="technical-info">
                        <?php echo $technicalInfo; ?>
                    </div>
                </div>

            </div>
        </div>


        <div class="product-nozzles">
            <div class="container">
                <div class="container-mini">
                    <h2 class="h2 nozzles-title"><?php echo $nozzlesTitle; ?></h2>
                    <p class="nozzles-desc"><?php echo $nozzlesDesc; ?></p>
                </div>

                <div class="container-medium">

                    <?php
                    if (!empty($nozzlesImages)) {
                        ?>
                        <div class="nozzles-images ctn-flex ctn-flex-top ctn-flex-no-mg ctn-flex-center">
                            <?php
                            foreach ($nozzlesImages as $img) {
                                ?>
                                <div class="images-item">
                                    <img src="<?php echo $img['img']['url']; ?>"
                                         alt="<?php echo $img['img']['alt']; ?>"/>
                                    <?php /*
                            if (!empty($img['desc'])) {
                                ?>
                                <div class="item-desc"><?php echo $img['desc']; ?></div>
                                <?php
                            } */
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if (!empty($nozzlesComparativeTable)) {
                        ?>
                        <div class="nozzles-featured ctn-flex ctn-flex-top">
                            <div class="featured-left">
                                <?php
                                if (!empty($nozzlesComparativeTable)) {
                                    ?>
                                    <?php /* ?>
                    <div class="featured-table">
                        <table>
                            <thead>
                            <tr>
                                <th><?php _e('Référence', 'beam'); ?></th>
                                <?php
                                foreach ($nozzlesComparativeTable as $item) {
                                    ?>
                                    <th><?php echo $item['reference']; ?></th>
                                    <?php
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>


                            <tr>
                                <th>
                                    <p><?php _e('Largeur de dépôt', 'beam'); ?></p>
                                    <p><?php _e('Précision de dépôt', 'beam'); ?></p>
                                </th>
                                <?php
                                foreach ($nozzlesComparativeTable as $item) {
                                    ?>
                                    <td>
                                        <p><?php echo $item['deposition_width']; ?></p>
                                        <p><?php echo $item['deposition_accuracy']; ?></p>
                                    </td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <tr>
                                <th>
                                    <p><?php _e('Volume de dépôt', 'beam'); ?></p>
                                    <p><?php _e('Taux de construction typique', 'beam'); ?></p>
                                </th>

                                <?php
                                foreach ($nozzlesComparativeTable as $item) {
                                    ?>
                                    <td>
                                        <p><?php echo $item['deposition_volume']; ?></p>
                                        <p><?php echo $item['typical_build_rate']; ?></p>
                                    </td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <tr>
                                <th><?php _e('Plage de puissance compatible', 'beam'); ?></th>

                                <?php
                                foreach ($nozzlesComparativeTable as $item) {
                                    ?>
                                    <td>
                                        <?php echo $item['compatible_power_range']; ?>
                                    </td>
                                    <?php
                                }
                                ?>

                            </tr>
                            </tbody>
                        </table>
<?php */ ?>

                                    <div class="ctn-flex featured-table-items">
                                        <?php foreach ($nozzlesComparativeTable

                                                       as $item) { ?>
                                            <div class="item">
                                                <div class="item-header">
                                                    <?php echo $item['reference']; ?>
                                                </div>
                                                <table>
                                                    <?php
                                                    if (!empty($item['deposition_width']) || !empty($item['deposition_accuracy'])) {
                                                        ?>
                                                        <tr>

                                                            <th>
                                                                <?php
                                                                if (!empty($item['deposition_width'])) {
                                                                    ?>
                                                                    <p><?php _e('Largeur de déposition', 'beam'); ?></p>
                                                                    <?php
                                                                }
                                                                if (!empty($item['deposition_accuracy'])) {
                                                                    ?>
                                                                    <p><?php _e('Précision de déposition', 'beam'); ?></p>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </th>
                                                            <td>
                                                                <?php
                                                                if (!empty($item['deposition_width'])) {
                                                                    ?>
                                                                    <p><?php echo $item['deposition_width']; ?></p>
                                                                    <?php
                                                                }
                                                                if (!empty($item['deposition_accuracy'])) {
                                                                    ?>
                                                                    <p><?php echo $item['deposition_accuracy']; ?></p>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    if (!empty($item['deposition_volume']) || !empty($item['typical_build_rate'])) {
                                                        ?>
                                                        <tr>
                                                            <th>
                                                                <?php
                                                                if (!empty($item['deposition_volume'])) {
                                                                    ?>
                                                                    <p><?php _e('Volume de dépôt', 'beam'); ?></p>
                                                                    <?php
                                                                }
                                                                if (!empty($item['typical_build_rate'])) {
                                                                    ?>
                                                                    <p><?php _e('Taux de construction', 'beam'); ?></p>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </th>
                                                            <td>
                                                                <?php
                                                                if (!empty($item['deposition_volume'])) {
                                                                    ?>
                                                                    <p><?php echo $item['deposition_volume']; ?></p>
                                                                    <?php
                                                                }
                                                                if (!empty($item['typical_build_rate'])) {
                                                                    ?>
                                                                    <p><?php echo $item['typical_build_rate']; ?></p>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    if (!empty($item['compatible_power_range'])) {
                                                        ?>
                                                        <tr>
                                                            <th><?php _e('Plage de puissance compatible', 'beam'); ?></th>
                                                            <td>
                                                                <?php echo $item['compatible_power_range']; ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        <?php } ?>
                                    </div>


                                    <div class="featured-table">
                                        <div class="table-slider">
                                            <?php
                                            foreach ($nozzlesComparativeTable as $item) {
                                                ?>
                                                <div class="table-item">
                                                    <table class="noresp">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                <?php echo $item['reference']; ?>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        if (!empty($item['deposition_width']) || !empty($item['deposition_accuracy'])) {
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($item['deposition_width'])) {
                                                                        ?>
                                                                        <p class="th"><?php _e('Largeur de déposition', 'beam'); ?></p>
                                                                        <p><?php echo $item['deposition_width']; ?></p>
                                                                        <?php
                                                                    }
                                                                    if (!empty($item['deposition_accuracy'])) {
                                                                        ?>
                                                                        <p class="th"><?php _e('Précision de déposition', 'beam'); ?></p>
                                                                        <p><?php echo $item['deposition_accuracy']; ?></p>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        if (!empty($item['deposition_volume']) || !empty($item['typical_build_rate'])) {
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <?php
                                                                    if (!empty($item['deposition_volume'])) {
                                                                        ?>
                                                                        <p class="th"><?php _e('Volume de dépôt', 'beam'); ?></p>
                                                                        <p><?php echo $item['deposition_volume']; ?></p>
                                                                        <?php
                                                                    }
                                                                    if (!empty($item['typical_build_rate'])) {
                                                                        ?>
                                                                        <p class="th"><?php _e('Taux de construction', 'beam'); ?></p>
                                                                        <p><?php echo $item['typical_build_rate']; ?></p>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        if (!empty($item['compatible_power_range'])) {
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <p class="th"><?php _e('Plage de puissance compatible', 'beam'); ?></p>
                                                                    <p><?php echo $item['compatible_power_range']; ?></p>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>




                                <?php
                                if (!empty($nozzlesQuote)) {
                                    ?>
                                    <div class="featured-quote bloc bloc-quote">
                                        <?php echo $nozzlesQuote; ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php

                            if (!empty($nozzlesSlider)) {
                                ?>
                                <div class="featured-slider">
                                    <?php
                                    foreach ($nozzlesSlider as $slide) {
                                        ?>
                                        <div class="slider-item">
                                            <img src="<?php echo $slide['img']['sizes']['medium']; ?>"
                                                 alt="<?php echo $slide['img']['alt']; ?>"/>
                                            <?php
                                            if (!empty($slide['title']) || !empty($slide['desc'])) {
                                                ?>
                                                <div class="item-content">
                                                    <?php
                                                    if (!empty($slide['title'])) {
                                                        ?>
                                                        <div class="item-title"><?php echo $slide['title']; ?></div>
                                                        <?php
                                                    }
                                                    if (!empty($slide['desc'])) {
                                                        ?>
                                                        <div class="item-desc"><?php echo $slide['desc']; ?></div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>

        <?php /* ?>
        <div class="banner-fixed">
            <div class="container">
                <div class="ctn-flex">
                    <a href="<?php echo $technicalDoc['url']; ?>"
                       download="<?php echo $technicalDoc['filename']; ?>"
                       class="link" target="_blank"><?php esc_html_e('Téléchargez la documentation', 'beam'); ?></a>
                    <a href="#page" class="scrolltop"><span
                                class="ico ico-top"></span><?php esc_html_e('Retour en haut', 'beam'); ?></a>
                    <a href="#" data-modal="modal-contact"
                       class="btn btn modal-trigger"><?php esc_html_e('Contactez-nous', 'beam'); ?></a>
                </div>
            </div>
        </div>
  <?php*/ ?>

    </div>

<?php
include(locate_template('template-parts/modal/modal-contact.php', false, false));
get_footer();
?>