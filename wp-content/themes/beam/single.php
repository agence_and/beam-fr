<?php get_header();

$id = get_the_ID();

$postPermalink = get_the_permalink($id);
//$date = get_the_date('F d\,\ Y', $id);
$date = get_the_date('d F Y', $id);
$author = get_the_author();
$category = get_the_terms($id, 'category');

?>
    <div class="single-post">
        <div class="container">
            <div class="page-header ctn-flex">
                <?php get_template_part('template-parts/breadcrumb'); ?>

                <a href="javascript:history.back(1);"><?php esc_html_e('< Retour aux actualités', 'beam'); ?></a>
            </div>

            <div class="container-mini">
                <h1 class="h1"><?php the_title(); ?></h1>
                <div class="post-meta ctn-flex ctn-flex-center">
                    <div class="meta-date">
                        <div class="ico ico-calendar"></div>
                        <div><?php echo $date; ?></div>
                    </div>
                    <?php
                    if (!empty($author)) {
                        ?>
                        <div class="meta-author">
                            <div class="ico ico-author"></div>
                            <div><?php echo $author; ?></div>
                        </div>
                        <?php
                    }
                    if (!empty($category)) {
                        ?>
                        <div class="meta-tag">
                            <div class="ico ico-tag"></div>
                            <div><?php echo $category[0]->name; ?></div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                if (!empty(get_the_excerpt())) {
                    ?>
                    <div class="post-excerpt">
                        <?php echo get_the_excerpt(); ?>
                    </div>
                    <?php
                }
                ?>
            </div>

            <?php
            if (has_post_thumbnail($id)) {
                ?>
                <div class="post-thumbnail">
                    <?php the_post_thumbnail(); ?>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        include(locate_template('template-parts/flexible-content.php', false, false));
        ?>
        <div class="container">
            <div class="container-mini">
                <?php
                $description = 'Partager cet article par email';
                include(locate_template('template-parts/share.php', false, false));
                ?>
            </div>


            <?php
            $args = array(
                'post_type' => 'post',
                'post_status' => array('publish'),
                'posts_per_page' => -1,
                'post__not_in' => array($id),
            );


            $query = new WP_Query($args);

            $cross = array(); // To store only posts with same category
            $all = array(); // To store all posts

            while ($query->have_posts()) {
                $query->the_post();

                $postID = get_the_ID();

                $cat = get_the_terms($postID, 'category');

                if (current(get_the_category())->term_id == $cat) {
                    $cross[] = get_the_ID();
                }

                $all[] = $postID;
            }

            if (count($cross) == 0) // If there is no post with same category ...
            {
                // ... use all posts
                $cross = $all;
            }

            if (count($cross) > 0) {
                ?>
                <div class="crosscontent">
                    <?php $crosscontentTitle = get_field('crosscontent_title', 'option'); ?>
                    <h2 class="h2-alt"><?php esc_html_e($crosscontentTitle, 'beam'); ?></h2>

                    <div class="ctn-flex ctn-flex-start ctn-flex-initial">
                        <?php
                        for ($c = 0; $c < min(count($cross), 4); $c++) {
                            global $post;
                            $post = get_post((int)$cross[$c]);
                            setup_postdata($post);

                            $lastPost = false;
                            include(locate_template('template-parts/post/post-thumb.php', false, false));

                        }
                        ?>
                    </div>

                </div>
                <?php
            }
            ?>

        </div>
    </div>

<?php get_footer(); ?>