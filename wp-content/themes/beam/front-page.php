<?php get_header();

$id = get_the_ID();

$fields = get_fields($id);

$header = $fields['header'];
$products = $fields['products'];
$news = $fields['news'];
$news = $fields['news'];
$hiring = $fields['hiring'];

?>
<div class="page-home">

    <div class="home-header">
        <div class="container">
            <div class="home-header-inner ctn-flex ctn-flex-initial ctn-flex-nowrap">
                <div class="header-left">
                    <h1>
                        <div class="header-logo">
                            <img src="<?php echo $header['logo']['url']; ?>"
                                 alt="<?php echo $header['logo']['alt']; ?>"/>
                        </div>
                        <?php echo $header['title']; ?>
                    </h1>

                    <?php echo $header['desc']; ?>

                    <a href="<?php echo $header['link']['url']; ?>" <?php echo($header['link']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                       class="link"><?php echo $header['link']['title']; ?></a>

                </div>


                <div class="header-right">
                    <div class="header-right-slider">

                        <?php
                        foreach ($header['slider'] as $slide) {

                            if ($slide['slide_type'] == 'img') {
                                ?>
                                <div class="header-slide"
                                     style="background-image: url('<?php echo $slide['img']['url']; ?>');">

                                    <?php if (!empty($slide['link'])) { ?>
                                        <a href="<?php echo $slide['link']['url']; ?>" <?php echo($slide['link']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                                           class="btn"><span><?php echo $slide['link']['title']; ?></span><span
                                                    class="ico ico-arrow-down-mini"></span></a>
                                    <?php } ?>
                                </div>
                                <?php
                            }
                            if ($slide['slide_type'] == 'video') {

                                $videoID = $slide['video_id'];
                                ?>
                                <div class="header-slide header-slide--video">
                                    <div class="tv" data-video="<?php echo $videoID; ?>">
                                        <div class="screen mute active" id="tv"></div>
                                    </div>


                                    <?php if (!empty($slide['link'])) { ?>
                                        <a href="<?php echo $slide['link']['url']; ?>" <?php echo($slide['link']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                                           class="btn"><?php echo $slide['link']['title']; ?></a>
                                    <?php } ?>
                                </div>
                                <?php
                            }
                        }

                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 5760 1043">
        <path d="M0,980.4,5760,0V1043H0Z"/>
    </svg>

    <div class="home-products">
        <div class="container">
            <?php
            if (!empty($products['title'])) {
                ?>
                <h2 class="h2 h2-alt"><?php echo $products['title']; ?></h2>
                <?php
            }
            ?>


            <?php
            $args = array(
                'post_type' => array('products'),
                'post_status' => array('publish'),
                'posts_per_page' => -1,
                'order' => 'DESC',
                'orderby' => 'date',
            );

            // The Query
            $products = new WP_Query($args);

            if ($products->have_posts()) {
                ?>
                <div class="home-products-inner ctn-flex ctn-flex-start ctn-flex-initial">
                    <?php
                    while ($products->have_posts()) {
                        $products->the_post();

                        $productsFields = get_fields(get_the_ID());
                        ?>

                        <a href="<?php the_permalink(get_the_ID()); ?>" class="products-item">
                            <div class="products-item-inner">
                                <div class="item-img">
                                    <img src="<?php echo $productsFields['hp']['img']['sizes']['medium']; ?>"
                                         alt="<?php echo $productsFields['hp']['img']['alt']; ?>"/>
                                </div>
                                <div class="item-desc">
                                    <h4 class="h4"><?php the_title(); ?></h4>
                                    <p><?php echo $productsFields['hp']['desc']; ?></p>
                                </div>
                            </div>
                        </a>

                        <?php

                    }

                    wp_reset_postdata();
                    ?>
                </div>
                <?php
            }
            ?>

        </div>
    </div>


    <?php
    $args = array(
        'post_type' => array('post'),
        'post_status' => array('publish'),
        'posts_per_page' => 4,
        'order' => 'DESC',
        'orderby' => 'date',
    );

    // The Query
    $blogPosts = new WP_Query($args);

    if ($blogPosts->have_posts()) {
        ?>
        <div class="home-news">
            <div class="container">
                <div class="home-news-intro ctn-flex">
                    <?php
                    if (!empty($news['title'])) {
                        ?>
                        <h2 class="h2 h2-alt"><?php echo $news['title']; ?></h2>
                        <?php
                    }

                    if (!empty($news['link'])) {
                        ?>
                        <a href="<?php echo $news['link']['url']; ?>" <?php echo($news['link']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                           class="link"><?php echo $news['link']['title']; ?></a>
                        <?php
                    }
                    ?>
                </div>

                <div class="home-news-inner ctn-flex ctn-flex-start ctn-flex-initial">
                    <?php
                    while ($blogPosts->have_posts()) {

                        $blogPosts->the_post();
                        $count++;
                        include(locate_template('template-parts/post/post-thumb.php', false, false));

                    }

                    wp_reset_postdata();
                    ?>
                </div>


                <?php
                if (!empty($news['link'])) {
                    ?>
                    <div class="btn-ctn">
                        <a href="<?php echo $news['link']['url']; ?>" <?php echo($news['link']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                           class="link"><?php echo $news['link']['title']; ?></a>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>


    <div class="home-hiring">
        <div class="container">
            <div class="home-hiring-inner ctn-flex ctn-flex-center ctn-flex-nowrap">
                <div class="hiring-img">
                    <?php
                    if (!empty($hiring['img'])) {
                        ?>
                        <img src="<?php echo $hiring['img']['sizes']['610x610']; ?>"
                             alt="<?php echo $hiring['img']['alt']; ?>"/>
                        <?php
                    }
                    ?>
                </div>
                <div class="hiring-content">
                    <?php
                    if (!empty($hiring['title'])) {
                        ?>
                        <h2 class="h2-alt"><?php echo $hiring['title']; ?></h2>
                        <?php
                    }
                    if (!empty($hiring['content'])) {
                        echo $hiring['content'];
                    }
                    if (!empty($hiring['list_items'])) {
                        ?>
                        <ul class="content-list ctn-flex ctn-flex-start ctn-flex-initial">
                            <?php
                            foreach ($hiring['list_items'] as $item) {
                                ?>
                                <li class="list-item"><a href="<?php the_permalink($item['item']->ID); ?>"><?php echo $item['item']->post_title; ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    if (!empty($hiring['link'])) {
                        ?>
                        <div class="btn-ctn">
                            <a href="<?php echo $hiring['link']['url']; ?>" <?php echo($hiring['link']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                               class="link"><?php echo $hiring['link']['title']; ?></a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>

<script>
    var videoID = jQuery('.tv').data('video');
    var tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/player_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var tv,
        playerDefaults = {
            autoplay: 0,
            autohide: 1,
            modestbranding: 0,
            rel: 0,
            showinfo: 0,
            controls: 0,
            disablekb: 1,
            enablejsapi: 0,
            iv_load_policy: 3,
            loop: 1
        };
    var vid = [
        {'videoId': videoID, 'suggestedQuality': 'highres'},
    ];

    function onYouTubePlayerAPIReady() {
        tv = new YT.Player('tv', {
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }, playerVars: playerDefaults
        });
    }

    function onPlayerReady() {
        tv.loadVideoById(vid[0]);
        tv.mute();
    }

    function onPlayerStateChange(e) {
        if (e.data === 1) {

        } else if (e.data === 2) {
            tv.loadVideoById(vid[0]);
        }
    }

    function vidRescale() {

        var w = jQuery('.header-slide--video').width(),
            h = jQuery('.header-slide--video').height();

        if (w / h > 16 / 9) {
            tv.setSize(w, w / 16 * 9);
        } else {
            tv.setSize(h / 9 * 16, h);
        }
    }

    jQuery(window).on('load resize', function () {
        vidRescale();
    });
</script>
