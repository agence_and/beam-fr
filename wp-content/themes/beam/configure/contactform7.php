<?php
	// validate phone FR
	
	function _wpcf7_is_tel_fr( $result, $tel ) 
	{
		$result = preg_match( '#^0[0-9]([-. ]?[0-9]{2}){4}$#', $tel );
		return $result;
	}
	add_filter( 'wpcf7_is_tel', '_wpcf7_is_tel_fr', 10, 2 );
	
	// valide date FR
	
	function _wpcf7_is_date_fr( $result, $date ) 
	{
		$result1 = preg_match( '/^([0-9]{4,})-([0-9]{2})-([0-9]{2})$/', $date, $matches1 );
		$result2 = preg_match( '/^([0-9]{2})[- .\/]([0-9]{2})[- .\/]([0-9]{4,})$/', $date, $matches2 );

		if ( $result1 )	$result = checkdate( $matches1[2], $matches1[3], $matches1[1] );
		else if( $result2 )	$result = checkdate( $matches2[2], $matches2[1], $matches2[3] );
		else $result = false;

		return $result;
	}
	add_filter( 'wpcf7_is_date', '_wpcf7_is_date_fr', 10, 2 );
	
	// custom image loader
	
	function _wpcf7_custom_ajax_loader()
	{
		return  get_bloginfo('template_directory') . '/ajax-loader.gif';
	}
	add_filter('wpcf7_ajax_loader', '_wpcf7_custom_ajax_loader');
	
	// tag site url	
 
	function _wpcf7_tag_siteurl( $output, $name, $html )
	{
	   // For backwards compatibility
	   $name = preg_replace( '/^wpcf7\./', '_', $name );
	 
	   if ( 'site_url' == $name ) {
		  $output = get_option( 'siteurl' );
	   }
	 
	   return $output;
	}
	add_filter( 'wpcf7_special_mail_tags', '_wpcf7_tag_siteurl', 20, 3 );
	
	// tag site name	
 
	function _wpcf7_tag_site_name( $output, $name, $html )
	{
	   // For backwards compatibility
	   $name = preg_replace( '/^wpcf7\./', '_', $name );
	 
	   if ( 'site_name' == $name ) {
		  $output = get_option( 'blogname' );
	   }
	 
	   return $output;
	}
	add_filter( 'wpcf7_special_mail_tags', '_wpcf7_tag_site_name', 20, 3 );
	
	// HTML in confirmation / error message
	
	function _wpcf7_html_in_message($output, $ccf7)
	{
		return str_replace('*LINEBREAK*','<br>',$output);
	}
	add_filter( 'wpcf7_display_message', '_wpcf7_html_in_message', 20, 3);