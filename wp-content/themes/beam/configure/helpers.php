<?php

// Custom length excerpt
function custom_theme_helpers_excerpt($limit = '165')
{
    $excerpt = get_the_excerpt();
    $excerpt = preg_replace(" ([.*?])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
//    $excerpt = trim(preg_replace('/s+/', ' ', $excerpt));
    $excerpt = $excerpt . '...';
    return $excerpt;
}

// Custom breadcrumb
/*function adjust_single_breadcrumb( $link_output) {
    if(strpos( $link_output, 'breadcrumb_last' ) !== false ) {
        $link_output = '';
    }
    return $link_output;
}
add_filter('wpseo_breadcrumb_single_link', 'adjust_single_breadcrumb' );*/


/**
 * Display time since post was published
 *
 * @uses    human_time_diff()  Return time difference in easy to read format
 * @uses    get_the_time()  Get the time the post was published
 * @uses    current_time()  Get the current time
 *
 * @return    string  Timestamp since post was published
 *
 * @author c.bavota
 */
function get_time_since_posted($id)
{
    $time_since_posted = human_time_diff( get_the_time('U', $id), current_time('timestamp') );

    return $time_since_posted;
}
