<?php
/*	function _acf_set_google_api_key()
	{
		// Need to enable Google Places API Web Service in Google Developer Console


		$options = get_option( 'custom_theme_settings' );
		
		return $options['custom_theme_settings_google_maps_api_key'];
	}
	add_filter( 'acf/settings/google_api_key', '_acf_set_google_api_key' );*/

function my_acf_init() {

    $googleAPIKey = get_field('google_api', 'option');

    acf_update_setting('google_api_key', $googleAPIKey);
}

add_action('acf/init', 'my_acf_init');

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'General site settings',
        'menu_title'	=> 'General site settings',
        'menu_slug' 	=> 'general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));


    acf_add_options_sub_page(array(
        'page_title' 	=> 'Header',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Footer',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'News',
        'menu_title'	=> 'News',
        'parent_slug'	=> 'general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Careers',
        'menu_title'	=> 'Careers',
        'parent_slug'	=> 'general-settings',
    ));

}