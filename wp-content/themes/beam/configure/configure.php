<?php
//add_filter( 'show_admin_bar', '__return_false' );

// MENUS

function _custom_theme_register_menu()
{
    register_nav_menus(
        array(
            'main-menu' => __('Main menu'),
            'header-top-menu' => __('Top header menu'),
            'footer-menu' => __('Footer menu'),
            'footer-bottom-menu' => __('Footer submenu'),
        )
    );
}

add_action('init', '_custom_theme_register_menu');

/*class Menu_With_Submenus extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth)
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu\"><div class=\"container\"><div class=\"ctn-flex ctn-flex-initial ctn-flex-start ctn-flex-nowrap ctn-flex-no-mg\">\n";
    }

    function end_lvl(&$output, $depth)
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</div></ul>\n";
    }
}*/

class Menu_With_Submenus extends Walker_Nav_Menu
{
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $full = get_field('full', $item->ID);
        if ($full == true) {
            $full = 'menu-subitem-img';
        }
        $itemWithImage = get_field('img', $item->ID);
        $partnerTax = get_field('anchor', $item->ID);


        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $classes[] = 'menu-item-' . $item->ID . ' ' . $full;

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names . '>';

        // MODIF 1
        // si le lien est vide...
        // ou s'il comment par '#' (ancre)...
        // alors la balise sera un 'span' ou lieu d'un 'a'
        // MODIF 1
        // $balise = a seulement si il y a un lien et que ce n'est pas une ancre
        // $balise = a si current-menu-item n'est pas dans les classes
        $balise = (!empty($item->url) && substr($item->url, 0, 1) != '#' && !in_array('current-menu-item', $classes)) ? 'a' : 'span';

        $atts = array();
        $atts['title'] = !empty($item->attr_title) ? $item->attr_title : '';
        $atts['target'] = !empty($item->target) ? $item->target : '';
        $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';

        // MODIF 2 : on ajoute l'URL seulement si c'est un lien
        if ('a' == $balise)
            if(!empty($partnerTax)){
                $atts['href'] = !empty($item->url) ? $item->url.'#'.$partnerTax->slug : '';
            } else {
                $atts['href'] = !empty($item->url) ? $item->url : '';
            }


        $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args);

        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $thumb = '';

        if ($item->object == 'products' && $itemWithImage == true) {
            $fieldProduct = get_field('hp', (int)$item->object_id);
            $img = $fieldProduct['img']['url'];
            $thumb = '<div class="menu-item-image"><img src="' . $img . '" alt="' . $item->title . '"></div>';
        }

        if ($item->object == 'page' && $itemWithImage == true && has_post_thumbnail((int)$item->object_id)) {
            $thumb = '<div class="menu-item-image--page">'.get_the_post_thumbnail((int)$item->object_id, 'full', array('alt' => $item->title)).'</div>';
        }




        // MODIF 3 : on remplace 'a' par $balise
        $item_output = $args->before;
        $item_output .= '<' . $balise . '' . $attributes . '>';
        $item_output .= $thumb;
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= '</' . $balise . '>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu sub-menu-$depth\"><div class=\"container\"><div class=\"ctn-flex ctn-flex-initial ctn-flex-start ctn-flex-nowrap ctn-flex-no-mg\">\n";
    }

    function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</div></ul>\n";
    }
}



// IMAGES

add_theme_support('post-thumbnails');

function _custom_theme_init_images_size()
{
    add_image_size( '500x500', 500, 500, true );
    add_image_size( '610x610', 610, 610, true );
    add_image_size('290x184', 290, 184, true);
    add_image_size('290x184-nocrop', 290, 184, false);
}

add_action('init', '_custom_theme_init_images_size');

add_filter('jpeg_quality', create_function('', 'return 70;'));

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support('title-tag');