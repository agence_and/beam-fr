<?php
	function _custom_theme_search_query( $query )
	{
        if ( is_search() && $query->is_main_query() && $query->get( 's' ) ){

            $query->set('post_type', array('post', 'page', 'products', 'jobs'));
        }

        return $query;
	}
	add_filter( 'pre_get_posts', '_custom_theme_search_query' );