<?php

/*
*Custom admin styles
*/
add_action('admin_head', '_custom_styles_admin_pages');
function _custom_styles_admin_pages()
{
    echo '<style>
	  body[class*="settings_page"] .form-table td input:not([type="checkbox"]):not([type="radio"]), body[class*="settings_page"] .form-table td textarea{
	    width: 100% !important;
	    padding: 8px;
	  }
	  body[class*="settings_page"] .form-table{
	    border-collapse: separate;
        background: #e5e5e5;
        padding: 15px;
	    margin-bottom: 50px;
	    border-bottom: 2px solid #ccc;
	  }
    </style>';
}