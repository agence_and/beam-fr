<?php
	function _custom_shortcode_menu( $atts )
	{
		// [menu location="" container="" class=""]
		
		$return = '';
		
		$location = $atts['location'];
		$container = ( $atts['container'] == '' ? false : $atts['container'] );
		$class = $atts['class'];
		
		return wp_nav_menu(
            array(
                'theme_location' 	=> $location,
				'container'			=> $container,
				'menu_class'		=> $class,
				'echo' 				=> false
            )
		);
	}
	add_shortcode( 'menu', '_custom_shortcode_menu' );