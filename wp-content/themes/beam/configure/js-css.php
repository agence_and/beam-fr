<?php

// Custom Scripting to Move JavaScript from the Head to the Footer
function remove_head_scripts()
{
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}

add_action('wp_enqueue_scripts', 'remove_head_scripts');
// END Custom Scripting to Move JavaScript


function _add_javascript()
{
    $googleAPIKey = get_field('google_api', 'option');

    wp_enqueue_script('boostrap-js', get_template_directory_uri() . '/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js', false, null, true);
    wp_enqueue_script('boostrap-validator-js', get_bloginfo('template_directory') . '/bower_components/bootstrap-validator/dist/validator.min.js', false, null, true);
//    wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js', false, null, true);
    wp_enqueue_script('google-map-api', 'https://maps.googleapis.com/maps/api/js?key='.$googleAPIKey, false, null, true);
    wp_enqueue_script('select2-js', get_bloginfo('template_directory') . '/bower_components/select2/dist/js/select2.min.js', false, null, true);
    wp_enqueue_script('flipster', get_template_directory_uri() . '/bower_components/jquery-flipster/dist/jquery.flipster.min.js', array('jquery'), null, true);
    wp_enqueue_script('slick', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.min.js', array('jquery'), null, true);
    wp_enqueue_script('jscroll-js', get_template_directory_uri() . '/bower_components/jscroll/jquery.jscroll.js', array('jquery'), null, true);
    wp_enqueue_script('sticky-js', get_template_directory_uri() . '/bower_components/jquery-sticky/jquery.sticky.js', array('jquery'), null, true);

    // Product View
/*    if (get_post_type() == 'products' && is_single()) {
        wp_enqueue_script('jquery-mousewheel', get_template_directory_uri() . '/assets/dist/js/lib/jquery.mousewheel.min.js', array('jquery'), null, true);
        wp_enqueue_script('jquery-waypoints', get_template_directory_uri() . '/assets/dist/js/lib/jquery.waypoints.min.js', array('jquery'), null, true);
        wp_enqueue_script('smooth-scroll', get_template_directory_uri() . '/assets/dist/js/lib/smooth-scroll.min.js', array('jquery'), null, true);
        wp_enqueue_script('scroll-video', get_template_directory_uri() . '/assets/dist/js/smooth-video.min.js', array('jquery'), null, true);

    }*/

    // Google Insights : load Cookie Law Info JS in footer
//        wp_dequeue_script( 'cookie-law-info-script' );
////        wp_enqueue_script( 'cookie-law-info-script', CLI_PLUGIN_URL . 'js/cookielawinfo.js', array( 'jquery' ), '1.5.3', true );
//        wp_enqueue_script( 'cookie-law-info-script', plugin_dir_url('cookie-law-info/js/cookielawinfo.js'), array( 'jquery' ), '1.5.3', true );


    wp_enqueue_script('js1', get_template_directory_uri() . '/assets/dist/js/app.min.js', array('jquery'), '20151215', true);
}

add_action('wp_enqueue_scripts', '_add_javascript', 100);

function _add_stylesheets()
{
    /* Async : https://www.keycdn.com/blog/google-pagespeed-insights-wordpress/
    // See _footer_assets() */
//    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Libre+Baskerville:400i|Raleway:400,400i,500,600,700|Roboto:500');

    // Google Insights : be careful in bootstrap and main CSS call order
    //wp_enqueue_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '20141106', 'all' );

    /*  wp_enqueue_style('select2-css', get_bloginfo('template_directory') . '/bower_components/select2/dist/css/select2.min.css', array(), null, 'all' );
      wp_enqueue_style('slick-css', get_bloginfo('template_directory') . '/bower_components/slick-carousel/slick/slick.css', array(), null, 'all' );
      wp_enqueue_style('slick-default-css', get_bloginfo('template_directory') . '/bower_components/slick-carousel/slick/slick-theme.css', array(), null, 'all' );*/
    wp_enqueue_style('css1', get_template_directory_uri() . '/assets/dist/css/style.min.css', array(), '20141106', 'all');
    if (get_post_type() == 'products' && is_single()) {
        wp_enqueue_style('animate', get_template_directory_uri() . '/assets/dist/css/lib/animate.min.css', array(), '20171201', 'all');
        wp_enqueue_style('scroll-video', get_template_directory_uri() . '/assets/dist/css/smooth-video.min.css', array(), '20171201', 'all');
    }
}

add_action('wp_enqueue_scripts', '_add_stylesheets');

function _footer_assets()
{

    ?>

    <script>

        jQuery(function ($) {

            if ($('.infinite').length > 0) {
                $('.infinite').not('.infinite-link').jscroll({
                    autoTrigger: false,
                    nextSelector: 'a.jscrollnext:last',
                    contentSelector: '.jscroll',
                    loadingHtml: '<div class="btn-ctn btn-ctn--loadmore"><div class="btn-loadmore jscrollnext"><span>Chargement ...</span></div></div>',
                    callback: function () {
                    }
                });
                $('.infinite.infinite-link').jscroll({
                    autoTrigger: false,
                    nextSelector: 'a.jscrollnext:last',
                    contentSelector: '.jscroll',
                    loadingHtml: '<div class="btn-ctn"><a class="link jscrollnext"><span>Chargement ...</span></a></div>',
                    callback: function () {
                    }
                });
            }

        });

    </script>

    <?php
    if (!is_admin()) {
        // Google Insights :
        // - Load CDN Async : https://www.keycdn.com/blog/google-pagespeed-insights-wordpress/
        // - Combine multiple fonts into one request : https://fonts.googleblog.com/2010/09/optimizing-use-of-google-font-api.html
        // - Load CSS from WP and JS plugins in footer
        ?>

        <link rel="stylesheet" id="select2-css"
              href="<?php echo get_bloginfo('template_directory') . '/bower_components/select2/dist/css/select2.min.css'; ?>"
              type="text/css" media="all">

        <link rel='stylesheet' id='slick-theme-css-css'
              href='<?php bloginfo('template_directory'); ?>/bower_components/jquery-flipster/dist/jquery.flipster.css'
              type='text/css' media='all'/>


        <link rel='stylesheet' id='slick-theme-css-css'
              href='<?php bloginfo('template_directory'); ?>/bower_components/slick-carousel/slick/slick-theme.css?ver=1.5.7'
              type='text/css' media='all'/>
        <link rel='stylesheet' id='slick-css-css'
              href='<?php bloginfo('template_directory'); ?>/bower_components/slick-carousel/slick/slick.css?ver=1.5.7'
              type='text/css' media='all'/>

        <link rel='stylesheet' id='cookielawinfo-style-css'
              href='<?php echo plugin_dir_path('cookie-law-info'); ?>/css/cli-style.css?ver=1.5.3' type='text/css'
              media='all'/>

        <?php


    }
    ?>
    <?php
    $init_recaptcha = false;
    if( function_exists( '_wp_and_share_by_email_footer_assets' ) )
    {
        $options = get_option( WP_AND_SHARE_BY_EMAIL_SETTINGS_OPTION_NAME );

        $init_recaptcha = ( (int)$options['wp_and_share_by_email_settings_recaptcha_active'] != 1 );
    }
    else
    {
        $init_recaptcha = true;
    }

    if( $init_recaptcha ) {
        ?>
        <script type="text/javascript">
            var recaptchaWidgets = new Object();

            var recaptchaCallback = function () {
                jQuery('.g-recaptcha').each(
                    function () {
                        id = jQuery(this).attr('id');

                        widget = grecaptcha.render(id, {
                            'sitekey': jQuery(this).data('sitekey')
                        });

                        recaptchaWidgets[id] = widget;
                    }
                );
            };
        </script>
        <script src="https://www.google.com/recaptcha/api.js?hl=fr&onload=recaptchaCallback&render=explicit" async
                defer></script>
        <?php
    }

}

add_action('wp_footer', '_footer_assets');