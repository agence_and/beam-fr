<?php
	function _tinymce_style_select( $buttons )
	{
		array_unshift( $buttons, 'styleselect' );
		
		return $buttons;
	}
	add_filter( 'mce_buttons_2', '_tinymce_style_select' );
	
	function _tinymce_insert_formats( $init_array )
	{  
		// See https://codex.wordpress.org/TinyMCE_Custom_Styles
		
		// Make sure "Insert editor-style.css" is not checked in TinyMCE Advanced settings
		
		$style_formats = array(  
			array(  
				'title' 	=> 'small',  
				'selector' 	=> 'p',  
				'classes'	=> 'small'             
			),
			array(  
				'title' 	=> 'one-line-button',  
				'selector' 	=> 'a',  
				'classes'	=> 'btn btn-primary one-line'             
			),
			array(  
				'title' 	=> 'two-lines-button',  
				'selector' 	=> 'a',  
				'classes'	=> 'btn btn-primary two-lines'             
			),
		);  
		$init_array['style_formats'] = json_encode( $style_formats );  
	
		return $init_array;  
	
	} 
	add_filter( 'tiny_mce_before_init', '_tinymce_insert_formats' );