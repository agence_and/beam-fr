<?php get_header();

$id = get_the_ID();

$bg = get_the_post_thumbnail_url();
if(empty($bg)){
    $bg = get_template_directory_uri() . '/assets/dist/img/header-default.jpg';
}

?>
<div class="page-default">
    <div class="page-intro" style="background-image: url(<?php echo $bg; ?>)">
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php the_title(); ?>
                </h1>
                <?php the_content(); ?>
            </div>

        </div>
    </div>
    <div class="page-content">
        <?php
        include(locate_template('template-parts/flexible-content.php', false, false));
        ?>
    </div>
</div>


<?php get_footer(); ?>