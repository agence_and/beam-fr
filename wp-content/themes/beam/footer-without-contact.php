</div><!-- #content -->

<?php
$footerBaseline = get_field('footer_baseline', 'option');
$footerLogo = get_field('footer_logo', 'option');
$footerDesc = get_field('footer_desc', 'option');
$socialNetworks = get_field('social', 'option');
?>

<footer id="colophon" class="site-footer" role="contentinfo">

    <div class="container">
        <div class="footer-top ctn-flex ctn-flex-no-mg">
            <?php
            if (!empty($footerLogo)) {
                ?>
                <div class="footer-logo">
                    <img src="<?php echo $footerLogo['sizes']['medium']; ?>" alt="<?php echo $footerLogo['alt']; ?>"/>
                </div>
                <?php
            }
            if (!empty($footerBaseline)) {
                ?>
                <div class="footer-baseline">
                    <img src="<?php echo $footerBaseline['sizes']['medium']; ?>"
                         alt="<?php echo $footerBaseline['alt']; ?>"/>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="footer-main ctn-flex ctn-flex-top ctn-flex-no-mg">
            <?php
            if (!empty($footerDesc)) {
                ?>
                <div class="footer-desc">
                    <?php echo $footerDesc; ?>
                </div>
                <?php
            }

            wp_nav_menu(array('theme_location' => 'footer-menu', 'menu_class' => 'footer-main-menu'));

            if (!empty($socialNetworks)) {
                ?>
                <div class="footer-social">
                    <div class="ctn-flex ctn-flex-start ctn-flex-mini">
                        <?php
                        foreach ($socialNetworks as $social) {
                            ?>
                            <a href="<?php echo $social['url']; ?>" target="_blank"
                               title="<?php esc_html_e('Follow us', 'beam'); ?>">
                            <span class="ico"
                                  style="background-image: url('<?php echo $social['ico']['sizes']['thumbnail']; ?>');"></span>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="footer-bottom ctn-flex ctn-flex-no-mg">
            <div class="footer-copyright">
                <?php esc_html_e('Copyright', 'beam'); ?> &copy; <?php echo date('Y') . ' ' . get_bloginfo('name'); ?>
            </div>
            <?php
            wp_nav_menu(array('theme_location' => 'footer-bottom-menu', 'menu_class' => 'footer-bottom-menu ctn-flex ctn-flex-start'));
            ?>
        </div>
    </div>

</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
