<?php

get_header();

$id = get_the_ID();

if (is_category()) {
    $currentCat = get_queried_object();
    $currentCatId = $currentCat->term_id;
}

$permalink = get_post_type_archive_link('post');

$socialFeed = get_field('social_feed', 'option');

$posts_per_page = (int)get_option('posts_per_page');

?>


    <div class="page-news">
        <?php

        if (!is_category()) {
            $argsLast = array(
                'post_type' => array('post'),
                'post_status' => array('publish'),
                'posts_per_page' => 5,
                'order' => 'DESC',
                'orderby' => 'date',
            );
            $sliderItems = new WP_Query($argsLast);

            $sliderItemsID = array();

            if ($sliderItems->have_posts()) {
                ?>
                <div class="news-slider">
                    <div class="news-slider-container">
                    <?php
                    while ($sliderItems->have_posts()) {
                        $sliderItems->the_post();
                        $itemID = get_the_ID();
                        $sliderItemsID[] = $itemID;
                        $itemTitle = get_the_title();
                        $itemBg = get_the_post_thumbnail_url($itemID);
                        ?>
                        <a href="<?php the_permalink(); ?>" class="news-slide" style="background-image: url(<?php echo $itemBg; ?>);">
                            <div class="container">
                                <div class="slide-title">
                                    <div class="title-tag"><?php esc_html_e('Dernières actualités', 'beam'); ?></div>
                                    <h2 class="h2"><?php echo $itemTitle; ?></h2>
                                </div>
                            </div>
                        </a>
                        <?php
                    }
                    ?>
                    </div>
                    <div class="slider-nav ctn-flex ctn-flex-start ctn-flex-nowrap ctn-flex-no-mg"></div>
                </div>
                <?php
            }
        }
        ?>

        <div class="container">
            <div class="page-header ctn-flex">
                <?php get_template_part('template-parts/breadcrumb'); ?>

                <form name="filters" id="filters" class="filters" method="get" action="<?php echo $permalink; ?>"
                      novalidate>

                    <label><?php esc_html_e('Filtrer :', 'beam'); ?></label>
                    <select name="filter" id="filtercat">


                        <?php if (is_category()) { ?>

                            <option value="<?php echo $currentCat->slug; ?>"><?php echo $currentCat->name; ?></option>
                            <option value="<?php echo $permalink; ?>"><?php esc_html_e('Tous les sujets', 'beam'); ?></option>

                        <?php } else { ?>

                            <option value="<?php echo $permalink; ?>"><?php esc_html_e('Tous les sujets', 'beam'); ?></option>

                        <?php } ?>


                        <?php
                        $args = array(
                            'hide_empty' => true,
                            'orderby' => 'name',
                            'order' => 'ASC'
                        );

                        $cats = get_categories($args);

                        foreach ($cats as $cat) {

                            if ($cat->term_id == $currentCatId) {
                                continue;
                            } else {
                                $catLink = get_category_link($cat->term_id);
                                $catName = $cat->name;
                                ?>
                                <option value="<?php echo $catLink; ?>"><?php echo $catName; ?></option>

                                <?php
                            }
                        }
                        ?>
                    </select>

                </form>

            </div>
        </div>
        <div class="posts">

            <?php
            if (get_query_var('paged')) {
                $paged = get_query_var('paged');
            } elseif (get_query_var('page')) {
                $paged = get_query_var('page');
            } else {
                $paged = 1;
            }


            if (is_category()) {
                $args = array(
                    'post_type' => array('post'),
                    'post_status' => array('publish'),
                    'posts_per_page' => $posts_per_page,
                    'order' => 'DESC',
                    'orderby' => 'date',
                    'paged' => $paged,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'terms' => $currentCatId,
                            'field' => 'id',
                            'include_children' => true,
                            'operator' => 'IN'
                        )
                    ),
                );
            } else {
                $args = array(
                    'post_type' => array('post'),
                    'post_status' => array('publish'),
                    'posts_per_page' => $posts_per_page,
                    'order' => 'DESC',
                    'orderby' => 'date',
                    'paged' => $paged,
                    'post__not_in' => $sliderItemsID
                );
            }


            // The Query
            $blogPosts = new WP_Query($args);

            if ($blogPosts->have_posts()) {
                $count = 0;
                ?>
                <div class="container infinite">
                    <div class="jscroll">
                        <div class="ctn-flex ctn-flex-start ctn-flex-initial">

                            <?php
                            while ($blogPosts->have_posts()) {

                                $blogPosts->the_post();
                                $count++;
                                include(locate_template('template-parts/post/post-thumb.php', false, false));

                            }

                            wp_reset_postdata();


                            // don't display the button if there are not enough posts
                            if (wp_count_posts()->publish >= $posts_per_page) {

                                if (is_category()) {
                                    $currentCat = get_queried_object();
                                    $currentCatId = $currentCat->term_id;
                                    $permalinkCat = get_category_link($currentCat);

                                    $next_page_permalink = esc_url($permalinkCat) . '/page/' . ($paged + 1);
                                } else {
                                    $next_page_permalink = esc_url($permalink) . '/page/' . ($paged + 1);
                                }


                                echo '<div class="btn-ctn btn-ctn--loadmore"><a href="' . esc_url($next_page_permalink) . '" class="btn-loadmore jscrollnext">';
                                echo '<span>' . __('Voir + d\'actualités', 'beam') . '</span>';
                                echo '</a></div>';
                            }

                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }

            ?>

        </div>
        <div class="social-posts">

            <div class="container">
                <div class="container-mini">
                    <h2 class="h2-alt"><?php echo $socialFeed['title']; ?></h2>
                    <div class="btn-ctn">
                        <a href="<?php echo $socialFeed['url']; ?>" class="btn-twitter twitter-follow-button"
                           target="_blank"
                           data-show-count="true" data-size="large">
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                            <span class="ico ico-twitter-btn"></span>
                            <?php esc_html_e('Suivre @BeAM_3DPrinting', 'beam'); ?>
                        </a>
                    </div>
                </div>
            </div>

            <div class="social-slider">

                <?php
                $social = array(
                    'twitter' => array(),
                );

                $argsSocial = array(
                    'post_type' => 'social_post',
                    'post_status' => array('publish'),
                    'posts_per_page' => 4,
                    'orderby' => 'date',
                    'order' => 'DESC',
                );

                $query = new WP_Query($argsSocial);

                while ($query->have_posts()) {
                    $query->the_post();


                    $socialID = $post->ID;

                    $metas_social = get_post_meta($socialID);
                    $socialDate = get_time_since_posted($socialID);


                    $post = array(
                        'permalink' => $metas_social['social_permalink'][0],
                        'content' => get_the_content(),
                        'author' => $metas_social['social_author_name'][0],
                        'authorScreen' => $metas_social['social_author_screenname'][0],
                        'authorUrl' => $metas_social['social_author_permalink'][0],
                        'authorThumb' => $metas_social['social_author_thumb'][0]
                    );


                    if ($metas_social['social_network'][0] == 'twitter') {
                        $social['twitter'][] = $post;
                    }

                }

                while (list($network, $social_posts) = each($social)) {

                    for ($p = 0; $p < min(count($social_posts), 4); $p++) {
                        $post = $social_posts[$p]; ?>
                        <div class="bloc bloc-testimonies bloc-tweet bloc-card ">
                            <div class="bloc-card-quote-ctn">

                                <?php
                                if (!empty($post['authorThumb'])) {
                                    ?>
                                    <div class="card-quote-img">
                                        <img src="<?php echo $post['authorThumb'] ?>"
                                             alt="<?php echo $post['author']; ?>"/>
                                    </div>
                                    <?php
                                }
                                ?>

                                <a href="<?php echo esc_url($post['permalink']); ?>" target="_blank"
                                   class="bloc-card-quote">
                                    <div class="card-author">
                                        <?php echo $post['author']; ?>
                                        <span>@<?php echo $post['authorScreen']; ?></span>
                                    </div>


                                    <div class="card-date"><?php esc_html_e('il y a ', 'beam'); ?><?php echo $socialDate; ?></div>

                                    <div class="card-quote">
                                        <p><?php echo $post['content']; ?></p>
                                    </div>

                                </a>
                            </div>
                        </div>
                        <?php

                    }

                }
                ?>

            </div>


        </div>
    </div>

<?php get_footer(); ?>