<?php
$content = get_sub_field('content');
$img = get_sub_field('img');
?>

<div class="bloc bloc-txt-img">
    <div class="container">
        <div class="bloc-txt-img-inner ctn-flex ctn-flex-initial ctn-flex-no-mg">
            <div class="inner-txt">
                <?php echo $content; ?>
            </div>
            <div class="inner-img" style="background-image: url(<?php echo $img['sizes']['large']; ?>);"></div>
        </div>
    </div>
</div>