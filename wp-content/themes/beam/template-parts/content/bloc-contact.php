<?php
$options = get_option(BEAM_FORMS_SETTINGS_OPTION_NAME);
$titleDesc = $options['beam_forms_settings_'.$form.'_title'];
$contentDesc = $options['beam_forms_settings_'.$form.'_desc'];
$noticeDesc = $options['beam_forms_settings_'.$form.'_notice'];
$titleForm = $options['beam_forms_settings_'.$form.'_form_title'];
?>

<div class="bloc bloc-contact">
    <div class="container">
        <div class="ctn-flex ctn-flex-top">
            <div class="contact-desc">
                <?php
                if (!empty($titleDesc)) {
                    ?>
                    <div class="contact-desc-title">
                        <?php echo $titleDesc; ?>
                    </div>
                    <?php
                }
                if (!empty($contentDesc)) {
                    ?>
                    <div class="contact-desc-content">
                        <?php echo $contentDesc; ?>
                    </div>
                    <?php
                }
                if (!empty($noticeDesc)) {
                    ?>
                    <div class="contact-desc-notice">
                        <?php echo $noticeDesc; ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="contact-form">
                <?php
                if (!empty($titleForm)) {
                    ?>
                    <div class="contact-form-title">
                        <?php echo $titleForm; ?>
                    </div>
                    <?php
                }

                if($form == 'press'){
                    beam_press_form();
                } else {
                    beam_contactmini_form();
                }

                ?>
            </div>
        </div>
    </div>
</div>
