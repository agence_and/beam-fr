<?php
$mainContent = get_sub_field('main_content');
$quote = get_sub_field('quote');
?>

<div class="bloc bloc-card">
    <div class="container">
        <div class="bloc-card-inner">
            <div class="bloc-card-content">
                <?php
                echo $mainContent['content'];
                $count = 0;
                if (!empty($mainContent['buttons'])) {
                    ?>
                    <div class="bloc-buttons ctn-flex ctn-flex-start ctn-flex-mini">
                        <?php
                        foreach ($mainContent['buttons'] as $btn) {
                            $count++;
                            ?>
                            <a href="<?php echo $btn['button']['url']; ?>" <?php echo($btn['button']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                               class="btn <?php echo($count == 1 ? '' : 'btn-grey'); ?>">
                                <?php echo $btn['button']['title']; ?>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="bloc-card-quote">
                <p class="card-quote">
                    <?php echo $quote['quote']; ?>
                </p>
                <?php
                if (!empty($quote['author'])) {
                    ?>
                    <div class="card-quote-author">
                        <?php echo $quote['author']; ?>
                    </div>
                    <?php
                }
                if (!empty($quote['function'])) {
                    ?>
                    <div class="card-quote-function">
                        <?php echo $quote['function']; ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
