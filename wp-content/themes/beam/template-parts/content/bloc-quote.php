<?php
    $txt = get_sub_field('content');
?>
<div class="bloc bloc-quote">
    <div class="container">
        <div class="container-mini">
            <?php echo $txt; ?>
        </div>
    </div>
</div>
