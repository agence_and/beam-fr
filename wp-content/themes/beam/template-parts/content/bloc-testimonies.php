<?php
$testimonies = get_sub_field('testimonies');
if (!empty($testimonies)) {
    ?>
    <div class="bloc bloc-testimonies bloc-card">
        <div class="container">
            <ul class="flipster-slider testimonies-slider">
                <?php foreach ($testimonies as $testimony) {
                    $fields = get_fields($testimony['testimony']);
                    $author = $fields['author'];
                    $quote = $fields['testimony'];
                    $link = $fields['link'];
                    ?>
                    <li class="bloc-card-quote-ctn">
                        <?php
                        if (!empty($author['img'])) {
                            ?>
                            <div class="card-quote-img">
                                <img src="<?php echo $author['img']['sizes']['thumbnail']; ?>"
                                     alt="<?php echo $author['img']['alt']; ?>"/>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="bloc-card-quote">
                            <?php
                            if (!empty($author['name'])) {
                                ?>
                                <div class="card-quote-author">
                                    <?php echo $author['name']; ?>
                                </div>
                                <?php
                            }
                            if (!empty($author['function'])) {
                                ?>
                                <div class="card-quote-function">
                                    <?php echo $author['function']; ?>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="card-quote">
                                <?php echo $quote; ?>
                            </div>
                            <div class="card-btn">
                                <a href="<?php echo $link['url']; ?>" <?php echo($link['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                                   class="link">
                                    <?php echo $link['title']; ?>
                                </a>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php } ?>