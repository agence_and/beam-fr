<?php
    $txt = get_sub_field('content');
    $txtCentered = get_sub_field('content_centered');
    $txtDivided = get_sub_field('content_divided');
?>
<div class="bloc bloc-text<?php echo($txtCentered == true ? ' bloc-text--centered' : ''); ?><?php echo($txtDivided == true ? ' bloc-text--divided' : ''); ?>">
    <div class="container">
        <div class="container-mini">
            <?php echo $txt; ?>
        </div>
    </div>
</div>
