<?php
$content = get_sub_field('content');
$secContentType = get_sub_field('content_type');
$secContentPosition = get_sub_field('content_position');
$centered = get_sub_field('vertically_centered');
?>

<div class="bloc bloc-cols">
    <div class="container">
        <div class="bloc-cols-inner-ctn ctn-flex<?php echo($secContentPosition == 'left' ? ' ctn-flex-start' : ' ctn-flex-end'); ?>">
            <div class="bloc-cols-inner ctn-flex ctn-flex-nowrap ctn-flex-start<?php echo($secContentPosition == 'left' ? ' ctn-flex-row-reverse' : ''); ?><?php echo($centered != 'true' ? ' ctn-flex-top' : ''); ?>">
                <div class="cols-main">
                    <?php echo $content; ?>
                </div>

                <div class="cols-secondary">
                    <?php
                    if ($secContentType == 'img') {

                        $img = get_sub_field('img');
                        ?>
                        <div class="cols-secondary-img">
                            <img src="<?php echo $img['sizes']['large']; ?>" alt="<?php echo $img['alt']; ?>"/>
                        </div>
                        <?php
                    }
                    if ($secContentType == 'wysiwyg') {
                        $wysiwyg = get_sub_field('wysiwyg');
                        ?>
                        <div class="cols-secondary-wysiwyg">
                            <?php echo $wysiwyg; ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
