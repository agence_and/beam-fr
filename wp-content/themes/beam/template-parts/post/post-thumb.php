<?php

$postID = get_the_ID();
$postPermalink = get_the_permalink();
$date = get_the_date('d.m.Y', $postID);
$extraClass = 'post-card';


?>

<article id="post-<?php the_ID(); ?>" <?php post_class($extraClass); ?>>

    <a href="<?php echo $postPermalink; ?>" rel="bookmark">

        <div class="post-thumbnail">
            <?php
            if (has_post_thumbnail() || !empty($images)) {
                if (get_post_type($postID) != 'post') {
                    ?>
                    <img src="<?php echo $images[0]['sizes']['290x184']; ?>" alt="<?php echo $images[0]['alt']; ?>"/>
                    <?php
                } else {
                    the_post_thumbnail('290x184');
                }
            } else { ?>
                <img src="<?php bloginfo('template_directory'); ?>/assets/dist/img/post-default.jpg" alt="BeAM">
                <?php
            }
            ?>

            <div class="overlay overlay--green">
                <div class="ico ico-eye"></div>
            </div>
        </div>

        <div class="post-desc">

            <?php
/*            $category = get_the_category($postID);
            echo  $category[0]->name; */?>
            <h3 class="post-title">
                <?php /*echo $postID */?><!-- - -->
                <?php the_title(); ?>
            </h3>

            <?php
            if (!empty(get_the_excerpt())) {
                ?>
                <div class="post-excerpt">
                    <p><?php echo custom_theme_helpers_excerpt(132); ?></p>
                </div>
                <?php
            }
            ?>
        </div>

    </a>

</article><!-- #post-## -->