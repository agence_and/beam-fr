<div class="post-share ctn-flex ctn-flex-no-mg">
    <span>
        <?php esc_html_e($description, 'beam'); ?>
    </span>
    <div class="share-networks ctn-flex">
        <a href="#" class="modal-trigger" data-modal="modal-email">
            <span class="ico ico-share-email"></span>
            <?php esc_html_e( 'Email', 'beam' ); ?>
        </a>

        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( $postPermalink ); ?>" target="_blank">
            <span class="ico ico-share-facebook"></span>
            <?php esc_html_e( 'Facebook', 'beam' ); ?>
        </a>

        <a href="https://twitter.com/home?status=<?php echo urlencode( $postTitle ); ?>%20<?php echo urlencode( $postPermalink ); ?>" target="_blank">
            <span class="ico ico-share-twitter"></span>
            <?php esc_html_e( 'Twitter', 'beam' ); ?>
        </a>

        <a href="https://plus.google.com/share?url=<?php echo urlencode( $postPermalink ); ?>" target="_blank">
            <span class="ico ico-share-google"></span>
            <?php esc_html_e( 'Google +', 'beam' ); ?>
        </a>
    </div>
</div>

<?php include(locate_template('template-parts/modal/modal-mail.php', false, false)); ?>