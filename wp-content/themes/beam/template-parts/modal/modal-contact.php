<?php
    $modalTitle = esc_html('Contactez-nous', 'beam');
    $modalDesc = esc_html('Complétez le formulaire pour obtenir plus d\'informations sur', 'beam');
?>

<div class="modal modal-contact" tabindex="-1" role="dialog" aria-labelledby="<?php echo $modalTitle; ?>" aria-hidden="true" id="modal-contact">
    <div class="modal-sandbox"></div>
    <div class="ico ico-close"></div>
    <div class="modal-box">
        <div class="modal-header">
            <div class="modal-title">
                <?php echo $modalTitle; ?>
            </div>
            <div class="modal-desc">
                <?php echo $modalDesc. ' ' . $productTitle; ?>
            </div>
        </div>
        <div class="modal-content">
        	<?php
				beam_contact_form( $contact_object, $contact_to );
			?>
        </div>
    </div>
</div>