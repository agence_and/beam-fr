<div class="modal modal-video" tabindex="-1" role="dialog" aria-labelledby="<?php esc_html_e('Video'); ?>" aria-hidden="true" id="modal-video">
    <div class="modal-sandbox"></div>
    <div class="ico ico-close"></div>
    <div class="modal-box">
        <?php echo $video; ?>
    </div>
</div>