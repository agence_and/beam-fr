<div class="modal modal-team" tabindex="-1" role="dialog" aria-labelledby="<?php the_title(); ?>" aria-hidden="true" id="modal-team-<?php echo $memberID; ?>">
    <div class="modal-sandbox"></div>
    <div class="ico ico-close"></div>
    <div class="modal-box">
        <div class="modal-content">
           <div class="ctn-flex ctn-flex-nowrap ctn-flex-start ctn-flex-top">
               <div class="member-img-ctn">
                   <?php the_post_thumbnail('medium', array('class'=>'member-img')); ?>
               </div>
               <div class="content-inner">
                   <div class="member-name"><?php the_title(); ?></div>
                   <div class="member-position"><?php echo $memberFields['position']; ?></div>
                   <?php the_content(); ?>
               </div>
           </div>
        </div>
    </div>
</div>