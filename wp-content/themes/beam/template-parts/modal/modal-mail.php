<?php
    $modalTitle = esc_html('Partager cet article', 'beam');
    $modalDesc = esc_html('Complétez le formulaire pour partager cet article à un ami', 'beam');
?>

<div class="modal modal-email" tabindex="-1" role="dialog" aria-labelledby="<?php echo $modalTitle; ?>" aria-hidden="true" id="modal-email">
    <div class="modal-sandbox"></div>
    <div class="ico ico-close"></div>
    <div class="modal-box">
        <div class="modal-header">
            <div class="modal-title">
                <?php echo $modalTitle; ?>
            </div>
            <div class="modal-desc">
                <?php echo $modalDesc; ?>
            </div>
        </div>
        <div class="modal-content">
        	<?php
				$post_thumb = '';
				if( has_post_thumbnail() )
				{
					$post_thumb = current( wp_get_attachment_image_src( get_post_thumbnail_id(), '305x190', true ) );
				}
				
				wp_and_share_by_email_form( get_the_ID(), '', '', $post_thumb );
			?>
        </div>
    </div>
</div>