<?php

$pagePermalink = get_the_permalink($pageID);

$intro = get_field('careers_intro', 'option');
$navigation = get_field('anchors', 'option');

$bg = 'style="background-image:url(' . $intro['bg']['url'] . ')"';
$templateTitle = $intro['title'];
$templateContent = $intro['desc'];
?>

    <div class="page-intro" <?php echo $bg; ?>>
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php echo $templateTitle; ?>
                </h1>
                <?php echo $templateContent; ?>
            </div>

        </div>
    </div>

<?php
if (!empty($navigation)) {
    ?>
    <div class="container">
        <div class="page-navigation">
            <div class="ctn-flex ctn-flex-center">
                <?php
                foreach ($navigation as $anchor) {
                    $link = $anchor['link'];
                    $jobsPage = $anchor['jobs_page'];
                    ?>
                    <a href="<?php echo $link['url']; ?>" <?php echo($link['target'] == '_blank' ? 'target="_blank"' : ''); ?> <?php echo($link['url'] == $pagePermalink || (is_single() && $jobsPage == true) ? 'class="active"' : ''); ?>><?php echo $link['title']; ?>

                        <?php
                        if ($jobsPage == true) {
                            $jobsCount = wp_count_posts('jobs');
                            $jobsCount = $jobsCount->publish;
                            ?>
                            <span><?php echo $jobsCount; ?></span>
                            <?php
                        }
                        ?>
                    </a>
                    <?php
                }
                ?>
            </div>
        </div>

        <div class="page-header ctn-flex ctn-flex-end ctn-flex-no-mg">
            <!--                --><?php //get_template_part('template-parts/breadcrumb'); ?>

            <form name="filters" id="filters" class="filters" method="get" action="<?php echo $templatePermalink; ?>"
                  novalidate>




                <select name="careers-nav"
                        onchange="document.location.href=this.options[this.selectedIndex].value;">


                    <?php
                    $anchors = array();
                    foreach ($navigation as $anchor) {
                        $link = $anchor['link'];
                        $jobsPage = $anchor['jobs_page'];


                        if($link['url'] == $pagePermalink || (is_single() && $jobsPage == true)){
                            array_unshift($anchors, $anchor);
                        } else {
                            $anchors[] = $anchor;
                        }
                    }

                    foreach ($anchors as $anchor){
                        $link = $anchor['link'];
                        $jobsPage = $anchor['jobs_page'];
                        ?>
                    <option value="<?php echo $link['url']; ?>"><?php echo $link['title']; ?>

                            <?php
                            if ($jobsPage == true) {
                                $jobsCount = wp_count_posts('jobs');
                                $jobsCount = $jobsCount->publish;
                                ?>
                                <span>(<?php echo $jobsCount; ?>)</span>
                                <?php
                            }
                            ?>
                        </option>
                        <?php
                    }
                    ?>

                </select>

            </form>
        </div>
    </div>
    <?php
}
?>