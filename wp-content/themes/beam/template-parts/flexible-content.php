<?php if (have_rows('flexible_content', $id)):

    while (have_rows('flexible_content', $id)) : the_row();

        switch (get_row_layout()) {
            case 'bloc_text':
                get_template_part('template-parts/content/bloc', 'text');
                break;
            case 'bloc_quote':
                get_template_part('template-parts/content/bloc', 'quote');
                break;
            case 'bloc_highlighted':
                get_template_part('template-parts/content/bloc', 'highlighted');
                break;
            case 'bloc_buttons':
                get_template_part('template-parts/content/bloc', 'buttons');
                break;
            case 'bloc_contact':
                $form = 'contactmini';
                include(locate_template('template-parts/content/bloc-contact.php', false, false));
                break;
            case 'bloc_card':
                get_template_part('template-parts/content/bloc', 'card');
                break;
            case 'bloc_testimonies':
                get_template_part('template-parts/content/bloc', 'testimonies');
                break;
            case 'bloc_text_img':
                get_template_part('template-parts/content/bloc', 'txt-img');
                break;
            case 'bloc_cols':
                get_template_part('template-parts/content/bloc', 'cols');
                break;
        }

    endwhile;
endif;
?>

<!--include(locate_template('template-parts/bloc-wysiwyg.php', false, false));-->
