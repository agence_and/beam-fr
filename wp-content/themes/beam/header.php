<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <?php
    $logo = get_field('logo', 'option');
    $logoBaseline = get_field('baseline', 'option');
    ?>


    <header id="masthead" class="site-header" role="banner">

        <div class="site-header-top">
            <div class="container">
                <?php wp_nav_menu(array('theme_location' => 'header-top-menu', 'menu_class' => 'menu-ctn header-top-menu ctn-flex ctn-flex-end ctn-flex-mini')); ?>
            </div>
        </div>

        <div class="container">

            <div class="site-header-bottom <?php echo(is_front_page() ? 'site-header-bottom--hp' : ''); ?> ctn-flex ctn-flex-initial">

                <div class="ctn-flex ctn-flex-no-mg header-bottom-inner">


                    <div class="site-branding ctn-flex ctn-flex-start ctn-flex-mini">

                        <p class="site-title">
                            <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                <img src="<?php echo $logo['url']; ?>" alt="<?php bloginfo('name'); ?>"/>
                            </a>
                        </p>

<!--                        <div class="site-baseline">-->
<!--                            <img src="--><?php //echo $logoBaseline['url']; ?><!--"-->
<!--                                 alt="--><?php //echo $logoBaseline['alt']; ?><!--"/>-->
<!--                        </div>-->

                    </div><!-- .site-branding -->

                    <nav id="site-navigation" class="main-navigation" role="navigation">
                        <?php
                        $walkerMainMenu = new Menu_With_Submenus;
                        wp_nav_menu(array('theme_location' => 'main-menu', 'menu_id' => 'primary-menu', 'menu_class' => 'menu-ctn header-bottom-menu ctn-flex ctn-flex-initial', 'walker' => $walkerMainMenu)); ?>
                    </nav><!-- #site-navigation -->
                </div>


                <div class="site-search">
                    <div class="site-search-btn ctn-flex ctn-flex-center ctn-flex-column">
                        <div class="ico ico-zoom"></div>
                    </div>
                    <div class="site-search-form">
                        <form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                            <input type="text" class="field" name="s" id="s"
                                   placeholder="<?php esc_html_e('Entrez ici votre mot clé et appuyez sur la touche [Entrée]', 'beam'); ?>"/>
                        </form>
                    </div>
                </div>

                <a href="#" class="menu-toggle">
                    <span></span>
                    <span></span>
                </a>


            </div>
        </div>


        <div class="mobile-menu">
            <?php
            $walkerMainMenu = new Menu_With_Submenus;
            wp_nav_menu(array('theme_location' => 'main-menu', 'menu_id' => 'primary-menu-mobile', 'menu_class' => 'menu-ctn', 'walker' => $walkerMainMenu)); ?>
            <!--            --><?php //wp_nav_menu(array('theme_location' => 'header-top-menu', 'menu_class' => 'menu-ctn header-top-menu ctn-flex ctn-flex-end ctn-flex-mini')); ?>
        </div>


    </header><!-- #masthead -->


    <div class="header-push"></div>

    <div id="content" class="site-content">
