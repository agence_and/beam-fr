jQuery(function ($) {

    var winW = '';

    $(document).ready(init);

    function init() {

        resize();
        $(window).on('resize', resize);

        $(window).scroll(function(){
            if($(this).scrollTop()){
                $('.scrolltop, .scrolltop-btn').fadeIn();
            } else {
                $('.scrolltop, .scrolltop-btn').fadeOut();
            }
        });

        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();
            $('.page-with-nav .content-aside-inner a').each(function () {
                var target = $(this).attr('href');
                if(scroll >= $(target).offset().top - 150){
                    $('.page-with-nav .content-aside-inner a').removeClass('active');
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        });
//trigger the scroll
        $(window).scroll();//ensure if you're in current position when page is refreshed

        $('.menu-item a[href="#"]').on('click', function (e) {
            e.preventDefault();
            return false;
        });

        $('.goto-next').on('click', function(){
            var nextDiv = $(this).closest('.product-intro').next();
            $("html, body").animate({
                scrollTop: $(nextDiv).offset().top - 280
            }, 800);
            return false;
        });

        stickyNav();

        menu();

        searchForm();

        $('select').select2({
            minimumResultsForSearch: 20
        });
        $('select#filtercat').on('change', function (e) {
            var target = $(this).find(":selected").val();
            window.location = target;
        });
        $('select#filter').on('change', function (e) {
            var target = $(this).find(":selected").val();
            var hash = $(target);
            if (target.substr(0,1) == "#"){
                $('html, body').animate({
                    scrollTop: hash.offset().top - 100
                }, 800);
                return false;
            } else {
                window.location = target;
            }
        });


        $('input[type="file"]').on('change', function () {
            var filename = $(this).val();
            if (filename.substring(3, 11) == 'fakepath') {
                filename = filename.substring(12);
            } // Remove c:\fake at beginning from localhost chrome
            $(this).closest('.file').find('label span:first-child').html(filename);
        });

        accordion();

        smoothScroll();

        flipster();
        slick();

        modal();

        mobileMenu();

        pageWithNav();

        $('.acf-map').each(function () {

            // create map
            map = new_map($(this));

        });
    }


    function menu() {
        $(".header-bottom-menu li").hover(function () {
            $(this).siblings('li').addClass('unfocused');
        }, function () {
            $(this).siblings('li').removeClass('unfocused');
        });

    }

    function flipster() {
        if ($('.bloc-testimonies').length) {
            $('.bloc-testimonies').flipster({
                style: 'flat',
                nav: 'after',
                spacing: -.75,
                scrollwheel: false,
                loop: true
            });
        }
    }

    function smoothScroll() {
        $(window).on("load", function () {
            var urlHash = window.location.href.split("#")[1];
            if (urlHash && $('#' + urlHash).length)
                $('html,body').animate({
                    scrollTop: $('#' + urlHash).offset().top - 280
                }, 800);
        });

        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 280
                    }, 800);
                    return false;
                }
            }
        });
    }

    function resize() {
        winW = window.innerWidth;
    }

    function searchForm() {
        $('.site-search-btn').on('click', function () {
            $(this).closest('.site-search').toggleClass('active');
            $('.site-search-form').find('input').focus();
        });
    }

    function modal() {
        $(".modal-trigger").click(function (e) {
            e.preventDefault();
            $('body').addClass('no-scroll');
            dataModal = $(this).attr("data-modal");
            dataType = $(this).attr("data-type");
            if (dataModal == 'modal-contact') {
                if ($(this).attr('data-email-to')) {
                    $('#form-contact input[name=contact_to]').val($(this).data('email-to'));
                }
            }
            if (dataType == 'team') {
                $("#" + dataModal).appendTo('.page-team').css({"display": "block"});
            } else {
                $("#" + dataModal).css({"display": "block"});
            }

        });

        $(".ico-close, .modal-sandbox").click(function () {
            $(".modal").css({"display": "none"});
            $('body').removeClass('no-scroll');
        });
    }

    function stickyNav() {
        var top_fixed;
        if ($('.site-header').length > 0) {
            top_fixed = $('.site-header-bottom').offset().top - 8;
        }
        $(window).scroll(function () {
            if ($('.site-header').length > 0) {
                $('.site-header').toggleClass('sticky', $(window).scrollTop() > top_fixed);
            }
        });
    }

    function mobileMenu() {
        // menu toggle
        $('.menu-toggle').on('click', function (e) {
            e.preventDefault();
            var parent = $(this).closest('.site-header');

            if ($(window).width() <= 991) {
                if (parent.hasClass('open')) {
                    $('.mobile-menu').slideUp();
                    $('body').removeClass('no-scroll');
                } else {
                    $('.mobile-menu').slideDown();
                    $('body').addClass('no-scroll');
                }
            }

            parent.toggleClass('open');

        });

        // menu mobile sub menu toggle
        if (winW <= 991) {
            $('#primary-menu-mobile >.menu-item-has-children >a, #primary-menu-mobile >.menu-item-has-children >span').on('click', function (e) {
                e.preventDefault();

                var parent = $(this).parent();
                var submenu = parent.find('.sub-menu-0');
                parent.addClass('active');
                $('#primary-menu-mobile >.menu-item-has-children').not(parent).removeClass('active');

                if (parent.hasClass('active')) {
                    $('.sub-menu-0').not(submenu).slideUp();
                    submenu.slideDown();
                } else {
                    submenu.slideUp();
                }

            });
        }


    }

    function accordion() {
        if ($('.accordion-items').length) {
            var allPanels = $('.accordion-items .accordion-item');
            var allAnswers = $('.accordion-items .accordion-item-answer').hide();

            $('.accordion-item .accordion-item-question').click(function (e) {
                e.preventDefault();
                $this = $(this);
                $parent = $this.closest('.accordion-item');
                $target = $this.next();

                if (!$parent.hasClass('active')) {
                    allPanels.removeClass('active');
                    $parent.addClass('active');
                    allAnswers.slideUp();
                    $target.slideDown();
                } else {
                    $parent.removeClass('active');
                    $target.slideUp();
                }

                return false;
            });
        }
    }

    function slick() {
        if ($('.featured-slider').length) {
            if ($('.slider-item').length > 1) {
                $('.featured-slider').slick({
                    infinite: true,
                    dots: true,
                    arrows: false,
                    adaptiveHeight: true
                });
            }
        }

        if ($('.home-header').length) {
            if ($('.header-slide').length > 1) {
                $('.header-right-slider').slick({
                    useTransform: true,
                    infinite: true,
                    dots: true,
                    arrows: false,
                    autoplaySpeed: 5000,
                    cssEase: 'ease-in-out'
                });
            }
        }

        if ($('.careers-testimonies').length) {
            $('.careers-testimonies').slick({
                infinite: true,
                dots: true,
                arrows: false,
            });
        }


        if ($('.news-slider').length) {
            $('.news-slider-container').on('init', function(){
                $('.news-slider-container .slick-slide').css('visibility', 'visible');
            });
            $('.news-slider-container').slick({
                infinite: false,
                arrows: true,
                dots: true,
                prevArrow: '<span class="slick-prev-news"></span>',
                nextArrow: '<span class="slick-next-news"></span>',
                appendArrows: '.news-slider .slider-nav',
                appendDots: '.news-slider .slider-nav',
                adaptiveHeight: false
            });
        }

        if ($('.feature-addition-slider').length) {
            // $('.feature-addition-slider').slick({
            //     infinite: true,
            //     dots: true,
            //     arrows: false,
            //     adaptiveHeight: true,
            //     // appendDots: $('.images-img')
            // });

            if ($('.images-img').length) {
                if ($('.images-img .images-img-item').length > 1) {
                    $('.images-img').slick({
                        infinite: true,
                        dots: true,
                        arrows: false,
                        adaptiveHeight: true,
                        asNavFor: '.images-desc'
                    });
                }
            }



            $('.images-desc').slick({
                infinite: true,
                dots: false,
                arrows: false,
                fade: true,
                cssEase: 'linear',
                speed: 500,
                adaptiveHeight: true,
                asNavFor: '.images-img'
            });

            // // On before slide change
            // $('.featured-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            //     currentSlide.find('.images-desc').fadeOut();
            //     nextSlide.find('.images-desc').fadeOut();
            // });
            //
            // // On before slide change
            // $('.featured-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
            //     currentSlide.find('.images-desc').fadeIn();
            // });
        }


        if ($('.social-slider').length) {
            var offsetX = (winW - $('.container').outerWidth()) / 2 - 15 - 7;
            $('.social-slider').slick({
                infinite: false,
                dots: true,
                arrows: false,
                adaptiveHeight: false,
                slidesToShow: 2,
                centerMode: true,
                centerPadding: offsetX + 'px',
                initialSlide: 1,
                responsive: [
                    {
                        breakpoint: 9999,
                        settings: {
                            slidesToShow: 2,
                            centerMode: true,
                            centerPadding: offsetX + 'px',
                            initialSlide: 1,
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            adaptiveHeight: true,
                            slidesToShow: 1,
                            centerMode: false,
                            initialSlide: 0,
                        }
                    },
                ]
            });
        }

        if ($('.team-members').length) {
            $('.team-members').slick({
                infinite: false,
                dots: true,
                arrows: false,
                adaptiveHeight: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                variableWidth: false,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 519,
                        settings: {
                            slidesToShow: 1,
                        }
                    },
                ]
            });
        }

        if ($('.nozzles-images').length) {
            if (winW <= 799) {
                $('.nozzles-images').slick({
                    infinite: true,
                    dots: true,
                    arrows: false,
                    asNavFor: '.table-slider',
                    responsive: [
                        {
                            breakpoint: 9999,
                            settings: "unslick"
                        },
                        {
                            breakpoint: 799,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
                });
            }
        }
        if ($('.table-slider').length) {
            if (winW <= 799) {
                $('.table-slider').slick({
                    infinite: true,
                    dots: true,
                    arrows: false,
                    asNavFor: '.nozzles-images',
                    responsive: [
                        {
                            breakpoint: 9999,
                            settings: "unslick"
                        },
                        {
                            breakpoint: 799,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($('.home-products-inner').length) {
            if (winW <= 991) {
                $('.home-products-inner').slick({
                    infinite: true,
                    dots: true,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 9999,
                            settings: "unslick"
                        },
                        {
                            breakpoint: 991,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($('.near-net-shapes-content').length) {
            if (winW <= 991) {
                $('.near-net-shapes-content .bloc-content-images').slick({
                    infinite: true,
                    dots: true,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 9999,
                            settings: "unslick"
                        },
                        {
                            breakpoint: 991,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($('.crosscontent').length || $('.home-news').length) {
            if (winW <= 1199) {
                $('.crosscontent >.ctn-flex, .home-news-inner').slick({
                    infinite: true,
                    dots: true,
                    arrows: false,
                    responsive: [
                        {
                            breakpoint: 9999,
                            settings: "unslick"
                        },
                        {
                            breakpoint: 1199,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                            }
                        },
                        {
                            breakpoint: 991,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                            }
                        },
                        {
                            breakpoint: 639,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
                });
            }
        }


    }

    function pageWithNav() {
        var headerH = $('.site-header').outerHeight() + 30;
        var footerH = $('.site-contact-map').outerHeight() + $('.site-footer').outerHeight() + 30;
        $(".content-aside-inner").sticky({
            topSpacing: headerH,
            bottomSpacing: footerH
        });

        $('.page-with-nav:not(.single-partners) .content-aside a').on('click', function (e) {
            e.preventDefault();
            $('.content-aside a').not($(this)).removeClass('active');
            $(this).addClass('active');
        });
    }


    /*
     *  new_map
     *
     *  This function will render a Google Map onto the selected jQuery element
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	$el (jQuery element)
     *  @return	n/a
     */

    function new_map($el) {

        // var
        var $markers = $el.find('.marker');


        // vars
        var args = {
            zoom: 16,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        // create map
        var map = new google.maps.Map($el[0], args);


        // add a markers reference
        map.markers = [];


        // add markers
        $markers.each(function () {

            add_marker($(this), map);

        });


        // center map
        center_map(map);


        // return
        return map;

    }

    /*
     *  add_marker
     *
     *  This function will add a marker to the selected Google Map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	$marker (jQuery element)
     *  @param	map (Google Map object)
     *  @return	n/a
     */

    function add_marker($marker, map) {

        // var
        var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

        // create marker
        var marker = new google.maps.Marker({
            position: latlng,
            map: map
        });

        // add to array
        map.markers.push(marker);

        // if marker contains HTML, add it to an infoWindow
        if ($marker.html()) {
            // create info window
            var infowindow = new google.maps.InfoWindow({
                content: $marker.html()
            });

            // show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function () {

                infowindow.open(map, marker);

            });
        }

    }

    /*
     *  center_map
     *
     *  This function will center the map, showing all markers attached to this map
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	4.3.0
     *
     *  @param	map (Google Map object)
     *  @return	n/a
     */

    function center_map(map) {

        // vars
        var bounds = new google.maps.LatLngBounds();

        // loop through all markers and create bounds
        $.each(map.markers, function (i, marker) {

            var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

            bounds.extend(latlng);

        });

        // only 1 marker?
        if (map.markers.length == 1) {
            // set center of map
            map.setCenter(bounds.getCenter());
            map.setZoom(16);
        }
        else {
            // fit to bounds
            map.fitBounds(bounds);
        }

    }

    /*
     *  document ready
     *
     *  This function will render each map when the document is ready (page has loaded)
     *
     *  @type	function
     *  @date	8/11/2013
     *  @since	5.0.0
     *
     *  @param	n/a
     *  @return	n/a
     */
// global var
    var map = null;


});