<?php
// CONFIG

include('configure/configure.php');

// JAVASCRIPT & CSS

include('configure/js-css.php');


// ADMIN

include('configure/admin.php');

// HELPERS

include('configure/helpers.php');

// SEARCH

include('configure/search.php');

// SHORTCODES

//include( 'configure/shortcodes.php' );

// TINYMCE

//include( 'configure/tinymce.php' );

// CONTACTFORM7

//include( 'configure/contactform7.php' );

// ACF

include('configure/acf.php');
