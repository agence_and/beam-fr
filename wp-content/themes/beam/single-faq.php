<?php
/**
 * Single faq template
 *
 * @package beam
 *
 */

get_header();


$args = [
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'templates/faq.php'
];
$pages = get_posts($args);
$templateID = $pages[0];

$templatePermalink = get_the_permalink($templateID);

if (has_post_thumbnail($templateID)) {
    $bg = 'style="background-image:url(' . get_the_post_thumbnail_url($templateID) . ');"';
} else {
    $bg = '';
}

$templateTitle = get_the_title($templateID);
$templateContent = get_post_field('post_content', $templateID);

$categories = get_terms(array(
    'taxonomy' => 'faq_category',
    'hide_empty' => true,
));

$templateFields = get_fields($templateID);
$intro = $templateFields['intro'];

$id = get_the_ID();
$currentCat = get_the_terms($id, 'faq_category');

$content = get_post_field('post_content', $id);
?>

<div class="page-with-nav page-partners single-partners">
    <div class="page-intro" <?php echo $bg; ?>>
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php echo $templateTitle; ?>
                </h1>
                <?php echo $templateContent; ?>
            </div>

        </div>
    </div>

    <div class="page-content">
        <div class="container">

            <div class="page-header ctn-flex ctn-flex-end ctn-flex-no-mg">
                <!--                --><?php //get_template_part('template-parts/breadcrumb'); ?>

                <form name="filters" id="filters" class="filters" method="get" action="<?php echo $templatePermalink; ?>"
                      novalidate>

                    <label><?php esc_html_e('Catégories', 'beam'); ?></label>


                    <select name="faq-tax"
                            onchange="document.location.href=this.options[this.selectedIndex].value;">

                        <option value="<?php echo $templatePermalink; ?>"><?php esc_html_e('Choix', 'beam'); ?></option>
                        <?php

                        foreach ($categories as $cat) {
                            ?>
                            <option value="<?php echo $templatePermalink .'#'. $cat->slug; ?>"><?php echo $cat->name; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                </form>
            </div>

            <div class="ctn-flex ctn-flex-top ctn-flex-start ctn-flex-nowrap">
                <div class="content-aside">
                    <div class="content-aside-inner">
                        <h4 class="h4"><?php _e('Catégories', 'beam'); ?></h4>
                        <?php
                        foreach ($categories as $cat) {
                            ?>
                            <a href="<?php echo $templatePermalink; ?>#<?php echo $cat->slug; ?>" <?php  echo($cat->term_id == $currentCat[0]->term_id ? 'class="active"' : ''); ?>><?php echo $cat->name; ?></a>
                        <?php } ?>
                    </div>
                </div>

                <div class="content-main">
                    <h3 class="h3"><?php the_title(); ?></h3>
                    <?php echo $content; ?>
                    <a class="link-back" href="<?php echo $templatePermalink; ?>"><?php esc_html_e('< Retour à la FAQ', 'beam'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
