<?php
/**
 * Single jobs template
 *
 * @package beam
 *
 */

get_header();

$pageID = get_the_ID();

$applyFormPage = get_field('apply_page', 'option');


$category = get_the_terms($pageID, 'jobs_category');

$fields = get_fields($pageID);
$aside = $fields['what_we_offer'];

?>

    <div class="page-careers single-job">

        <?php
        include(locate_template('template-parts/careers-header.php', false, false));
        ?>


        <div class="container">
            <div class="container-mini">
                <div class="industry"><?php echo $category[0]->name; ?></div>
                <h2 class="h2"><?php the_title(); ?></h2>
                <?php
                if (!empty($fields['location'])) {
                    ?>
                    <div class="intro"><?php echo $fields['location']['address']; ?></div>
                    <?php
                }

                if (!empty($fields['short_desc'])) {
                    echo $fields['short_desc'];
                }
                ?>
            </div>
        </div>

        <div class="job-inner ctn-flex ctn-flex-nowrap ctn-flex-top">

            <div class="job-main">
                <div class="main-inner">
                <?php
                if (!empty($fields['desc'])) {
                    echo $fields['desc'];
                }
                ?>

                    <div class="btn-ctn">
                <a href="<?php echo $applyFormPage; ?>?offerID=<?php echo $pageID; ?>" class="btn"><?php esc_html_e('Postuler', 'beam'); ?></a>
                    </div>
                </div>
            </div>

            <div class="job-aside">
                <a href="<?php echo $applyFormPage; ?>?offerID=<?php echo $pageID; ?>" class="btn"><?php esc_html_e('Postuler à cette offre', 'beam'); ?></a>

                <div class="aside-content">
                    <?php
                    if (!empty($aside['title'])) {
                        ?>
                        <h4 class="h4"><?php echo $aside['title']; ?></h4>
                        <?php
                    }

                    if (!empty($aside['content'])) {
                        echo $aside['content'];
                    }

                    if ($aside['show_workwithbeam_content'] == true) {
                        $workwithbeamPageId = $aside['workwithbeam_page'];
                        ?>

                        <div class="content-workwithbeam">

                            <?php
                            if (have_rows('flexible_content', $workwithbeamPageId)):

                                while (have_rows('flexible_content', $workwithbeamPageId)) :
                                    the_row();

                                    switch (get_row_layout()) {

                                        case 'img_content':

                                            $content = get_sub_field('content');
                                            $img = get_sub_field('img');
                                            $video = get_sub_field('video');
                                            ?>


                                            <?php
                                            if (!empty($video)) {
                                                ?>
                                                <div class="ctn-img">
                                                    <img src="<?php echo $img['sizes']['medium_large']; ?>"
                                                         alt="<?php echo $img['alt']; ?>"/>
                                                    <a href="#"
                                                       class="modal-trigger btn-play"
                                                       data-modal="modal-video">
                                                        <span class="ico ico-play-big"></span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="ctn-txt">
                                                <?php echo $content; ?>
                                            </div>
                                            <?php
                                            break;
                                    }
                                endwhile;
                            endif;
                            ?>

                        </div>

                        <?php
                    }
                    ?>
                </div>

                <?php
                $description = 'Partager cette offre';
                include(locate_template('template-parts/share.php', false, false));
                ?>
            </div>
        </div>

        <div class="btn-ctn">
            <a href="<?php echo $applyFormPage; ?>?offerID=<?php echo $pageID; ?>" class="btn"><?php esc_html_e('Postuler', 'beam'); ?></a>
        </div>

    </div>

<?php include(locate_template('template-parts/modal/modal-video.php', false, false)); ?>


<?php get_footer(); ?>