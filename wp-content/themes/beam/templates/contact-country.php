<?php
/**
 * Template Name: Contact us - Country
 *
 * @package beam
 *
 */
get_header();

$templatePermalink = get_the_permalink();

$options = get_option(BEAM_FORMS_SETTINGS_OPTION_NAME);

$usaOffice = get_field('usa_office', 'option');
$franceOffice = get_field('france_office', 'option');


if ($franceOffice['link'] == $templatePermalink) {
    $office = $franceOffice;
    $object = 'BeAM - Contact France - Message from the site';
    $recipient = $options['beam_forms_settings_contactfr_email_to'];
}
if ($usaOffice['link'] == $templatePermalink) {
    $office = $usaOffice;
    $object = 'BeAM - Contact USA - Message from the site';
    $recipient = $options['beam_forms_settings_contactusa_email_to'];
}

?>

<div class="page-content page-contact-country page-form-aside">

    <div class="container">
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>
                <div class="container-mini">
                    <?php the_content(); ?>
                </div>
                <?php
            }
        }
        ?>

        <div class="ctn-flex ctn-flex-top ctn-flex-nowrap">
            <div class="content-form form-alt">
                <?php beam_contact_form($object, $recipient); ?>
            </div>

            <div class="content-aside">
                <?php
                if (!empty($office['gmap'])) {
                    ?>
                    <div class="contact-map acf-map">
                        <div class="marker" data-lat="<?php echo $office['gmap']['lat']; ?>"
                             data-lng="<?php echo $office['gmap']['lng']; ?>"></div>
                    </div>
                    <?php
                }
                ?>
                <div class="contact-aside-main">
                    <div class="contact-localisation">
                        <?php
                        if (!empty($office['country'])) {
                            ?>
                            <span class="contact-country"><?php echo $office['country']; ?></span>
                            <?php
                        }
                        if (!empty($office['city'])) {
                            ?>
                            <span class="contact-city"><?php echo $office['city']; ?></span>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if (!empty($office['address'])) {
                        ?>
                        <p class="contact-address">
                            <?php echo $office['address']; ?>
                        </p>
                        <?php
                    }
                    if (!empty($office['phone'])) {
                        ?>
                        <div class="contact-phone">
                            <?php echo $office['phone']; ?>
                        </div>
                        <?php
                    }
                    if (!empty($office['links'])) {
                        ?>
                        <div class="contact-links ctn-flex ctn-flex-start ctn-flex-mini">
                            <?php
                            foreach ($office['links'] as $linkItem) {
                                $link = $linkItem['link'];
                                ?>
                                <?php
                                if (!empty($link['title'])) {
                                    ?>
                                    <a href="<?php echo $link['url']; ?>" <?php echo($link['target'] == '_blank' ? 'target="_blank"' : ''); ?>><?php echo $link['title']; ?></a>
                                    <?php
                                }
                                ?>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                if (!empty($office['how_to_come'])) {
                    ?>
                    <div class="contact-howtocome">
                        <?php
                        if (!empty($office['how_to_come']['title'])) {
                            ?>
                            <div class="howtocome-title">
                                <?php echo $office['how_to_come']['title']; ?>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        foreach ($office['how_to_come']['solutions'] as $solution) {
                            ?>
                            <?php
                            if (!empty($solution['title'])) {
                                ?>
                                <div class="howtocome-subtitle">
                                    <?php echo $solution['title']; ?>
                                </div>
                                <?php
                            }
                            if (!empty($solution['content'])) {
                                ?>
                                <?php echo $solution['content']; ?>
                                <?php
                            }
                            ?>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

</div>

<?php
get_footer('without-contact');
?>
