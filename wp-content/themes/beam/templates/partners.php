<?php
/**
 * Template Name: Partners
 *
 * @package beam
 *
 */
get_header();


if (is_archive()) {
    $args = [
        'post_type' => 'page',
        'fields' => 'ids',
        'nopaging' => true,
        'meta_key' => '_wp_page_template',
        'meta_value' => 'templates/partners.php'
    ];
    $pages = get_posts($args);
    $templateID = $pages[0];
} else {
    $templateID = get_the_ID();
}

$templatePermalink = get_the_permalink($templateID);


if (has_post_thumbnail($templateID)) {
    $bg = 'style="background-image:url(' . get_the_post_thumbnail_url($templateID) . ');"';
} else {
    $bg = '';
}

$templateTitle = get_the_title($templateID);
$templateContent = get_post_field('post_content', $templateID);

$categories = get_terms(array(
    'taxonomy' => 'partners_category',
    'hide_empty' => true,
));

$catsID = array();

$fields = get_fields($templateID);
$intro = $fields['intro'];
?>

<div class="page-partners page-with-nav">
    <div class="page-intro" <?php echo $bg; ?>>
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php echo $templateTitle; ?>
                </h1>
                <?php echo $templateContent; ?>
            </div>

        </div>
    </div>

    <div class="page-content">
        <div class="container">

            <div class="page-header ctn-flex ctn-flex-end ctn-flex-no-mg">
                <!--                --><?php //get_template_part('template-parts/breadcrumb'); ?>

                <form name="filters" id="filters" class="filters" method="get" action="<?php echo $templatePermalink; ?>"
                      novalidate>

                    <label><?php esc_html_e('Catégories', 'beam'); ?></label>


                    <select name="partners-tax" id="filter">
                        <option value="<?php echo $templatePermalink; ?>"><?php esc_html_e('Choix', 'beam'); ?></option>
                        <?php

                        foreach ($categories as $cat) {
                                ?>
                                <option value="<?php echo '#'. $cat->slug; ?>"><?php echo $cat->name; ?></option>
                                <?php
                        }
                        ?>
                    </select>

                </form>
            </div>

            <div class="ctn-flex ctn-flex-top ctn-flex-start ctn-flex-nowrap">
                <div class="content-aside">
                    <div class="content-aside-inner">
                        <h4 class="h4"><?php _e('Catégories', 'beam'); ?></h4>
                        <a href="#partenaires-intro" class="active"><?php echo $intro['title']; ?></a>
                        <?php
                        foreach ($categories as $cat) {
                            $catsID[] = $cat->term_id;
                            ?>
                            <a href="#<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></a>
                        <?php } ?>
                    </div>
                </div>

                <div class="content-main">
                    <div class="partners-group partners-group--intro" id="partenaires-intro">
                        <h3 class="h3"><?php echo $intro['title']; ?></h3>

                        <div class="partner-content ctn-flex ctn-flex-start ctn-flex-nowrap">

                            <div class="content-logo ctn-flex ctn-flex-center">
                                <img src="<?php echo $intro['img']['sizes']['290x184-nocrop']; ?>"
                                     alt="<?php echo $intro['img']['alt']; ?>"/>
                            </div>

                            <div class="content-desc">
                                <?php echo $intro['desc']; ?>
                                <div class="btn-ctn">
                                    <a href="<?php echo $intro['link']['url']; ?>"
                                       class="link" <?php echo($intro['link']['target'] == '_blank' ? 'target="_blank"' : ''); ?>><?php echo $intro['link']['title']; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    foreach ($catsID as $catID) {
                        $catObject = get_category($catID);

                        ?>
                        <div class="partners-group" id="<?php echo $catObject->slug; ?>">
                            <h3 class="h3"><?php echo get_cat_name($catID); ?></h3>
                            <?php


                            $args = array(
                                'post_type' => array('partners'),
                                'post_status' => array('publish'),
                                'posts_per_page' => -1,
                                'order' => 'DESC',
                                'orderby' => 'date',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => 'partners_category',
                                        'field' => 'id',
                                        'terms' => $catID,
                                    ),
                                )
                            );

                            // The Query
                            $partner_items = new WP_Query($args);


                            // The Loop
                            if ($partner_items->have_posts()) {
                                ?>

                                <?php while ($partner_items->have_posts()) {
                                    $partner_items->the_post();

                                    $partnerID = get_the_ID();
                                    $partnerFields = get_fields($partnerID);
                                    $partnerExcerpt = $partnerFields['excerpt'];
                                    $partnerPermalink = get_the_permalink($partnerID);
                                    ?>

                                    <div class="partner">
                                        <div class="partner-content ctn-flex ctn-flex-start ctn-flex-top ctn-flex-nowrap">
                                            <div class="content-logo ctn-flex ctn-flex-center">
                                                <a href="<?php echo $partnerPermalink; ?>">
                                                    <img src="<?php the_post_thumbnail_url(); ?>"
                                                     alt="<?php echo the_title(); ?>"/>
                                                </a>
                                            </div>
                                            <div class="content-desc">
                                                <div class="h4"><?php the_title(); ?></div>
                                                <p><?php echo $partnerExcerpt; ?></p>
                                                <div class="btn-ctn">
                                                    <a href="<?php echo $partnerPermalink; ?>"
                                                       class="link"><?php _e('En savoir +', 'beam'); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>


                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
