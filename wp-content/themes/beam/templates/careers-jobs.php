<?php
/**
 * Template Name: Careers - Jobs
 *
 * @package beam
 *
 */
get_header();

$pageID = get_the_ID();
?>

<div class="page-careers">

    <?php
    include(locate_template('template-parts/careers-header.php', false, false));
    ?>

    <div class="container">

        <div class="container-mini">

            <h2 class="h2"><?php the_title(); ?></h2>
            <div class="intro"><?php the_content(); ?></div>

        </div>

        <?php
        $args = array(
            'post_type' => array('jobs'),
            'post_status' => array('publish'),
            'posts_per_page' => -1,
            'order' => 'DESC',
            'orderby' => 'date',
        );

        $jobs = new WP_Query($args);

        if ($jobs->have_posts()) {
            ?>

            <table class="jobs-list">
                <thead>
                <tr>
                    <th><?php esc_html_e('Localisation', 'beam'); ?></th>
                    <th><?php esc_html_e('Offre', 'beam'); ?></th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <?php
                while ($jobs->have_posts()) {
                    $jobs->the_post();

                    $jobID = get_the_ID();
                    $jobIndustry = get_the_terms($jobID, 'jobs_category');
                    $jobDesc = get_field('short_desc', $jobID);
                    ?>
                    <tr>
                        <td>
                            <span class="job-industry job-industry--<?php echo $jobIndustry[0]->slug; ?>"><?php echo $jobIndustry[0]->name; ?></span>
                        </td>
                        <td><span class="job-title"><?php the_title(); ?></span><?php echo $jobDesc; ?></td>
                        <td><a href="<?php the_permalink(); ?>"
                               class="btn btn-o"><?php esc_html_e('Voir l’offre', 'beam'); ?></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <?php
        } else {

            $fields = get_fields($pageID);
            $noOffers = $fields['no_offers'];
            $img = $noOffers['image'];
            $title = $noOffers['title'];
            $content = $noOffers['content'];

            ?>

            <div class="jobs-list-nooffers">
                <?php
                if (!empty($title)) {
                    ?>
                    <h3 class="h3"><?php echo $title; ?></h3>
                    <?php
                }
                ?>
                <div class="ctn-flex ctn-flex-nowrap">
                    <img src="<?php echo $img['sizes']['medium']; ?>" alt="<?php echo $img['alt']; ?>"/>
                    <div class="nooffers-content">
                        <?php
                        if (!empty($content)) {
                            ?>
                            <?php echo $content; ?>
                            <?php
                        }
                        if (!empty($link)) {
                            ?>
                            <a href="<?php echo $link['url']; ?>" <?php echo($link['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                               class="btn"><?php echo $link['title']; ?></a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>


    </div>
</div>

<?php
get_footer();
?>