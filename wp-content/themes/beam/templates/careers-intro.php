<?php
/**
 * Template Name: Careers - Work with BeAM
 *
 * @package beam
 *
 */
get_header();

$pageID = get_the_ID();
?>

<div class="page-careers">


    <?php
    include(locate_template('template-parts/careers-header.php', false, false));
    ?>



    <?php if (have_rows('flexible_content', $id)):

        while (have_rows('flexible_content', $id)) :
            the_row();

            switch (get_row_layout()) {
                case 'title':
                    ?>
                    <div class="container">
                        <div class="container-mini">
                            <h2 class="h2"><?php the_sub_field('title'); ?></h2>
                        </div>
                    </div>
                    <?php
                    break;
                case 'intro_content':
                    ?>
                    <div class="container">
                        <div class="container-mini">
                            <div class="intro"><?php the_sub_field('content'); ?></div>
                        </div>
                    </div>
                    <?php
                    break;
                case 'content':
                    get_template_part('template-parts/content/bloc', 'text');
                    break;
                case 'shadow_content':
                    $title = get_sub_field('title');
                    $content = get_sub_field('content');
                    $list = get_sub_field('list');
                    ?>
                    <div class="container">
                        <div class="content-shadow">
                            <?php
                            if (!empty($title)) {
                                ?>
                                <h3 class="h3"><?php echo $title; ?></h3>
                                <?php
                            }
                            if (!empty($content)) {
                                echo $content;
                            }
                            if (!empty($list)){
                            ?>
                            <div class="list-items ctn-flex ctn-flex-initial">
                                <?php
                                foreach ($list as $item) {
                                    ?>
                                    <div class="item">
                                        <?php
                                        if (!empty($item['title'])) {
                                            ?>
                                            <div class="item-title"><?php echo $item['title']; ?></div>
                                            <?php
                                        }
                                        if (!empty($item['content'])) {
                                            echo $item['content'];
                                        }
                                        ?>

                                    </div>
                                    <?php
                                }
                                ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    break;
                case 'testimonies':
                    $quotes = get_sub_field('quotes');
                    ?>
                    <?php
                    if (!empty($quotes)) {
                        ?>
                        <div class="container">
                            <div class="careers-testimonies">
                                <?php
                                foreach ($quotes as $quote) {
                                    ?>
                                    <div class="testimonies-item">
                                        <div class="ctn-flex ctn-flex-bottom ctn-flex-nowrap">
                                            <?php
                                            if (!empty($quote['img'])) {
                                                ?>
                                                <div class="item-img">
                                                    <img src="<?php echo $quote['img']['url']; ?>"
                                                         alt="<?php echo $quote['img']['alt']; ?>"/>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="item-quote">
                                                <?php
                                                if (!empty($quote['title'])) {
                                                    ?>
                                                    <div class="quote-title"><?php echo $quote['title']; ?></div>
                                                    <?php
                                                }

                                                if (!empty($quote['content'])) {
                                                    ?>
                                                    <div class="quote-content"><?php echo $quote['content']; ?></div>
                                                    <?php
                                                }

                                                if (!empty($quote['name'])) {
                                                    ?>
                                                    <div class="quote-name"><?php echo $quote['name']; ?></div>
                                                    <?php
                                                }

                                                if (!empty($quote['job_position'])) {
                                                    ?>
                                                    <div class="quote-job"><?php echo $quote['job_position']; ?></div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    break;
                case 'img_content':

                    $content = get_sub_field('content');
                    $img = get_sub_field('img');
                    $video = get_sub_field('video');
                    ?>

                    <div class="bloc bloc-txt-img">

                        <div class="bloc-txt-img-inner ctn-flex ctn-flex-initial ctn-flex-no-mg">
                            <div class="inner-txt">
                                <?php echo $content; ?>
                            </div>
                            <div class="inner-img"
                                 style="background-image: url(<?php echo $img['sizes']['large']; ?>);">
                                <?php
                                if (!empty($video)) {
                                    ?>
                                    <a href="#"
                                       class="modal-trigger btn-play"
                                       data-modal="modal-video">
                                        <span class="ico ico-play-big"></span>
                                    </a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                    </div>

                    <?php
                    break;
            }

        endwhile;
    endif;
    ?>

</div>

<?php include(locate_template('template-parts/modal/modal-video.php', false, false)); ?>

<?php
get_footer();
?>
