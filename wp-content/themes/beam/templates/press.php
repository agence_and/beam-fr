<?php
/**
 * Template Name: Press
 *
 * @package beam
 *
 */

if (is_archive()) {
    $currentYear = get_query_var('year');
    $args = [
        'post_type' => 'page',
        'fields' => 'ids',
        'nopaging' => true,
        'meta_key' => '_wp_page_template',
        'meta_value' => 'templates/press.php'
    ];
    $pages = get_posts($args);
    $pageID = $pages[0];
} else {
    $pageID = get_the_ID();

}

$permalink = get_the_permalink($pageID);


if (has_post_thumbnail($pageID)) {
    $bg = 'style="background-image:url(' . get_the_post_thumbnail_url($pageID) . ');"';
} else {
    $bg = '';
}

$pageTitle = get_the_title($pageID);
$pageContent = get_post_field('post_content', $pageID);

$fields = get_fields($pageID);
$mediakit = $fields['media_kit'];

get_header();
?>

<div class="page-press">
    <div class="page-intro" <?php echo $bg; ?>>
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php
                    echo $pageTitle;
                    if (is_archive()) {
                        echo ' - ' . $currentYear;
                    } ?>
                </h1>
                <?php echo $pageContent; ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="page-header ctn-flex">
            <?php get_template_part('template-parts/breadcrumb'); ?>

            <form name="filters" id="filters" class="filters" method="get" action="<?php echo $permalink; ?>"
                  novalidate>

                <label><?php esc_html_e('Filtrer :', 'beam'); ?></label>


                <select name="archive-dropdown"
                        onchange="document.location.href=this.options[this.selectedIndex].value;">
                    <?php if (is_archive()) { ?>
                        <option value=""><?php echo $currentYear; ?></option>
                    <?php } ?>
                    <option value="<?php echo $permalink; ?>"><?php esc_html_e('Années', 'beam'); ?></option>

                    <?php wp_get_archives(array(
                        'type' => 'yearly',
                        'format' => 'option',
                        'show_post_count' => 0,
                        'order' => 'DESC',
                        'post_type' => 'press'
                    )); ?>
                </select>

            </form>
        </div>


        <?php
        $years = array();


        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }

        if (is_archive()) {
            $ppp = -1;
        } else {
            $ppp = (int)get_option('posts_per_page');
        }


        $args = array(
            'post_type' => array('press'),
            'post_status' => array('publish'),
            'posts_per_page' => $ppp,
            'order' => 'DESC',
            'orderby' => 'date',
            'paged' => $paged,
        );


        $pressReleases = new WP_Query($args);

        if ($pressReleases->have_posts()) {
        $count = 0;

        ?>

        <div class="infinite infinite-link">
            <div class="jscroll">

                <?php

                if (is_archive()) {
                ?>
                <div class="press-releases-year ctn-flex">
                    <?php echo $currentYear; ?>
                </div>
                <div class="press-releases">
                    <?php
                    }

                    while ($pressReleases->have_posts()) {

                    $pressReleases->the_post();


                    $pageID = get_the_ID();
//                    $date = get_the_date('F d\,\ Y', $pageID);
                    $date = get_the_date('d F Y', $pageID);
                    $fields = get_fields($pageID);
                    $type = $fields['type'];

                    $year = get_the_date('Y');

                    if (is_archive()){

                    if ($currentYear == $year){
                    ?>


                    <?php
                    if ($type != 'web') {
                    $fileName = $fields['file']['filename'];
                    $fileUrl = $fields['file']['url'];
                    ?>
                    <a href="<?php echo $fileUrl; ?>"
                       class="press-releases-item ctn-flex ctn-flex-start ctn-flex-top ctn-flex-nowrap"
                       download="<?php echo $fileName; ?>" target="_blank">
                        <?php
                        } else {
                        $url = $fields['url'];
                        ?>
                        <a href="<?php echo $url; ?>"
                           class="press-releases-item ctn-flex ctn-flex-start ctn-flex-top ctn-flex-nowrap"
                           target="_blank">
                            <?php
                            }
                            ?>
                            <div class="item-date"><?php echo $date; ?></div>

                            <div class="ctn-flex ctn-flex-top ctn-flex-nowrap">
                                <div class="item-content">
                                    <div class="item-title"><?php the_title(); ?></div>
                                    <?php the_content(); ?>
                                </div>

                                <div class="item-file">
                                    <span class="<?php echo $type; ?>"><?php echo $type; ?></span>
                                    <?php
                                    if ($type != 'web') {
                                        $fileSize = filesize(get_attached_file($fields['file']['ID']));
                                        $fileSize = size_format($fileSize, 0);
                                        echo $fileSize;
                                    }
                                    ?>
                                </div>
                            </div>

                        </a>
                        <!--                </div>-->
                        <?php
                        }
                        ?>
                        <?php
                        } else {

                        if ($year != $year_check) {
                        if ($year_check != '') {
                            echo '</div>'; // close the list here
                        }
                        $count++;
                        //                        echo $count;
                        ?>
                        <div class="press-releases-year ctn-flex">
                            <?php echo $year; ?>
                        </div>
                        <div class="press-releases">
                            <?php
                            }
                            ?>


                            <?php
                            if ($type != 'web') {
                            $fileName = $fields['file']['filename'];
                            $fileUrl = $fields['file']['url'];
                            ?>
                            <a href="<?php echo $fileUrl; ?>"
                               class="press-releases-item ctn-flex ctn-flex-start ctn-flex-top ctn-flex-nowrap"
                               download="<?php echo $fileName; ?>" target="_blank">
                                <?php
                                } else {
                                $url = $fields['url'];
                                ?>
                                <a href="<?php echo $url; ?>"
                                   class="press-releases-item ctn-flex ctn-flex-start ctn-flex-top ctn-flex-nowrap"
                                   target="_blank">
                                    <?php
                                    }
                                    ?>
                                    <div class="item-date"><?php echo $date; ?></div>

                                    <div class="ctn-flex ctn-flex-top ctn-flex-nowrap">
                                        <div class="item-content">
                                            <div class="item-title"><?php the_title(); ?></div>
                                            <?php the_content(); ?>
                                        </div>

                                        <div class="item-file">
                                            <span class="<?php echo $type; ?>"><?php echo $type; ?></span>
                                            <?php
                                            if ($type != 'web') {
                                                $fileSize = filesize(get_attached_file($fields['file']['ID']));
                                                $fileSize = size_format($fileSize, 0);
                                                echo $fileSize;
                                            }
                                            ?>
                                        </div>
                                    </div>


                                </a>


                                <?php
                                $year_check = $year;

                                }

                                }

                                wp_reset_postdata();
                                ?>
                        </div>
                        <?php

                        // don't display the button if there are not enough posts
                        if (wp_count_posts('press')->publish >= $ppp) {

                            $next_page_permalink = esc_url($permalink) . '/page/' . ($paged + 1);

                            echo '<div class="btn-ctn"><a href="' . esc_url($next_page_permalink) . '" class="link jscrollnext">';
                            echo '<span>' . __('Voir plus de communiqués de presse', 'beam') . '</span>';
                            echo '</a></div>';
                        }


                        ?>

                </div>
                <?php
                }
                ?>
            </div>
        </div>


        <div class="mediakit-ctn">
            <div class="container">
                <div class="mediakit-ctn-inner" style="background-image: url(<?php echo $mediakit['bg']['url']; ?>);">
                    <h2 class="h2"><?php echo $mediakit['title']; ?></h2>
                    <div class="inner-desc"> <?php echo $mediakit['desc']; ?></div>
                    <?php if ($mediakit['items']) { ?>
                        <div class="inner-items ctn-flex ctn-flex-start ctn-flex-initial">
                            <?php
                            foreach ($mediakit['items'] as $item) {
                                ?>
                                <a href="<?php echo $item['file']['url']; ?>" class="items-file"
                                   download="<?php echo $item['file']['filename']; ?>" target="_blank">
                                    <h4 class="h4"><?php echo $item['title']; ?></h4>
                                    <p><?php echo $item['desc']; ?></p>
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>


        <?php
        $form = 'press';
        include(locate_template('template-parts/content/bloc-contact.php', false, false));
        ?>


        <?php
        get_footer();
        ?>
