<?php
/**
 * Template Name: Contact us
 *
 * @package beam
 *
 */
get_header();

$usaOffice = get_field('usa_office', 'option');
$franceOffice = get_field('france_office', 'option');
$offices = array($usaOffice, $franceOffice);
?>

<div class="page-content page-contact">
    <div class="site-contact-map">
        <div class="container">
            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    the_content();
                }
            }
            ?>
        </div>
        <div class="container">
            <?php
            foreach ($offices as $pin) {
                ?>
                <a href="<?php echo $pin['link']; ?>" class="contact-pin"><?php echo $pin['city']; ?></a>
                <?php
            }
            ?>

            <div class="contact-offices ctn-flex ctn-flex-center ctn-flex-initial">
                <?php

                    foreach ($offices as $office) {

                        ?>
                        <a href="<?php echo $office['link']; ?>" class="offices-item">
                            <div class="item-ctn">
                            <?php
                            if (!empty($office['country'])) {
                                ?>
                                <div class="item-country"><?php echo $office['country']; ?></div>
                                <?php
                            }
                            if (!empty($office['city'])) {
                                ?>
                                <div class="item-city"><?php echo $office['city']; ?></div>
                                <?php
                            }
                            if (!empty($office['address'])) {
                                ?>
                                <p class="item-address"><?php echo $office['address']; ?></p>
                                <?php
                            }
                            ?>
                            </div>
                            <div class="item-btn"><?php esc_html_e('Contact', 'beam'); ?></div>
                        </a>
                        <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
get_footer('without-contact');
?>
