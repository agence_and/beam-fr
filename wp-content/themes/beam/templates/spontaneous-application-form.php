<?php
/**
 * Template Name: Spontaneous application form
 *
 * @package beam
 *
 */
get_header();

$pageID = get_the_ID();

$usaOffice = get_field('usa_office', 'option');
$franceOffice = get_field('france_office', 'option');
$offices = array($usaOffice, $franceOffice);
?>

<div class="page-careers page-application-form page-form-aside">

    <?php
    include(locate_template('template-parts/careers-header.php', false, false));
    ?>


    <div class="container">
        <div class="container-mini">
            <?php the_content(); ?>
        </div>

        <div class="ctn-flex ctn-flex-top ctn-flex-nowrap">
            <div class="content-form form-alt">
                <?php beam_spontaneousapplication_form("Spontaneous application"); ?>
            </div>

            <div class="content-aside">
                <?php

                foreach ($offices as $office) {
                    ?>
                    <div class="contact-aside-main">
                        <div class="contact-localisation">
                            <?php
                            if (!empty($office['country'])) {
                                ?>
                                <span class="contact-country"><?php echo $office['country']; ?></span>
                                <?php
                            }
                            if (!empty($office['city'])) {
                                ?>
                                <span class="contact-city"><?php echo $office['city']; ?></span>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        if (!empty($office['address'])) {
                            ?>
                            <p class="contact-address">
                                <?php echo $office['address']; ?>
                            </p>
                            <?php
                        }
                        if (!empty($office['phone'])) {
                            ?>
                            <div class="contact-phone">
                                <?php echo $office['phone']; ?>
                            </div>
                            <?php
                        }
                        if (!empty($office['links'])) {
                            ?>
                            <div class="contact-links ctn-flex ctn-flex-start ctn-flex-mini">
                                <?php
                                foreach ($office['links'] as $linkItem) {
                                    $link = $linkItem['link'];
                                    ?>
                                    <?php
                                    if (!empty($link['title'])) {
                                        ?>
                                        <a href="<?php echo $link['url']; ?>" <?php echo($link['target'] == '_blank' ? 'target="_blank"' : ''); ?>><?php echo $link['title']; ?></a>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

</div>

<?php
get_footer('without-contact');
?>
