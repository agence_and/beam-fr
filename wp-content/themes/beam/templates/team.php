<?php
/**
 * Template Name: Team
 *
 * @package beam
 *
 */
get_header();


$templateID = get_the_ID();
$templatePermalink = get_the_permalink($templateID);
$bg = 'style="background-image:url(' . get_the_post_thumbnail_url($templateID) . ');"';
$templateTitle = get_the_title($templateID);
$templateContent = get_post_field('post_content', $templateID);


$categories = get_terms(array(
    'taxonomy' => 'team_category',
    'hide_empty' => true,
));

$catsID = array();

foreach ($categories as $cat) {
    $catsID[] = $cat->term_id;
}

$fields = get_fields($templateID);
?>

<div class="page-team">
    <div class="page-intro" <?php echo $bg; ?>>
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php echo $templateTitle; ?>
                </h1>
                <?php echo $templateContent; ?>
            </div>

        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <?php
            if (!empty($fields['title'])) {
                ?>
                <h2 class="h2"><?php echo $fields['title']; ?></h2>
                <?php
            }
            ?>
            <div class="team-intro<?php echo(!empty($fields['img']) ? ' ctn-flex ctn-flex-nowrap' : ''); ?>">
                <?php
                if (!empty($fields['img'])) {
                    ?>
                    <div class="content-img">
                        <img src="<?php echo $fields['img']['sizes']['610x610']; ?>"
                             alt="<?php echo $fields['img']['alt']; ?>"/>
                    </div>
                    <?php
                }
                ?>
                <div class="content">
                    <?php echo $fields['content']; ?>
                </div>
            </div>



            <?php
            foreach ($catsID as $catID) {
                $catObject = get_category($catID);
                ?>
                <div class="team-members-group">
                    <h3 class="h3"><?php echo get_cat_name($catID); ?></h3>
                    <?php


                    $args = array(
                        'post_type' => array('team'),
                        'post_status' => array('publish'),
                        'posts_per_page' => -1,
                        'order' => 'ASC',
                        'orderby' => 'date',
                        'tax_query' => array(
                            'relation' => 'AND',
                            array(
                                'taxonomy' => 'team_category',
                                'field' => 'id',
                                'terms' => $catID,
                            ),
                        )
                    );

                    // The Query
                    $team_members = new WP_Query($args);


                    // The Loop
                    if ($team_members->have_posts()) {
                        ?>
                        <div class="team-members ctn-flex ctn-flex-initial">
                            <?php while ($team_members->have_posts()) {
                                $team_members->the_post();

                                $memberID = get_the_ID();
                                $memberFields = get_fields($memberID);
                                ?>

                                <div class="member">
                                    <div class="member-img-ctn">
                                        <?php the_post_thumbnail('medium', array('class' => 'member-img')); ?>
                                    </div>
                                    <h4 class="member-name"><?php the_title(); ?></h4>
                                    <div class="member-position"><?php echo $memberFields['position']; ?></div>
                                    <a href="#"
                                       class="modal-trigger link" data-modal="modal-team-<?php echo $memberID; ?>"
                                       data-type="team"><?php _e('En savoir +', 'beam'); ?></a>

                                    <?php
                                    include(locate_template('template-parts/modal/modal-team.php', false, false));
                                    ?>

                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
        </div>





    </div>
</div>


<?php
get_footer();
?>
