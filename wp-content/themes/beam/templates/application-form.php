<?php
/**
 * Template Name: Application form
 *
 * @package beam
 *
 */

$offerID = $_GET['offerID'];


if(isset($offerID)) {

    $offerName = get_the_title($offerID);
    $offerLocationObj = get_field('location', $offerID);
    $offerLocation = $offerLocationObj['address'];


get_header();

$templatePermalink = get_the_permalink();

$usaOffice = get_field('usa_office', 'option');
$franceOffice = get_field('france_office', 'option');
$offices = array($usaOffice, $franceOffice);

?>

<div class="page-content page-application-form page-form-aside">
    <div class="container">
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>
                <div class="container-mini">
                    <h1 class="h1"><?php the_title(); ?></h1>
                    <h2 class="h2"><?php echo $offerName; ?></h2>
                    <div class="job-localisation"><?php echo $offerLocation; ?></div>
                </div>
                <?php
            }
        }
        ?>

        <div class="ctn-flex ctn-flex-top ctn-flex-nowrap">
            <div class="content-form form-alt">
                <?php beam_application_form($offerID.' - '.$offerName, $offerID); ?>
            </div>

            <div class="content-aside">
                <?php

                foreach ($offices as $office) {
                    ?>
                    <div class="contact-aside-main">
                        <div class="contact-localisation">
                            <?php
                            if (!empty($office['country'])) {
                                ?>
                                <span class="contact-country"><?php echo $office['country']; ?></span>
                                <?php
                            }
                            if (!empty($office['city'])) {
                                ?>
                                <span class="contact-city"><?php echo $office['city']; ?></span>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        if (!empty($office['address'])) {
                            ?>
                            <p class="contact-address">
                                <?php echo $office['address']; ?>
                            </p>
                            <?php
                        }
                        if (!empty($office['phone'])) {
                            ?>
                            <div class="contact-phone">
                                <?php echo $office['phone']; ?>
                            </div>
                            <?php
                        }
                        if (!empty($office['links'])) {
                            ?>
                            <div class="contact-links ctn-flex ctn-flex-start ctn-flex-mini">
                                <?php
                                foreach ($office['links'] as $linkItem) {
                                    $link = $linkItem['link'];
                                    ?>
                                    <?php
                                    if (!empty($link['title'])) {
                                        ?>
                                        <a href="<?php echo $link['url']; ?>" <?php echo($link['target'] == '_blank' ? 'target="_blank"' : ''); ?>><?php echo $link['title']; ?></a>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

</div>

<?php
get_footer('without-contact');

} else{
    $spApplyFormPage = get_field('sp_apply_page', 'option');
    header("Location: ".$spApplyFormPage);
    die();
}
?>
