<?php
/**
 * Template Name: FAQ
 *
 * @package beam
 *
 */
get_header();


if (is_archive()) {
    $args = [
        'post_type' => 'page',
        'fields' => 'ids',
        'nopaging' => true,
        'meta_key' => '_wp_page_template',
        'meta_value' => 'templates/faq.php'
    ];
    $pages = get_posts($args);
    $templateID = $pages[0];
} else {
    $templateID = get_the_ID();
}

$templatePermalink = get_the_permalink($templateID);


if (has_post_thumbnail($templateID)) {
    $bg = 'style="background-image:url(' . get_the_post_thumbnail_url($templateID) . ');"';
} else {
    $bg = '';
}

$templateTitle = get_the_title($templateID);
$templateContent = get_post_field('post_content', $templateID);

$categories = get_terms(array(
    'taxonomy' => 'faq_category',
    'hide_empty' => true,
));

$catsID = array();

$fields = get_fields($templateID);
?>

<div class="page-faq page-with-nav">
    <div class="page-intro" <?php echo $bg; ?>>
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php echo $templateTitle; ?>
                </h1>
                <?php echo $templateContent; ?>
            </div>

        </div>
    </div>

    <div class="page-content">
        <div class="container">

            <div class="page-header ctn-flex ctn-flex-end ctn-flex-no-mg">
                <!--                --><?php //get_template_part('template-parts/breadcrumb'); ?>

                <form name="filters" id="filters" class="filters" method="get" action="<?php echo $templatePermalink; ?>"
                      novalidate>

                    <label><?php esc_html_e('Catégories', 'beam'); ?></label>


                    <select name="faq-tax" id="filter">

                        <option value="<?php echo $templatePermalink; ?>"><?php esc_html_e('Choix', 'beam'); ?></option>
                        <?php

                        foreach ($categories as $cat) {
                            ?>
                            <option value="<?php echo '#'. $cat->slug; ?>"><?php echo $cat->name; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                </form>
            </div>

            <div class="ctn-flex ctn-flex-top ctn-flex-start ctn-flex-nowrap">
                <div class="content-aside">
                    <div class="content-aside-inner">
                        <h4 class="h4"><?php _e('Catégories', 'beam'); ?></h4>
                        <?php
                        $count = 0;
                        foreach ($categories as $cat) {
                            $catsID[] = $cat->term_id;
                            $count++;
                            ?>
                            <a href="#<?php echo $cat->slug; ?>" <?php echo($count == 1 ? 'class="active"' : ''); ?>><?php echo $cat->name; ?></a>
                        <?php } ?>
                    </div>
                </div>

                <div class="content-main">
                    <?php
                    foreach ($catsID as $catID) {
                        $catObject = get_category($catID);

                        ?>
                        <div class="faq-group" id="<?php echo $catObject->slug; ?>">
                            <h3 class="h3"><?php echo get_cat_name($catID); ?></h3>

                            <div class="accordion-items">
                                <?php


                                $args = array(
                                    'post_type' => array('faq'),
                                    'post_status' => array('publish'),
                                    'posts_per_page' => -1,
                                    'order' => 'DESC',
                                    'orderby' => 'date',
                                    'tax_query' => array(
                                        'relation' => 'AND',
                                        array(
                                            'taxonomy' => 'faq_category',
                                            'field' => 'id',
                                            'terms' => $catID,
                                        ),
                                    )
                                );

                                // The Query
                                $faq_items = new WP_Query($args);


                                // The Loop
                                if ($faq_items->have_posts()) {
                                    ?>

                                    <?php while ($faq_items->have_posts()) {
                                        $faq_items->the_post();

                                        $faqID = get_the_ID();
                                        $faqPermalink = get_the_permalink($faqID);
                                        ?>

                                        <div class="faq accordion-item">
                                            <div class="faq-title accordion-item-question ctn-flex ctn-flex-no-mg ctn-flex-nowrap">
                                                <div><?php the_title(); ?></div>
                                                <div class="ico ico-arrow-down"></div>
                                            </div>
                                            <div class="faq-content accordion-item-answer"><?php the_content(); ?></div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>


                </div>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
