<?php
/**
 * Template Name: Applications
 *
 * @package beam
 *
 */
get_header();


$templateID = get_the_ID();
$templatePermalink = get_the_permalink($templateID);

if (has_post_thumbnail($templateID)) {
    $bg = 'style="background-image:url(' . get_the_post_thumbnail_url($templateID) . ');"';
} else {
    $bg = '';
}

$templateTitle = get_the_title($templateID);
$templateContent = get_post_field('post_content', $templateID);

$fields = get_fields($templateID);


$overview = $fields['overview'];
$repair = $fields['repair'];
$featureAddition = $fields['feature_addition'];
$nearNetShapes = $fields['near_net_shapes'];

$blocs = array($overview, $repair, $featureAddition, $nearNetShapes);
//$blocsSorted = array();

?>

<div class="page-applications">

    <div class="page-intro" <?php echo $bg; ?>>
        <div class="overlay overlay--black"></div>
        <div class="container">
            <div class="intro-inner">
                <h1 class="h1">
                    <?php echo $templateTitle; ?>
                </h1>
                <?php echo $templateContent; ?>
            </div>

        </div>
    </div>

    <div class="page-navigation">
        <div class="container">
            <div class="ctn-flex ctn-flex-center">


                <?php
                foreach ($blocs as $bloc) {
//            $blocsSorted[$bloc['position']] = $bloc;
                    $blocPosition = $bloc['position'];
                    $anchorTitle = $bloc['anchor_title'];
                    $anchorLink = strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $anchorTitle));
                    /*            $activate = $bloc['activated'];
                                $title = $bloc['title'];
                                $subtitle = $bloc['subtitle'];
                                $intro = $bloc['intro'];
                                $desc = $bloc['desc'];
                                $content = $bloc['content'];
                                $media1 = $bloc['media_1'];
                                $media2Type = $bloc['media_type'];
                                $media2Img = $bloc['media_2'];
                                $media2Video = $bloc['media_2_video'];
                                $media2VideoBtn = $bloc['media_2_btn'];
                                $images = $bloc['images'];
                                $slider = $bloc['slider'];*/

                    if (!empty($anchorTitle)) {
                        ?>
                        <a href="#<?php echo $anchorLink; ?>"
                           style="-webkit-box-ordinal-group: <?php echo $blocPosition; ?>; -moz-box-ordinal-group: <?php echo $blocPosition; ?>; -ms-flex-order: <?php echo $blocPosition; ?>; -webkit-order: <?php echo $blocPosition; ?>; order: <?php echo $blocPosition; ?>;"><?php echo $anchorTitle; ?></a>
                        <?php
                    }
                }
                ?>
            </div>
        </div>

    </div>

    <div class="container">

        <div class="ctn-flex ctn-flex-column ctn-flex-no-mg">
            <?php
            if ($overview['activated'] == true) {
                $blocPosition = $overview['position'];
                ?>
                <div class="applications-bloc"
                     style="-webkit-box-ordinal-group: <?php echo $blocPosition; ?>; -moz-box-ordinal-group: <?php echo $blocPosition; ?>; -ms-flex-order: <?php echo $blocPosition; ?>; -webkit-order: <?php echo $blocPosition; ?>; order: <?php echo $blocPosition; ?>;"
                     id="<?php echo strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $overview['anchor_title'])); ?>">
                    <div class="container-medium">
                        <?php
                        if (!empty($overview['title'])) {
                            ?>
                            <h2 class="h2"><?php echo $overview['title']; ?></h2>
                            <?php
                        }

                        if (!empty($overview['subtitle'])) {
                            ?>
                            <h4 class="h4"><?php echo $overview['subtitle']; ?></h4>
                            <?php
                        }
                        if (!empty($overview['intro'])) {
                            ?>
                            <div class="intro"><?php echo $overview['intro']; ?></div>
                            <?php
                        }
                        if (!empty($overview['content'])) {
                            echo $overview['content'];
                        }
                        ?>
                    </div>
                    <?php
                    if (!empty($overview['media_1'])) {
                        ?>
                        <div class="overview-medias ctn-flex ctn-flex-center">
                            <div class="medias-item">
                                <img src="<?php echo $overview['media_1']['url']; ?>"
                                     alt="<?php echo $overview['media_1']['alt']; ?>"/>
                            </div>
                            <?php
                            if (!empty($overview['media_2'] && $overview['media_type'] == 'img')) {
                                ?>
                                <div class="medias-item medias-item-img">
                                    <img src="<?php echo $overview['media_2']['url']; ?>"
                                         alt="<?php echo $overview['media_2']['alt']; ?>"/>
                                </div>
                                <?php
                            }
                            if (!empty($overview['media_2_video'] && $overview['media_type'] == 'video')) {
                                $video = $overview['media_2_video'];
                                ?>
                                <a href="#"
                                   class="modal-trigger medias-item medias-item-video"
                                   data-modal="modal-video">
                                    <div>
                                        <span class="ico ico-play"></span>
                                        <?php
                                        if (!empty($overview['media_2_btn'])) {
                                            echo '<span>' . $overview['media_2_btn'] . '</span>';
                                        }
                                        ?>
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400">
                                        <circle class="outer" cx="50%" cy="50%" r="49%"/>
                                        <!--                                        <circle class="outer" cx="95" cy="95" r="85" transform="rotate(180, 95, 95)"/>-->
                                    </svg>
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>

                </div>
                <?php
            }

            if ($repair['activated'] == true) {
                $blocPosition = $repair['position'];
                ?>
                <div class="applications-bloc"
                     style="-webkit-box-ordinal-group: <?php echo $blocPosition; ?>; -moz-box-ordinal-group: <?php echo $blocPosition; ?>; -ms-flex-order: <?php echo $blocPosition; ?>; -webkit-order: <?php echo $blocPosition; ?>; order: <?php echo $blocPosition; ?>;"
                     id="<?php echo strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $repair['anchor_title'])); ?>">
                    <div class="container-mini">
                        <?php
                        if (!empty($repair['title'])) {
                            ?>
                            <h2 class="h2"><?php echo $repair['title']; ?></h2>
                            <?php
                        }
                        if (!empty($repair['intro'])) {
                            ?>
                            <div class="intro"><?php echo $repair['intro']; ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if (!empty($repair['content'] || !empty($repair['images']))) {
                        ?>
                        <div class="repair-content ctn-flex ctn-flex-nowrap ctn-flex-initial">

                            <?php
                            if (!empty($repair['content'])) {
                                ?>
                                <div class="bloc-content-txt">
                                    <?php echo $repair['content']; ?>
                                </div>
                                <?php

                            }
                            ?>
                            <?php
                            if (!empty($repair['images'])) {
                                ?>
                                <div class="repair-content-images">
                                    <?php foreach ($repair['images'] as $image) { ?>
                                        <img src="<?php echo $image['img']['url']; ?>"
                                             alt="<?php echo $image['img']['alt']; ?>"/>
                                    <?php } ?>
                                </div>
                                <?php

                            }
                            ?>

                        </div>
                        <?php
                    }
                    ?>

                </div>
                <?php
            }

            if ($featureAddition['activated'] == true) {
                $blocPosition = $featureAddition['position'];
                ?>
                <div class="applications-bloc"
                     style="-webkit-box-ordinal-group: <?php echo $blocPosition; ?>; -moz-box-ordinal-group: <?php echo $blocPosition; ?>; -ms-flex-order: <?php echo $blocPosition; ?>; -webkit-order: <?php echo $blocPosition; ?>; order: <?php echo $blocPosition; ?>;"
                     id="<?php echo strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $featureAddition['anchor_title'])); ?>">
                    <div class="container-mini">
                        <?php
                        if (!empty($featureAddition['title'])) {
                            ?>
                            <h2 class="h2"><?php echo $featureAddition['title']; ?></h2>
                            <?php
                        }
                        if (!empty($featureAddition['intro'])) {
                            ?>
                            <div class="intro"><?php echo $featureAddition['intro']; ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if (!empty($featureAddition['content'] || !empty($featureAddition['images']))) {
                        ?>
                        <div class="feature-addition-content ctn-flex ctn-flex-nowrap ctn-flex-top">

                            <?php
                            if (!empty($featureAddition['content'])) {
                                ?>
                                <div class="bloc-content-txt">
                                    <?php echo $featureAddition['content']; ?>
                                </div>
                                <?php

                            }
                            ?>
                            <?php
                            if (!empty($featureAddition['slider'])) {
                                ?>
                                <div class="bloc-content-images feature-addition-slider">
                                    <div>
                                        <div class="ctn-flex ctn-flex-no-mg ctn-flex-bottom ctn-flex-nowrap">
                                            <div class="images-desc">
                                                <?php foreach ($featureAddition['slider'] as $slide) {
                                                    ?>
                                                    <div class="images-desc-item">
                                                        <?php
                                                        if (!empty($slide['title'])) {
                                                            ?>
                                                            <h4 class="h4"><?php echo $slide['title']; ?></h4>
                                                            <?php
                                                        }
                                                        if (!empty($slide['desc'])) { ?>
                                                            <p><?php echo $slide['desc']; ?></p>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="images-img">
                                                <?php foreach ($featureAddition['slider'] as $slide) {
                                                    ?>
                                                    <div class="images-img-item">
                                                        <img src="<?php echo $slide['img']['sizes']['500x500']; ?>"
                                                             alt="<?php echo $slide['img']['alt']; ?>"/>
                                                        <?php if (!empty($slide['legend'])) { ?>
                                                            <div class="img-legend">
                                                                <?php echo $slide['legend']; ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!--       <?php /*foreach ($featureAddition['slider'] as $slide) {
                                        */?>
                                        <div>
                                            <div class="ctn-flex ctn-flex-no-mg ctn-flex-bottom ctn-flex-nowrap">
                                                <div class="images-desc">
                                                    <?php
                                    /*                                                    if (!empty($slide['title'])) {
                                                                                            */?>
                                                        <h4 class="h4"><?php /*echo $slide['title']; */?></h4>
                                                        <?php
                                    /*                                                    }
                                                                                        if (!empty($slide['desc'])) { */?>
                                                        <p><?php /*echo $slide['desc']; */?></p>
                                                        <?php
                                    /*                                                    }
                                                                                        */?>
                                                </div>
                                                <div class="images-img">
                                                    <img src="<?php /*echo $slide['img']['sizes']['500x500']; */?>"
                                                         alt="<?php /*echo $slide['img']['alt']; */?>"/>
                                                    <?php /*if (!empty($slide['legend'])) { */?>
                                                        <div class="img-legend">
                                                            <?php /*echo $slide['legend']; */?>
                                                        </div>
                                                    <?php /*} */?>
                                                </div>
                                            </div>
                                        </div>
                                    --><?php /*} */?>
                                </div>
                                <?php

                            }
                            ?>

                        </div>
                        <?php
                    }
                    ?>

                </div>
                <?php
            }

            if ($nearNetShapes['activated'] == true) {
                $blocPosition = $nearNetShapes['position'];
                ?>
                <div class="applications-bloc"
                     style="-webkit-box-ordinal-group: <?php echo $blocPosition; ?>; -moz-box-ordinal-group: <?php echo $blocPosition; ?>; -ms-flex-order: <?php echo $blocPosition; ?>; -webkit-order: <?php echo $blocPosition; ?>; order: <?php echo $blocPosition; ?>;"
                     id="<?php echo strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $nearNetShapes['anchor_title'])); ?>">
                    <div class="container-mini">
                        <?php
                        if (!empty($nearNetShapes['title'])) {
                            ?>
                            <h2 class="h2"><?php echo $nearNetShapes['title']; ?></h2>
                            <?php
                        }
                        if (!empty($nearNetShapes['intro'])) {
                            ?>
                            <div class="intro"><?php echo $nearNetShapes['intro']; ?></div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    if (!empty($nearNetShapes['content'] || !empty($nearNetShapes['images']))) {
                        ?>
                        <div class="near-net-shapes-content ctn-flex ctn-flex-column ctn-flex-stretch ctn-flex-nowrap">

                            <?php
                            if (!empty($nearNetShapes['content'])) {
                                ?>
                                <div class="bloc-content-txt">
                                    <?php echo $nearNetShapes['content']; ?>
                                </div>
                                <?php

                            }
                            ?>
                            <?php
                            if (!empty($nearNetShapes['images'])) {
                                ?>
                                <div class="bloc-content-images ctn-flex ctn-flex-end ctn-flex-nowrap">
                                    <?php foreach ($nearNetShapes['images'] as $image) { ?>
                                        <div class="images-item">
                                            <div class="item-img ctn-flex ctn-flex-center ctn-flex-no-mg">
                                                <img src="<?php echo $image['img']['url']; ?>"
                                                     alt="<?php echo $image['img']['alt']; ?>"/>
                                            </div>
                                            <div class="item-title">
                                                <?php echo $image['title']; ?>
                                            </div>
                                            <p><?php echo $image['desc']; ?></p>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php

                            }
                            ?>

                        </div>
                        <?php
                    }
                    ?>

                </div>
                <?php
            }
            ?>
        </div>

    </div>


    <?php include(locate_template('template-parts/modal/modal-video.php', false, false)); ?>

    <?php
    get_footer();
    ?>
