</div><!-- #content -->

<?php
$contactMap = get_field('contact_map', 'option');
$footerBaseline = get_field('footer_baseline', 'option');
$footerLogo = get_field('footer_logo', 'option');
$footerDesc = get_field('footer_desc', 'option');
$socialNetworks = get_field('social', 'option');

$usaOffice = get_field('usa_office', 'option');
$franceOffice = get_field('france_office', 'option');
$offices = array($usaOffice, $franceOffice);
?>


<?php
$hideMap = get_field('hide_map', get_the_ID());
?>

<div class="site-contact-map<?php echo($hideMap ? ' site-contact-map--map-hidden' : ''); ?>">
    <div class="container">
        <?php
        if (!$hideMap) {
            foreach ($offices as $pin) {
                ?>
                <a href="<?php echo $pin['link']; ?>" class="contact-pin"><?php echo $pin['city']; ?></a>
                <?php
            }
        }
        if (!empty($contactMap['title'])) {
            ?>
            <div class="contact-map-title h2 h2-alt">
                <?php echo $contactMap['title']; ?>
            </div>
            <?php
        }
        if (!empty($contactMap['desc'])) {
            ?>
            <p>
                <?php echo $contactMap['desc']; ?>
            </p>
            <?php
        }
        if (!empty($contactMap['buttons'])) {
            $count = 0;
            ?>

            <div class="contact-map-buttons ctn-flex ctn-flex-center ctn-flex-mini">
                <?php
                foreach ($contactMap['buttons'] as $btn) {
                    $count++;
                    ?>
                    <a href="<?php echo $btn['button']['url']; ?>" <?php echo($btn['button']['target'] == '_blank' ? 'target="_blank"' : ''); ?>
                       class="btn <?php echo($count == 1 ? '' : 'btn-grey'); ?>">
                        <?php echo $btn['button']['title']; ?>
                    </a>
                    <?php
                }
                ?>
            </div>

            <?php
        }
        ?>
    </div>
</div>

<footer id="colophon" class="site-footer" role="contentinfo">

    <div class="container">
        <div class="footer-top ctn-flex ctn-flex-no-mg">
            <?php
            if (!empty($footerLogo)) {
                ?>
                <div class="footer-logo">
                    <img src="<?php echo $footerLogo['sizes']['medium']; ?>" alt="<?php echo $footerLogo['alt']; ?>"/>
                </div>
                <?php
            }
            if (!empty($footerBaseline)) {
                ?>
                <div class="footer-baseline">
                    <img src="<?php echo $footerBaseline['sizes']['medium']; ?>"
                         alt="<?php echo $footerBaseline['alt']; ?>"/>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="footer-main ctn-flex ctn-flex-top ctn-flex-no-mg">
            <?php
            if (!empty($footerDesc)) {
                ?>
                <div class="footer-desc">
                    <?php echo $footerDesc; ?>
                </div>
                <?php
            }
            ?>

            <div class="footer-menu">
                <?php
                wp_nav_menu(array('theme_location' => 'footer-menu', 'menu_class' => 'menu-ctn footer-main-menu'));
                ?>
            </div>

            <?php
            if (!empty($socialNetworks)) {
                ?>
                <div class="footer-social">
                    <div class="ctn-flex ctn-flex-start ctn-flex-mini">
                        <?php
                        foreach ($socialNetworks as $social) {
                            ?>
                            <a href="<?php echo $social['url']; ?>" target="_blank"
                               title="<?php esc_html_e('Suivez-nous', 'beam'); ?>">
                            <span class="ico"
                                  style="background-image: url('<?php echo $social['ico']['sizes']['thumbnail']; ?>');"></span>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="footer-bottom ctn-flex ctn-flex-no-mg">
            <div class="footer-copyright">
                <?php esc_html_e('Copyright', 'beam'); ?> &copy; <?php echo date('Y') . ' ' . get_bloginfo('name'); ?>
            </div>
            <?php
            wp_nav_menu(array('theme_location' => 'footer-bottom-menu', 'menu_class' => 'menu-ctn footer-bottom-menu ctn-flex ctn-flex-start'));
            ?>
        </div>
    </div>

</footer><!-- #colophon -->

<a href="#page" class="scrolltop-btn"></a>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
